﻿using CreditExchange.Syndication.Plaid.Request;
using CreditExchange.Syndication.Plaid.Response;
using LendFoundry.Foundation.Services;
using Moq;
using RestSharp;
using Xunit;

namespace CreditExchange.Plaid.Client.Test
{
    public class PlaidServiceClientTest
    {
        public PlaidServiceClientTest()
        {
            MockServiceClient = new Mock<IServiceClient>();
            plaidServiceClient = new PlaidService(MockServiceClient.Object);
        }

        public PlaidService plaidServiceClient { get; }

        private IRestRequest restRequest { get; set; }

        private Mock<IServiceClient> MockServiceClient { get; }

        [Fact]
        public async void client_GetAccounts()
        {
            MockServiceClient.Setup(s => s.ExecuteAsync<AccountsResponse>(It.IsAny<IRestRequest>()))
               .Callback<IRestRequest>(r => restRequest = r).ReturnsAsync(new AccountsResponse());

            var response = await plaidServiceClient.GetAccounts("123", "test", It.IsAny<IAccountsRequest>());
            Assert.Equal("{entitytype}/{entityid}/plaid/getaccounts", restRequest.Resource);
            Assert.Equal(Method.POST, restRequest.Method);
        }

        [Fact]
        public async void client_CalculateCashFlow()
        {
            MockServiceClient.Setup(s => s.ExecuteAsync<TransactionsAndAccountsResponse>(It.IsAny<IRestRequest>()))
               .Callback<IRestRequest>(r => restRequest = r).ReturnsAsync(new TransactionsAndAccountsResponse());

            var response = await plaidServiceClient.CalculateCashFlow("123", "test", It.IsAny<IAccountsRequest>());
            Assert.Equal("{entitytype}/{entityid}/plaid/cashflow", restRequest.Resource);
            Assert.Equal(Method.POST, restRequest.Method);
        }

        [Fact]
        public async void client_GetAllBanks()
        {
            MockServiceClient.Setup(s => s.ExecuteAsync<BankResponse>(It.IsAny<IRestRequest>()))
               .Callback<IRestRequest>(r => restRequest = r).ReturnsAsync(new BankResponse());

            var response = await plaidServiceClient.GetAllBanks("123", "test");
            Assert.Equal("{entitytype}/{entityid}/plaid/getallbanks", restRequest.Resource);
            Assert.Equal(Method.GET, restRequest.Method);
        }

        [Fact]
        public async void client_GetExchangeToken()
        {
            MockServiceClient.Setup(s => s.ExecuteAsync<ExchangeTokenResponse>(It.IsAny<IRestRequest>()))
               .Callback<IRestRequest>(r => restRequest = r).ReturnsAsync(new ExchangeTokenResponse());

            var response = await plaidServiceClient.GetExchangeToken("123", "test", It.IsAny<IExchangeTokenRequest>());
            Assert.Equal("{entitytype}/{entityid}/plaid/get_exchange_token", restRequest.Resource);
            Assert.Equal(Method.POST, restRequest.Method);
        }

        [Fact]
        public async void client_DeleteToken()
        {
            MockServiceClient.Setup(s => s.ExecuteAsync<DeleteTokenResponse>(It.IsAny<IRestRequest>()))
               .Callback<IRestRequest>(r => restRequest = r).ReturnsAsync(new DeleteTokenResponse());

            var response = await plaidServiceClient.DeleteToken("123", "test", It.IsAny<IDeleteTokenRequest>());
            Assert.Equal("{entitytype}/{entityid}/plaid/delete_token", restRequest.Resource);
            Assert.Equal(Method.DELETE, restRequest.Method);
        }

        [Fact]
        public async void client_GetRiskData()
        {
            MockServiceClient.Setup(s => s.ExecuteAsync<GetRiskDataResponse>(It.IsAny<IRestRequest>()))
               .Callback<IRestRequest>(r => restRequest = r).ReturnsAsync(new GetRiskDataResponse());

            var response = await plaidServiceClient.GetRiskData("123", "test", "test");
            Assert.Equal("{entitytype}/{entityid}/plaid/get_risk_data/{accessToken}", restRequest.Resource);
            Assert.Equal(Method.GET, restRequest.Method);
        }

        [Fact]
        public async void client_GetTransactionsAndAccounts()
        {
            MockServiceClient.Setup(s => s.ExecuteAsync<TransactionsAndAccountsResponse>(It.IsAny<IRestRequest>()))
               .Callback<IRestRequest>(r => restRequest = r).ReturnsAsync(new TransactionsAndAccountsResponse());

            var response = await plaidServiceClient.GetTransactionsAndAccounts("123", "test", It.IsAny<IAccountsRequest>());
            Assert.Equal("{entitytype}/{entityid}/plaid/get_transactions_accounts", restRequest.Resource);
            Assert.Equal(Method.POST, restRequest.Method);
        }
    }
}