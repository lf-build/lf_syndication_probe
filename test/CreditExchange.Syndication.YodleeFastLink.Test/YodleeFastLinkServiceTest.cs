﻿using CreditExchange.Syndication.YodleeFastLink.Events;
using CreditExchange.Syndication.YodleeFastLink.Proxy;
using CreditExchange.Syndication.YodleeFastLink.Proxy.Response;
using CreditExchange.Syndication.YodleeFastLink.Request;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Lookup;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace CreditExchange.Syndication.YodleeFastLink.application
{
    public class YodleeFastLinkServiceapplication
    {
        private Mock<IEventHubClient> EventHub { get; set; }
        private Mock<ILookupService> Lookup { get; set; }
        private YodleeFastLinkProxy Proxy { get; set; }

        private YodleeFastLinkService yodleeFastLinkService { get; set; }
        private YodleeFastLinkAuthenticateUserRequested yodleeFastLinkAuthenticateUserRequested { get; set; }

        public YodleeFastLinkServiceapplication()
        {
            YodleeFastLinkConfiguration configuration = new YodleeFastLinkConfiguration();
            configuration.ConfigurableParameters = new ConfigurableParameters();
            configuration.ConfigurableParameters.FastLinkLaunchForm = "<form id='fastlinkForm' style='display:none' action='##FastLinkBaseUrl##' method='POST'><input type='text' name='app' value='##AppId##' /><input type='text' name='rsession' value='##UserSessionToken##' /><input type='text' name='token'  value='##FastlinkAccessToken##' /><input type='text' name='redirectReq' value='##IsRedirect##'/><input type='text' name='extraParams' value='##ExtraParams##' /></form><script>document.getElementById('fastlinkForm').submit();</script>";
            configuration.ServiceUrl = new ServiceUrl();
            EventHub = new Mock<IEventHubClient>();
            Lookup = new Mock<ILookupService>();
            Proxy = new YodleeFastLinkProxy(configuration);
            yodleeFastLinkService = new YodleeFastLinkService(Proxy, EventHub.Object, Lookup.Object);
        }

        [Fact]
        public void ThrowsArgumentExceptionWithRequestObjectIsNull_GetCobrandToken()

        {
            Assert.ThrowsAsync<ArgumentNullException>(() => yodleeFastLinkService.GetCobrandToken(null, "CA000001"));
            Assert.ThrowsAsync<ArgumentNullException>(() => yodleeFastLinkService.GetCobrandToken("application", null));
            var categoryLookup = new Dictionary<string, string>();
            categoryLookup.Add("1", "application");
            Lookup.Setup(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(categoryLookup);
            Assert.ThrowsAsync<ArgumentNullException>(() => yodleeFastLinkService.GetCobrandToken("application", "CA000001"));
        }

        [Fact]
        public void ThrowsArgumentExceptionWithRequestObjectIsNull_RegisterUser()
        {
            Assert.ThrowsAsync<ArgumentNullException>(() => yodleeFastLinkService.RegisterUser(null, "CA000001", It.IsAny<UserRegisterNAuthenticateRequest>()));
            Assert.ThrowsAsync<ArgumentNullException>(() => yodleeFastLinkService.RegisterUser("application", null, It.IsAny<UserRegisterNAuthenticateRequest>()));
            Assert.ThrowsAsync<ArgumentNullException>(() => yodleeFastLinkService.RegisterUser("application", "CA000001", null));
            var categoryLookup = new Dictionary<string, string>();
            categoryLookup.Add("1", "application");
            Lookup.Setup(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(categoryLookup);
            Assert.ThrowsAsync<ArgumentNullException>(() => yodleeFastLinkService.RegisterUser(null, "CA000001", It.IsAny<UserRegisterNAuthenticateRequest>()));
        }

        [Fact]
        public void ThrowsArgumentExceptionWithRequestObjectIsNull_AuthenticateUser()
        {
            Assert.ThrowsAsync<ArgumentNullException>(() => yodleeFastLinkService.AuthenticateUser(null, "CA000001", It.IsAny<UserRegisterNAuthenticateRequest>()));
            Assert.ThrowsAsync<ArgumentNullException>(() => yodleeFastLinkService.AuthenticateUser("application", null, It.IsAny<UserRegisterNAuthenticateRequest>()));
            Assert.ThrowsAsync<ArgumentNullException>(() => yodleeFastLinkService.AuthenticateUser("application", "CA000001", null));
            var categoryLookup = new Dictionary<string, string>();
            categoryLookup.Add("1", "application");
            Lookup.Setup(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(categoryLookup);
            Assert.ThrowsAsync<ArgumentNullException>(() => yodleeFastLinkService.AuthenticateUser(null, "CA000001", It.IsAny<UserRegisterNAuthenticateRequest>()));
        }

        [Fact]
        public void ThrowsArgumentExceptionWithRequestObjectIsNull_GetFastlinkAccessToken()
        {
            Assert.ThrowsAsync<ArgumentNullException>(() => yodleeFastLinkService.GetFastlinkAccessToken(null, "CA000001", It.IsAny<FastlinkAccessTokenRequest>()));
            Assert.ThrowsAsync<ArgumentNullException>(() => yodleeFastLinkService.GetFastlinkAccessToken("application", null, It.IsAny<FastlinkAccessTokenRequest>()));
            Assert.ThrowsAsync<ArgumentNullException>(() => yodleeFastLinkService.GetFastlinkAccessToken("application", "CA000001", null));
            var categoryLookup = new Dictionary<string, string>();
            categoryLookup.Add("1", "application");
            Lookup.Setup(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(categoryLookup);
            Assert.ThrowsAsync<ArgumentNullException>(() => yodleeFastLinkService.GetFastlinkAccessToken(null, "CA000001", It.IsAny<FastlinkAccessTokenRequest>()));
        }

        [Fact]
        public void ThrowsArgumentExceptionWithRequestObjectIsNull_GetFalstLinkUrl()
        {
            Assert.ThrowsAsync<ArgumentNullException>(() => yodleeFastLinkService.GetFalstLinkUrl(null, "CA000001", It.IsAny<FastLinkUrlRequest>()));
            Assert.ThrowsAsync<ArgumentNullException>(() => yodleeFastLinkService.GetFalstLinkUrl("application", null, It.IsAny<FastLinkUrlRequest>()));
            Assert.ThrowsAsync<ArgumentNullException>(() => yodleeFastLinkService.GetFalstLinkUrl("CA000001", "CA000001", null));
            var categoryLookup = new Dictionary<string, string>();
            categoryLookup.Add("1", "application");
            Lookup.Setup(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(categoryLookup);
            Assert.ThrowsAsync<ArgumentNullException>(() => yodleeFastLinkService.GetFalstLinkUrl("application", "CA000001", It.IsAny<FastLinkUrlRequest>()));
        }

        [Fact]
        public void ThrowsArgumentExceptionWithRequestObjectIsNull_GetUserBankAccounts()
        {
            Assert.ThrowsAsync<ArgumentNullException>(() => yodleeFastLinkService.GetUserBankAccounts(null, "CA000001", It.IsAny<GetBankAccountsRequest>()));
            Assert.ThrowsAsync<ArgumentNullException>(() => yodleeFastLinkService.GetUserBankAccounts("application", null, It.IsAny<GetBankAccountsRequest>()));
            Assert.ThrowsAsync<ArgumentNullException>(() => yodleeFastLinkService.GetUserBankAccounts("application", "CA000001", null));
            var categoryLookup = new Dictionary<string, string>();
            categoryLookup.Add("1", "application");
            Lookup.Setup(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(categoryLookup);
            Assert.ThrowsAsync<ArgumentNullException>(() => yodleeFastLinkService.GetUserBankAccounts("application", "CA000001", It.IsAny<GetBankAccountsRequest>()));
        }

        [Fact]
        public void ThrowsArgumentExceptionWithRequestObjectIsNull_GetAccountTransactions()
        {
            Assert.ThrowsAsync<ArgumentNullException>(() => yodleeFastLinkService.GetAccountTransactions(null, "CA000001", It.IsAny<GetTransactionRequest>()));
            Assert.ThrowsAsync<ArgumentNullException>(() => yodleeFastLinkService.GetAccountTransactions("application", null, It.IsAny<GetTransactionRequest>()));
            Assert.ThrowsAsync<ArgumentNullException>(() => yodleeFastLinkService.GetAccountTransactions("application", "CA000001", null));
            var categoryLookup = new Dictionary<string, string>();
            categoryLookup.Add("1", "application");
            Lookup.Setup(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(categoryLookup);
            Assert.ThrowsAsync<ArgumentNullException>(() => yodleeFastLinkService.GetAccountTransactions("application", "CA000001", It.IsAny<GetTransactionRequest>()));
        }

        [Fact]
        public void VerifySuccessWithNotNull_GetFalstLinkUrl()
        {
            var categoryLookup = new Dictionary<string, string>();
            categoryLookup.Add("1", "application");
            Lookup.Setup(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(categoryLookup);
            var yodleeFastLinkResponse = yodleeFastLinkService.GetFalstLinkUrl("application", "CA000001", new Request.FastLinkUrlRequest
            {
            }).Result;
            Assert.NotNull(yodleeFastLinkResponse.Content);
            Assert.Equal(yodleeFastLinkResponse.ResponseStatus.ToString(), "Completed");
        }

        [Fact]
        public void VerifySuccessWithNULL_GetFalstLinkUrl()
        {
            var categoryLookup = new Dictionary<string, string>();
            categoryLookup.Add("1", "application");
            Lookup.Setup(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(categoryLookup);
            var yodleeFastLinkResponse = yodleeFastLinkService.GetFalstLinkUrl("application", "CA000001", new Request.FastLinkUrlRequest
            {
                UserSessionToken = "08062013_0:fd93f9b05f7c989451a25e9f9239b8691a93d61ab9878a390b9201db6ae87ac7512a0842879ba8d00f406e5226118391ff20e9e7e533f19e945fffc261cea3ac",
                FastlinkAccessToken = "98c75a5aaf946f50dfb09a5c2860d17a07f828ecc468a3769530e345c65bcc76",
                CobrandToken = "08062013_0:0b3e34b7b05118e50f4b650ec87a97fb731367f7a0d0e3a8573d5d0a22c31867bed66ec75a72e1a3e2edba3de436f0ff51f1d762e0f1cc2079a26d59a45ecb65",
                FastLinkFlow = 0,
                BankSearchKeyWord = "america",
                SiteId = 0,
                CallBackUrl = "https://www.google.com"
            }).Result;
            Assert.NotNull(yodleeFastLinkResponse.Content);
            Assert.Equal(yodleeFastLinkResponse.ResponseStatus.ToString(), "Completed");
        }
    }
}