﻿using CreditExchange.Plaid.Api.Controllers;
using CreditExchange.Syndication.Plaid;
using CreditExchange.Syndication.Plaid.Request;
using CreditExchange.Syndication.Plaid.Response;
using LendFoundry.Foundation.Services;
using Microsoft.AspNet.Mvc;
using Moq;
using Xunit;

namespace CreditExchange.Plaid.Api.Test
{
    public class PlaidController
    {
        private Mock<IPlaidService> yelpService { get; }

        public ApiController GetController(IPlaidService plaidService)
        {
            return new ApiController(plaidService);
        }

        private Mock<IPlaidService> GetPlaidService()
        {
            return new Mock<IPlaidService>();
        }

        #region RetunOnSuccessTest

        [Fact]
        public async void GetExchangeTokenReturnOnSuccess()
        {
            var plaidService = GetPlaidService();
            plaidService.Setup(x => x.GetExchangeToken("", "", It.IsAny<IExchangeTokenRequest>())).ReturnsAsync(It.IsAny<IExchangeTokenResponse>());

            var response = (HttpOkObjectResult)await GetController(plaidService.Object).GetExchangeToken("", "", It.IsAny<ExchangeTokenRequest>());
            Assert.NotNull(response);
            Assert.Equal(response.StatusCode, 200);
        }

        [Fact]
        public async void GetDeleteTokenReturnOnSuccess()
        {
            var plaidService = GetPlaidService();
            plaidService.Setup(x => x.DeleteToken("", "", It.IsAny<IDeleteTokenRequest>())).ReturnsAsync(It.IsAny<IDeleteTokenResponse>());

            var response = (HttpOkObjectResult)await GetController(plaidService.Object).DeleteToken("", "", It.IsAny<DeleteTokenRequest>());
            Assert.NotNull(response);
            Assert.Equal(response.StatusCode, 200);
        }

        [Fact]
        public async void GetGetAccountsReturnOnSuccess()
        {
            var plaidService = GetPlaidService();
            plaidService.Setup(x => x.GetAccounts("", "", It.IsAny<IAccountsRequest>())).ReturnsAsync(It.IsAny<IAccountsResponse>());

            var response = (HttpOkObjectResult)await GetController(plaidService.Object).GetAccounts("", "", It.IsAny<AccountsRequest>());
            Assert.NotNull(response);
            Assert.Equal(response.StatusCode, 200);
        }

        [Fact]
        public async void GetTransactionsAndAccountsReturnOnSuccess()
        {
            var plaidService = GetPlaidService();
            plaidService.Setup(x => x.GetTransactionsAndAccounts("", "", It.IsAny<IAccountsRequest>())).ReturnsAsync(It.IsAny<ITransactionsAndAccountsResponse>());

            var response = (HttpOkObjectResult)await GetController(plaidService.Object).GetAccounts("", "", It.IsAny<AccountsRequest>());
            Assert.NotNull(response);
            Assert.Equal(response.StatusCode, 200);
        }

        [Fact]
        public async void GetRiskDataReturnOnSuccess()
        {
            var plaidService = GetPlaidService();
            plaidService.Setup(x => x.GetRiskData("", "", It.IsAny<string>())).ReturnsAsync(It.IsAny<IGetRiskDataResponse>());

            var response = (HttpOkObjectResult)await GetController(plaidService.Object).GetRiskData("", "", It.IsAny<string>());
            Assert.NotNull(response);
            Assert.Equal(response.StatusCode, 200);
        }

        [Fact]
        public async void GetAllBanksReturnOnSuccess()
        {
            var plaidService = GetPlaidService();
            plaidService.Setup(x => x.GetAllBanks("", "")).ReturnsAsync(It.IsAny<IBankResponse>());

            var response = (HttpOkObjectResult)await GetController(plaidService.Object).GetAllBanks("", "");
            Assert.NotNull(response);
            Assert.Equal(response.StatusCode, 200);
        }

        [Fact]
        public async void CalculateCashFlowReturnOnSuccess()
        {
            var plaidService = GetPlaidService();
            plaidService.Setup(x => x.CalculateCashFlow("", "", It.IsAny<IAccountsRequest>())).ReturnsAsync(It.IsAny<ITransactionsAndAccountsResponse>());

            var response = (HttpOkObjectResult)await GetController(plaidService.Object).CalculateCashFlow("", "", It.IsAny<AccountsRequest>());
            Assert.NotNull(response);
            Assert.Equal(response.StatusCode, 200);
        }

        #endregion RetunOnSuccessTest

        #region ReturnError

        [Fact]
        public async void GetExchangeTokenReturnError()
        {
            var plaidService = GetPlaidService();
            plaidService.Setup(x => x.GetExchangeToken("", "", It.IsAny<IExchangeTokenRequest>())).Throws(new PlaidException());

            var response = (ErrorResult)await GetController(plaidService.Object).GetExchangeToken("", "", It.IsAny<ExchangeTokenRequest>());
            Assert.NotNull(response);
            Assert.Equal(response.StatusCode, 400);
        }

        [Fact]
        public async void GetDeleteTokenReturnError()
        {
            var plaidService = GetPlaidService();
            plaidService.Setup(x => x.DeleteToken("", "", It.IsAny<IDeleteTokenRequest>())).Throws(new PlaidException());

            var response = (ErrorResult)await GetController(plaidService.Object).DeleteToken("", "", It.IsAny<DeleteTokenRequest>());
            Assert.NotNull(response);
            Assert.Equal(response.StatusCode, 400);
        }

        [Fact]
        public async void GetAccountsReturnError()
        {
            var plaidService = GetPlaidService();
            plaidService.Setup(x => x.GetAccounts("", "", It.IsAny<IAccountsRequest>())).Throws(new PlaidException());

            var response = (ErrorResult)await GetController(plaidService.Object).GetAccounts("", "", It.IsAny<AccountsRequest>());
            Assert.NotNull(response);
            Assert.Equal(response.StatusCode, 400);
        }

        [Fact]
        public async void GetRiskDataReturnError()
        {
            var plaidService = GetPlaidService();
            plaidService.Setup(x => x.GetRiskData("", "", It.IsAny<string>())).Throws(new PlaidException());

            var response = (ErrorResult)await GetController(plaidService.Object).GetRiskData("", "", It.IsAny<string>());
            Assert.NotNull(response);
            Assert.Equal(response.StatusCode, 400);
        }

        [Fact]
        public async void GetAllBanksReturnError()
        {
            var plaidService = GetPlaidService();
            plaidService.Setup(x => x.GetAllBanks("", "")).Throws(new PlaidException());

            var response = (ErrorResult)await GetController(plaidService.Object).GetAllBanks("", "");
            Assert.NotNull(response);
            Assert.Equal(response.StatusCode, 400);
        }

        [Fact]
        public async void CalculateCashFlowReturnError()
        {
            var plaidService = GetPlaidService();
            plaidService.Setup(x => x.CalculateCashFlow("", "", It.IsAny<IAccountsRequest>())).Throws(new PlaidException());

            var response = (ErrorResult)await GetController(plaidService.Object).CalculateCashFlow("", "", It.IsAny<AccountsRequest>());
            Assert.NotNull(response);
            Assert.Equal(response.StatusCode, 400);
        }

        [Fact]
        public async void GetTransactionsAndAccountsReturnError()
        {
            var plaidService = GetPlaidService();
            plaidService.Setup(x => x.GetTransactionsAndAccounts("", "", It.IsAny<IAccountsRequest>())).Throws(new PlaidException());

            var response = (ErrorResult)await GetController(plaidService.Object).GetTransactionsAndAccounts("", "", It.IsAny<AccountsRequest>());
            Assert.NotNull(response);
            Assert.Equal(response.StatusCode, 400);
        }

        #endregion ReturnError
    }
}