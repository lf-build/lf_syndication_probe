﻿using CreditExchange.Syndication.Datamerch.Proxy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace CreditExchange.Syndication.Datamerch.Test.Live
{
    public class DataMerchServiceLiveTest
    {
        private IDatamerchConfiguration datamerchConfiguration { get; }
        private DatamerchProxy datamerchProxy { get; }

        public DataMerchServiceLiveTest()
        {
            datamerchConfiguration = new DatamerchConfiguration
            {
                AddMerchantsApiUrl = "/api/v1/merchants",
                AddNoteApiUrl = "/api/v1/notes",
                AuthorizationKey = "zu4F9YcJaw",
                AuthorizationToken = "WDs-s-NjbN4fqeNzYxiJ",
                DatamerchBaseUrl = "https://www.datamerch.com",
                SearchMerchantsApiUrl = "/api/v1/merchants",
                FeinLength= 7

            };
            datamerchProxy = new DatamerchProxy(datamerchConfiguration);
        }

        [Fact]
        public void SearchMerchant_Response()
        {
            var response = datamerchProxy.SearchMerchant("1234567");
            Assert.NotNull(response);
        }
    }
}