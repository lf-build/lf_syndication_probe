﻿using CreditExchange.Syndication.Yelp.Proxy;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Lookup;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace CreditExchange.Syndication.Yelp.Test
{
    public class YelpServiceTest
    {
        private Mock<IEventHubClient> EventHub { get; set; }
        private Mock<ILookupService> Lookup { get; }
        private YelpProxy yelpProxy { get; set; }

        private YelpService yelpService { get; set; }

        public YelpServiceTest()
        {
            EventHub = new Mock<IEventHubClient>();
            Lookup = new Mock<ILookupService>();
            yelpProxy = new YelpProxy(new YelpConfiguration
            {
                TokenSecret = "Fk7-A5npmXhDwx7t3oSX47v5tG0",
                ApiBaseUrl = "http://192.168.1.67:7022",
                SearchApiUrl = "/businesses/",
                ConsumerSecret = "bdkAdwLsGAcCoNKJMyZsCWH-jUs",
                BusinessApiUrl = "/businesses/",
                PhonePrefix = "+",
                Token = "HX2mFfrM-9OIuP1XstU8a0loWZz6ENXk",
                ConsumerKey = "JoqnjKwlWuZ_sR1XBDJG-A",
                GrantType = "client_credentials",
                PhoneApiUrl = "/businesses/"
            });
            yelpService = new YelpService(yelpProxy, EventHub.Object, Lookup.Object);
        }

        [Fact]
        public void VerifyThrowsArgumentExceptionWithRequestObjectIsNull_GetBusinessDetails()
        {
            Assert.ThrowsAsync<ArgumentException>(() => yelpService.GetBusinessDetails("application", "CA000001", "", "", "", true, true));
            Assert.ThrowsAsync<ArgumentNullException>(() => yelpService.GetBusinessDetails("", "CA000001", "123", "", "", true, true));
            Assert.ThrowsAsync<ArgumentNullException>(() => yelpService.GetBusinessDetails("application", "", "123", "", "", true, true));
            Dictionary<string, string> categoryLookup = new Dictionary<string, string>();
            categoryLookup.Add("1", "application");
            Lookup.Setup(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(categoryLookup);
            Assert.ThrowsAsync<ArgumentNullException>(() => yelpService.GetBusinessDetails("application", "CA000001", "", "", "", true, true));
            Assert.ThrowsAsync<ArgumentNullException>(() => yelpService.GetBusinessDetails("application", "", "", "", "", true, true));
            Assert.ThrowsAsync<ArgumentNullException>(() => yelpService.GetBusinessDetails("", "CA000001", "", "", "", true, true));
        }

        [Fact]
        public void VerifyThrowsArgumentExceptionWithRequestObjectIsNull_GetPhoneDetails()
        {
            Assert.ThrowsAsync<ArgumentException>(() => yelpService.GetPhoneDetails("application", "CA000001", ""));
            Assert.ThrowsAsync<ArgumentNullException>(() => yelpService.GetPhoneDetails("", "CA000001", "9876543210"));
            Assert.ThrowsAsync<ArgumentNullException>(() => yelpService.GetPhoneDetails("test", "", "123"));
            Dictionary<string, string> categoryLookup = new Dictionary<string, string>();
            categoryLookup.Add("ABC", "1");
            Lookup.Setup(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(categoryLookup);
            Assert.ThrowsAsync<ArgumentNullException>(() => yelpService.GetPhoneDetails("application", "CA000001", ""));
            Assert.ThrowsAsync<ArgumentNullException>(() => yelpService.GetPhoneDetails("application", "", "9876543210"));
            Assert.ThrowsAsync<ArgumentNullException>(() => yelpService.GetPhoneDetails("", "CA000001", ""));
        }

        [Fact]
        public void VerifyThrowsArgumentExceptionWithRequestObjectIsNull_SearchDetails()
        {
            Assert.ThrowsAsync<ArgumentNullException>(() => yelpService.SearchDetails("application", "", It.IsAny<string>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>()));
            Assert.ThrowsAsync<ArgumentNullException>(() => yelpService.SearchDetails("", "application", It.IsAny<string>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>()));
            Dictionary<string, string> categoryLookup = new Dictionary<string, string>();
            categoryLookup.Add("ABC", "1");
            Lookup.Setup(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(categoryLookup);
            Assert.ThrowsAsync<ArgumentNullException>(() => yelpService.SearchDetails("", "CA000001", It.IsAny<string>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>()));
            Assert.ThrowsAsync<ArgumentNullException>(() => yelpService.SearchDetails("application", "", It.IsAny<string>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>()));
        }

        [Fact]
        public void VerifySuccessWithNotNull_GetPhoneDetails()
        {
            var categoryLookup = new Dictionary<string, string>();
            categoryLookup.Add("1", "application");
            Lookup.Setup(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(categoryLookup);
            var bbbSearchResponse = yelpService.GetPhoneDetails("application", "CA000001", "6192344700").Result;
            Assert.NotNull(bbbSearchResponse.Businesses);
            Assert.NotNull(bbbSearchResponse.Region);
            Assert.NotNull(bbbSearchResponse.Total);
        }

        [Fact]
        public void VerifySuccessWithNULL_GetPhoneDetails()
        {
            var categoryLookup = new Dictionary<string, string>();
            categoryLookup.Add("1", "application");
            Lookup.Setup(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(categoryLookup);
            var bbbSearchResponse = yelpService.GetPhoneDetails("application", "CA000001", "1111111111").Result;
            Assert.Null(bbbSearchResponse.Businesses);
            Assert.Null(bbbSearchResponse.Region);
            Assert.Equal(bbbSearchResponse.Total, 0);
        }
    }
}