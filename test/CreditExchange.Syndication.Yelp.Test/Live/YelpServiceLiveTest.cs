﻿using CreditExchange.Syndication.Yelp.Proxy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace CreditExchange.Syndication.Yelp.Test.Live
{
    public class YelpServiceLiveTest
    {
        private IYelpProxy yelpProxy { get; }
        private IYelpConfiguration yelpConfiguration { get; }

        public YelpServiceLiveTest()
        {
            yelpConfiguration = new YelpConfiguration
            {
                ApiBaseUrl = "http://192.168.1.67:7022",
                BusinessApiUrl = "/businesses/",
                ConsumerKey = "JoqnjKwlWuZ_sR1XBDJG-A",
                ConsumerSecret = "bdkAdwLsGAcCoNKJMyZsCWH-jUs",
                GrantType = "client_credentials",
                PhoneApiUrl = "/businesses/",
                SearchApiUrl = "/businesses/",
                Token = "HX2mFfrM-9OIuP1XstU8a0loWZz6ENXk",
                TokenSecret = "Fk7-A5npmXhDwx7t3oSX47v5tG0",
                PhonePrefix= "+",
            };
            yelpProxy = new YelpProxy(yelpConfiguration);
        }

        [Fact]
        public void ReturnAddDeviceResponseOnValid()
        {
            var response = yelpProxy.SearchDetails("Capital Alliance", "Santa Ana, CA", 2, 1, 1, "", 50, true);
            Assert.NotNull(response);
        }
    }
}