﻿using CreditExchange.Syndication.Yelp.Proxy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace CreditExchange.Syndication.Yelp.Test
{
    public class YelpConfigurationTest
    {
        [Fact]
        public void ThrowArgumentExceptionWithNullConfiguration()
        {
            Assert.Throws<ArgumentNullException>(() => new YelpProxy(null));
        }

        [Fact]
        public void ThrowArgumentExceptionWithMissingConfiguration()
        {
            var yelpConfiguration = new YelpConfiguration
            {
                ApiBaseUrl = "xyztest",
                BusinessApiUrl = "",
                ConsumerKey = "",
                ConsumerSecret = "bdkAdwLsGAcCoNKJMyZsCWH-jUs",
                GrantType = "client_credentials",
                PhoneApiUrl = "",
                SearchApiUrl = "",
                Token = "HX2mFfrM-9OIuP1XstU8a0loWZz6ENXk",
                TokenSecret = ""
            };

            Assert.Throws<ArgumentNullException>(() => new YelpProxy(yelpConfiguration));

            var yelpConfiguration1 = new YelpConfiguration
            {
                ApiBaseUrl = "xyztest",
                BusinessApiUrl = "",
                ConsumerKey = "JoqnjKwlWuZ_sR1XBDJG-A",
                ConsumerSecret = "bdkAdwLsGAcCoNKJMyZsCWH-jUs",
                GrantType = "client_credentials",
                PhoneApiUrl = "",
                SearchApiUrl = "",
                Token = "",
                TokenSecret = ""
            };

            Assert.Throws<ArgumentNullException>(() => new YelpProxy(yelpConfiguration1));

            var yelpConfiguration2 = new YelpConfiguration
            {
                ApiBaseUrl = "",
                BusinessApiUrl = "",
                ConsumerKey = "JoqnjKwlWuZ_sR1XBDJG-A",
                ConsumerSecret = "bdkAdwLsGAcCoNKJMyZsCWH-jUs",
                GrantType = "client_credentials",
                PhoneApiUrl = "",
                SearchApiUrl = "",
                Token = "HX2mFfrM-9OIuP1XstU8a0loWZz6ENXk",
                TokenSecret = ""
            };

            Assert.Throws<ArgumentNullException>(() => new YelpProxy(yelpConfiguration2));
        }
    }
}