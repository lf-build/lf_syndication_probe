﻿using CreditExchange.Syndication.Plaid.Proxy;
using CreditExchange.Syndication.Plaid.Proxy.Response;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup;
using Moq;
using System;
using System.Collections.Generic;
using Xunit;

namespace CreditExchange.Syndication.Plaid.Test
{
    public class PlaidServiceTest
    {
        #region Public Constructors

        public PlaidServiceTest()
        {
            EventHub = new Mock<IEventHubClient>();
            Lookup = new Mock<ILookupService>();
            Proxy = new PlaidProxy(new PlaidConfiguration
            {
                IsLive = false,
                APIURL = "https://tartan.plaid.com/",
                ProxyUrl = "",
                Secret = "test_secret",
                UseProxy = true,
                TransactionDays = 30,
                ClientId = "test_id",
                TLSVersion = "1.2"
            });
            PlaidSyndicationRepository = new Mock<IPlaidSyndicationRepository>();
            Logger = new Mock<ILogger>();
            TenantTime = new Mock<ITenantTime>();
            plaidService = new PlaidService(Proxy, EventHub.Object, Lookup.Object, PlaidSyndicationRepository.Object, Logger.Object, TenantTime.Object);
        }

        #endregion Public Constructors

        #region Private Properties

        private Mock<IEventHubClient> EventHub { get; set; }
        private Mock<ILogger> Logger { get; set; }
        private Mock<ILookupService> Lookup { get; set; }
        private PlaidService plaidService { get; set; }
        private Mock<IPlaidSyndicationRepository> PlaidSyndicationRepository { get; set; }
        private PlaidProxy Proxy { get; set; }
        private Mock<ITenantTime> TenantTime { get; set; }

        #endregion Private Properties

        #region RequestObjectvalueNull Methods

        [Fact]
        public void ThrowsArgumentExceptionWithRequestObjectIsNull_CalculateCashFlow()
        {
            Assert.ThrowsAsync<ArgumentNullException>(() => plaidService.CalculateCashFlow(null, "CA000001", It.IsAny<Request.IAccountsRequest>()));
            Assert.ThrowsAsync<ArgumentNullException>(() => plaidService.CalculateCashFlow("application", null, It.IsAny<Request.IAccountsRequest>()));
            Assert.ThrowsAsync<ArgumentNullException>(() => plaidService.CalculateCashFlow("application", "CA000001", null));
            var categoryLookup = new Dictionary<string, string>();
            categoryLookup.Add("1", "application");
            Lookup.Setup(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(categoryLookup);
            Assert.ThrowsAsync<ArgumentNullException>(() => plaidService.CalculateCashFlow("application", "CA000001", It.IsAny<Request.IAccountsRequest>()));
        }

        [Fact]
        public void ThrowsArgumentExceptionWithRequestObjectIsNull_DeleteToken()
        {
            Assert.ThrowsAsync<ArgumentNullException>(() => plaidService.DeleteToken(null, "CA000001", It.IsAny<Request.IDeleteTokenRequest>()));
            Assert.ThrowsAsync<ArgumentNullException>(() => plaidService.DeleteToken("application", null, It.IsAny<Request.IDeleteTokenRequest>()));
            Assert.ThrowsAsync<ArgumentNullException>(() => plaidService.DeleteToken("application", "CA000001", null));
            var categoryLookup = new Dictionary<string, string>();
            categoryLookup.Add("1", "application");
            Lookup.Setup(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(categoryLookup);
            Assert.ThrowsAsync<ArgumentNullException>(() => plaidService.DeleteToken("application", "CA000001", It.IsAny<Request.DeleteTokenRequest>()));
        }

        [Fact]
        public void ThrowsArgumentExceptionWithRequestObjectIsNull_GetAccounts()
        {
            Assert.ThrowsAsync<ArgumentNullException>(() => plaidService.GetAccounts(null, "CA000001", It.IsAny<Request.IAccountsRequest>()));
            Assert.ThrowsAsync<ArgumentNullException>(() => plaidService.GetAccounts("application", null, It.IsAny<Request.IAccountsRequest>()));
            Assert.ThrowsAsync<ArgumentNullException>(() => plaidService.GetAccounts("application", "CA000001", null));
            var categoryLookup = new Dictionary<string, string>();
            categoryLookup.Add("1", "application");
            Lookup.Setup(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(categoryLookup);
            Assert.ThrowsAsync<ArgumentNullException>(() => plaidService.GetAccounts("application", "CA000001", It.IsAny<Request.IAccountsRequest>()));
        }

        [Fact]
        public void ThrowsArgumentExceptionWithRequestObjectIsNull_GetAllBanks()
        {
            Assert.ThrowsAsync<ArgumentNullException>(() => plaidService.GetAllBanks(null, "CA000001"));
            Assert.ThrowsAsync<ArgumentNullException>(() => plaidService.GetAllBanks("application", null));
            var categoryLookup = new Dictionary<string, string>();
            categoryLookup.Add("1", "appliaction");
            Lookup.Setup(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(categoryLookup);
            Assert.ThrowsAsync<ArgumentNullException>(() => plaidService.GetAllBanks("application", "CA000001"));
        }

        [Fact]
        public void ThrowsArgumentExceptionWithRequestObjectIsNull_GetExchangeToken()
        {
            Assert.ThrowsAsync<ArgumentNullException>(() => plaidService.GetExchangeToken(null, "CA000001", It.IsAny<Request.IExchangeTokenRequest>()));
            Assert.ThrowsAsync<ArgumentNullException>(() => plaidService.GetExchangeToken("application", null, It.IsAny<Request.IExchangeTokenRequest>()));
            Assert.ThrowsAsync<ArgumentNullException>(() => plaidService.GetExchangeToken("application", "CA000001", null));
            var categoryLookup = new Dictionary<string, string>();
            categoryLookup.Add("1", "application");
            Lookup.Setup(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(categoryLookup);
            Assert.ThrowsAsync<ArgumentNullException>(() => plaidService.GetExchangeToken("application", "CA000001", It.IsAny<Request.ExchangeTokenRequest>()));
        }

        [Fact]
        public void ThrowsArgumentExceptionWithRequestObjectIsNull_GetRiskData()
        {
            Assert.ThrowsAsync<ArgumentNullException>(() => plaidService.GetRiskData(null, "CA000001", It.IsAny<string>()));
            Assert.ThrowsAsync<ArgumentNullException>(() => plaidService.DeleteToken("application", null, It.IsAny<Request.IDeleteTokenRequest>()));
            Assert.ThrowsAsync<ArgumentNullException>(() => plaidService.DeleteToken("application", "CA000001", null));
            var categoryLookup = new Dictionary<string, string>();
            categoryLookup.Add("1", "application");
            Lookup.Setup(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(categoryLookup);
            Assert.ThrowsAsync<ArgumentNullException>(() => plaidService.DeleteToken("application", "CA000001", It.IsAny<Request.DeleteTokenRequest>()));
        }

        [Fact]
        public void ThrowsArgumentExceptionWithRequestObjectIsNull_GetTransactionsAndAccounts()
        {
            Assert.ThrowsAsync<ArgumentNullException>(() => plaidService.GetTransactionsAndAccounts(null, "CA000001", It.IsAny<Request.IAccountsRequest>()));
            Assert.ThrowsAsync<ArgumentNullException>(() => plaidService.GetTransactionsAndAccounts("application", null, It.IsAny<Request.IAccountsRequest>()));
            Assert.ThrowsAsync<ArgumentNullException>(() => plaidService.GetTransactionsAndAccounts("application", "CA000001", null));
            var categoryLookup = new Dictionary<string, string>();
            categoryLookup.Add("1", "application");
            Lookup.Setup(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(categoryLookup);
            Assert.ThrowsAsync<ArgumentNullException>(() => plaidService.GetTransactionsAndAccounts("application", "CA000001", It.IsAny<Request.AccountsRequest>()));
        }

        #endregion RequestObjectvalueNull Methods

        #region SuccessWithnotNull

        public void VerifySuccessWithNotNull_CalculateCashFlow()
        {
            var categoryLookup = new Dictionary<string, string>();
            categoryLookup.Add("1", "application");
            Lookup.Setup(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(categoryLookup);
            var CalculateCashFlowResponse = plaidService.CalculateCashFlow("application", "CA000001", new Request.AccountsRequest
            {
                AccessToken = "",
                BankSupportedProductType = "",
                TenantId = "capital-alliance",
            }).Result;
            Assert.NotNull(CalculateCashFlowResponse.Transactions);
        }

        #endregion SuccessWithnotNull
    }
}