﻿using CreditExchange.Syndication.Plaid.Proxy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace CreditExchange.Syndication.Plaid.Test.Live
{
    public class PlaidServiceLiveTest
    {
        private IPlaidProxy plaidProxy { get; }
        private IPlaidConfiguration plaidConfiguration { get; }

        public PlaidServiceLiveTest()
        {
            plaidConfiguration = new PlaidConfiguration
            {
                APIURL = "https://tartan.plaid.com",
                ClientId = "test_id",
                IsLive = false,
                Secret = "test_secret",
                TLSVersion = "1.2",
                TransactionDays = 30,
                UseProxy = false,
                ProxyUrl = ""
            };
            plaidProxy = new PlaidProxy(plaidConfiguration);
        }

        //[Fact]
        //public void ReturnGetAccountsResponseOnValid()
        //{
        //    var response = plaidProxy.GetAccounts(new Proxy.Request.AccountsRequest()
        //    {
        //        AccessToken = "test_chase",
        //        BankSupportedProductType = "connect"
        //    });
        //    Assert.NotNull(response);
        //}
        //[Fact]
        //public void ReturnDeletetoeknResponseOnValid()
        //{
        //    var response = plaidProxy.DeleteToken(new Proxy.Request.DeleteTokenRequest()
        //    {
        //        AccessToken = "test_chase",
        //        ProductType = "connect"
        //    });

        //    Assert.NotNull(response);
        //}
    }
}