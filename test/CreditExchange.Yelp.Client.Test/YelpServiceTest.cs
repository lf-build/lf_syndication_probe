﻿using CreditExchange.Syndication.Yelp.Response;
using LendFoundry.Foundation.Services;
using Moq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace CreditExchange.Yelp.Client.Test
{
    public class YelpServiceTest
    {
        public YelpService yelpServiceClient { get; }

        private IRestRequest restRequest { get; set; }

        private Mock<IServiceClient> MockServiceClient { get; }

        public YelpServiceTest()
        {
            MockServiceClient = new Mock<IServiceClient>();
            yelpServiceClient = new YelpService(MockServiceClient.Object);
        }

        [Fact]
        public async void Client_GetBusinnesDetails()
        {
            MockServiceClient.Setup(s => s.ExecuteAsync<BusinessDetailResponse>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => restRequest = r).ReturnsAsync(new BusinessDetailResponse());
            var response = await yelpServiceClient.GetBusinessDetails("123", "123", "123", "", "", true, true);
            Assert.Equal("/{entitytype}/{entityid}/{businessId}/businesses", restRequest.Resource);
            Assert.Equal(Method.GET, restRequest.Method);
        }

        [Fact]
        public async void Client_GetPhoneDetails()
        {
            MockServiceClient.Setup(s => s.ExecuteAsync<SearchResponse>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => restRequest = r).ReturnsAsync(new SearchResponse());
            var response = await yelpServiceClient.GetPhoneDetails("123", "123", "123");
            Assert.Equal("/{entitytype}/{entityid}/businesses/search/{phoneNumber}/phone", restRequest.Resource);
            Assert.Equal(Method.GET, restRequest.Method);
        }

        [Fact]
        public async void Client_SearchDetails()
        {
            MockServiceClient.Setup(s => s.ExecuteAsync<SearchResponse>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => restRequest = r).ReturnsAsync(new SearchResponse());
            var response = await yelpServiceClient.SearchDetails("123", "123", "123", "123", 1, 1, 1, "", 1, true);
            Assert.Equal("/{entitytype}/{entityid}/businesses/search/{location}", restRequest.Resource);
            Assert.Equal(Method.GET, restRequest.Method);
        }
    }
}