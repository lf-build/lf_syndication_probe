﻿using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Syndication.Experian.Proxy;
using LendFoundry.Syndication.Experian.Proxy.PersonalReportRequest;
using LendFoundry.Syndication.Experian.Request;
using Moq;
using System;
using System.Collections.Generic;
using Xunit;

namespace LendFoundry.Syndication.Experian.Test
{
    public class PersonalReportServiceTest
    {
        private Mock<IExperianConfiguration> PersonalReportConfiguration { get; }
        private Mock<IPersonalReportProxy> PersonalReportProxy { get; }
        private PersonalReportService PersonalReportService { get; }
        private Mock<IEventHubClient> EventHub { get; }
        private Mock<ILookupService> Lookup { get; }

        public PersonalReportServiceTest()
        {
            EventHub = new Mock<IEventHubClient>();
            Lookup = new Mock<ILookupService>();
            PersonalReportConfiguration = new Mock<IExperianConfiguration>();
            PersonalReportProxy = new Mock<IPersonalReportProxy>();
            PersonalReportService = new PersonalReportService(PersonalReportConfiguration.Object, PersonalReportProxy.Object, EventHub.Object, Lookup.Object);
        }

        [Fact]
        public void GetPersonalReportResponse_ArgumentNullException()
        {
            var FirstnameNullrequest = new GetPersonalReportRequest
            {
                Firstname = "",
                Lastname = "Martin",
                Middlename = "B",
                Ssn = "1234567",
                Street = "abc",
                City = "Irvine",
                State = "CA",
                Zip = "123123"
            };

            var LastnameNullrequest = new GetPersonalReportRequest
            {
                Firstname = "Jhon",
                Lastname = "",
                Middlename = "B",
                Ssn = "1234567",
                Street = "abc",
                City = "Irvine",
                State = "CA",
                Zip = "123123"
            };
            var SsnNullrequest = new GetPersonalReportRequest
            {
                Firstname = "Jhon",
                Lastname = "Martin",
                Middlename = "B",
                Ssn = "",
                Street = "abc",
                City = "Irvine",
                State = "CA",
                Zip = "123123"
            };
            var StreetNullrequest = new GetPersonalReportRequest
            {
                Firstname = "Jhon",
                Lastname = "Martin",
                Middlename = "B",
                Ssn = "1234567",
                Street = "",
                City = "Irvine",
                State = "CA",
                Zip = "123123"
            };

            var ZipNullrequest = new GetPersonalReportRequest
            {
                Firstname = "Jhon",
                Lastname = "Martin",
                Middlename = "B",
                Ssn = "1234567",
                Street = "abc",
                City = "Irvine",
                State = "CA",
                Zip = ""
            };
            var Ssnrequest = new GetPersonalReportRequest
            {
                Firstname = "Jhon",
                Lastname = "Martin",
                Middlename = "B",
                Ssn = "12345678910",
                Street = "abc",
                City = "Irvine",
                State = "CA",
                Zip = ""
            };
            Dictionary<string, string> categoryLookup = new Dictionary<string, string>();
            categoryLookup.Add("1", "application");
            Lookup.Setup(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(categoryLookup);
            Assert.ThrowsAsync<ArgumentNullException>(() => PersonalReportService.GetPersonalReportResponse("application", "1234", It.IsAny<IPersonalReportRequest>()));
            Assert.ThrowsAsync<ArgumentException>(() => PersonalReportService.GetPersonalReportResponse("application", "1234", FirstnameNullrequest));
            Assert.ThrowsAsync<ArgumentException>(() => PersonalReportService.GetPersonalReportResponse("application", "1234", LastnameNullrequest));
            Assert.ThrowsAsync<ArgumentException>(() => PersonalReportService.GetPersonalReportResponse("application", "1234", SsnNullrequest));
            Assert.ThrowsAsync<ArgumentException>(() => PersonalReportService.GetPersonalReportResponse("application", "1234", StreetNullrequest));
            Assert.ThrowsAsync<ArgumentException>(() => PersonalReportService.GetPersonalReportResponse("application", "1234", ZipNullrequest));
            Assert.ThrowsAsync<ArgumentException>(() => PersonalReportService.GetPersonalReportResponse("application", "1234", Ssnrequest));
            NetConnectRequest personalReportProxy = new NetConnectRequest();
            PersonalReportProxy.Setup(x => x.GetPersonalReport(It.IsAny<Proxy.PersonalReportRequest.NetConnectRequest>()));
            Assert.ThrowsAsync<ArgumentNullException>(() => PersonalReportService.GetPersonalReportResponse("application", "1234", It.IsAny<GetPersonalReportRequest>()));
        }
    }
}