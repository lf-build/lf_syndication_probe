﻿using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Syndication.Experian.Proxy;
using LendFoundry.Syndication.Experian.Proxy.PersonalReportRequest;
using LendFoundry.Syndication.Experian.Request;
using Moq;
using Xunit;

namespace LendFoundry.Syndication.Experian.Test.Live
{
    public class PersonalReportServiceLiveTest
    {
        private Mock<IPersonalReportProxy> personalReportProxy { get; }
        private Mock<IPersonalReportConfiguration> personalReportConfiguration { get; }
        private Mock<IEventHubClient> EventHub { get; }
        private Mock<ILookupService> Lookup { get; }
        private ExperianConfiguration experianConfiguration { get; }
        private PersonalReportService PersonalReportService { get; }


        public PersonalReportServiceLiveTest()
        {
            EventHub = new Mock<IEventHubClient>();
            Lookup = new Mock<ILookupService>();
            personalReportProxy = new Mock<IPersonalReportProxy>();
           PersonalReportConfiguration personalReportConfiguration = new PersonalReportConfiguration
            {
                NetConnectSubscriber = "capitalalliance_bisdemo",
                NetConnectPassword = "Sigma@123",
                NetConnectRequestUrl = "http://192.168.1.67:7022/personal",
                Eai = "OXOEYRDW",
                DbHost = "BISTEST",
                ReferenceId = null,
                Preamble = "TBD1",
                OpInitials = "OP",
                SubCode = "0487130",
                ArfVersion = "07",
                VendorNumber = "BIS183",
                VendorVersion = null
            };
            experianConfiguration = new ExperianConfiguration
            {
                businessReportConfiguration = null,
                personalReportConfiguration = personalReportConfiguration
            };
            PersonalReportService = new PersonalReportService(experianConfiguration, personalReportProxy.Object, EventHub.Object, Lookup.Object);
        }

        [Fact]
        public void GetPersonalReport_Response()
        {
            var response = PersonalReportService.GetPersonalReportResponse("application","123456",new GetPersonalReportRequest
            {

                Firstname = "Peter",
                Lastname = "LINS",
                Middlename = null,
                Gen = null,
                Ssn = "666-59-%3#5",
                Street = "21598 100TH AVE",
                City = "TUSTIN",
                State = "MI",
                Zip = "496888651",
                RiskModels = "Y",
                CustomRRDashKeyword = "XXP1"

            });
            Assert.NotNull(response);
        }
    }
}