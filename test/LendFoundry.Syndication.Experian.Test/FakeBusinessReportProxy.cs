﻿using LendFoundry.Syndication.Experian.Proxy;
using LendFoundry.Syndication.Experian.Proxy.BusinessReportResponse;
using LendFoundry.Syndication.Experian.Proxy.SearchBusinessResponse;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Experian.Test
{
    public class FakeBusinessReportProxy : IBusinessReportProxy
    {
        public Proxy.SearchBusinessResponse.NetConnectResponse GetBusinessSearch(Proxy.SearchBusinessRequest.NetConnectRequest searchRequest)
        {
            if (searchRequest.Request?.Products?.PremierProfile?.BusinessApplicant != null)
                if (searchRequest.Request.Products.PremierProfile.BusinessApplicant.BusinessName == "NULL")
                    return GetFakeNullSearchResponse();
                else
                    return GetFakeSearchResponse();
            return null;
        }

        public Proxy.BusinessReportResponse.NetConnectResponse GetBusinessReport(Proxy.BusinessReportRequest.NetConnectRequest reportRequest)
        {
            if (reportRequest.Request?.Products?.ListOfSimilars?.BusinessApplicant != null)
                if (reportRequest.Request.Products.ListOfSimilars.BusinessApplicant.TransactionNumber == "NULL")
                    return GetFakeNullBusinessReportResponse();
                else
                    return GetFakeBusinessReportResponse();
            return null;
        }

        public Proxy.DirectHitReportResponse.NetConnectResponse GetDirectHitReport(Proxy.DirectHitReportRequest.NetConnectRequest reportRequest)
        {
            // if (reportRequest.Request?.Products?.PremierProfile?.BusinessApplicant != null)
            //if (reportRequest.Request.Products.PremierProfile.BusinessApplicant.BusinessName == "NULL")
            //    return GetFakeNullBusinessReportResponse();
            //else
            //    return GetFakeBusinessReportResponse();
            return null;
        }

        public static Proxy.BusinessReportResponse.NetConnectResponse GetFakeBusinessReportResponse()
        {
            var response = new Proxy.BusinessReportResponse.NetConnectResponse
            {
                CompletionCode = "0000",
                ErrorMessage = "",
                ErrorTag = "",
                ReferenceId = "Premier Profile List Example",
                TransactionId = "20566968",
                Products = new Proxy.BusinessReportResponse.Products
                {
                    BusinessProfile = new Proxy.BusinessReportResponse.NetConnectResponseProductsBusinessProfile
                    {
                        InputSummary =
                            new[]
                            {
                                new Proxy.BusinessReportResponse.NetConnectResponseProductsPremierProfileInputSummary
                                {
                                    SubscriberNumber = "156960",
                                    InquiryTransactionNumber = "C005542330",
                                    CPUVersion = "CPU006",
                                    Inquiry = "LRPT/C005542330/79674420300/BUSP/V-BIS110",
                                }
                            },
                        Bankruptcy =
                            new[]
                            {
                                new NetConnectResponseProductsPremierProfileBankruptcy
                                {
                                    AssetAmount = "2324",
                                    CustomerDispute = "34",
                                    DateFiled = "23432",
                                    DocumentNumber = "234",
                                    ExemptAmount = "554",
                                    FilingLocation = "ERT",
                                    LegalAction = new[]
                                    {
                                        new LegalAction
                                        {
                                            code = "E",
                                            Value = "LEGAL ACTION"
                                        }
                                    },
                                    LegalType = new[]
                                    {
                                        new LegalType
                                        {
                                            code = "R",
                                            Value = "LEGAL TYPE"
                                        }
                                    },
                                    LiabilityAmount = "3434",
                                    Owner = "Owner",
                                    Statement = new[]
                                    {
                                        new Statement
                                        {
                                            code = "T",
                                            Value = "Statement"
                                        }
                                    }
                                }
                            },
                        Inquiry = new[]
                        {
                            new NetConnectResponseProductsPremierProfileInquiry
                            {
                                InquiryBusinessCategory = "APPAREL",
                                InquiryCount =
                                    new[]
                                    {
                                        new NetConnectResponseProductsPremierProfileInquiryInquiryCount
                                        {
                                            Date = "20150700",
                                            Count = "000"
                                        }
                                    }
                            }
                        },
                        BusinessFacts =
                            new[]
                            {
                                new NetConnectResponseProductsPremierProfileBusinessFacts
                                {
                                    BusinessTypeIndicator =
                                        new[]
                                        {
                                            new NetConnectResponseProductsPremierProfileBusinessFactsBusinessTypeIndicator
                                            {
                                                code = "C",
                                                Value = "Corporation"
                                            },
                                        },
                                    DateOfIncorporation = "",
                                    EmployeeSize = "000000000",
                                    EmployeeSizeIndicator =
                                        new[]
                                        {
                                            new NetConnectResponseProductsPremierProfileBusinessFactsEmployeeSizeIndicator
                                            {
                                                code = "D"
                                            }
                                        },
                                    FileEstablishedDate = "19770100",
                                    FileEstablishedFlag =
                                        new[]
                                        {
                                            new NetConnectResponseProductsPremierProfileBusinessFactsFileEstablishedFlag
                                            {
                                                code = "R",
                                                Value = "Business was added prior to Jan 1977"
                                            }
                                        },
                                    NonProfitIndicator =
                                        new[]
                                        {
                                            new NetConnectResponseProductsPremierProfileBusinessFactsNonProfitIndicator
                                            {
                                                code = "P",
                                                Value = "Profit"
                                            }
                                        },
                                    PublicIndicator =
                                        new[]
                                        {
                                            new NetConnectResponseProductsPremierProfileBusinessFactsPublicIndicator
                                            {
                                                code = "S"
                                            }
                                        },
                                    SalesIndicator =
                                        new[]
                                        {
                                            new NetConnectResponseProductsPremierProfileBusinessFactsSalesIndicator
                                            {
                                                code = "W"
                                            }
                                        },
                                    SalesRevenue = "000000000000",
                                    StateOfIncorporation = "W",
                                    YearsInBusiness = "0000",
                                    YearsInBusinessIndicator =
                                        new[]
                                        {
                                            new NetConnectResponseProductsPremierProfileBusinessFactsYearsInBusinessIndicator
                                            {
                                                code = "A",
                                                Value = "Under 1 Year"
                                            }
                                        }
                                }
                            },
                        AdditionalPaymentExperiences =
                            new[]
                            {
                                new NetConnectResponseProductsPremierProfileAdditionalPaymentExperiences
                                {
                                    Terms = "VARIED",
                                    AccountBalance = new[]
                                    {
                                        new AccountBalance
                                        {
                                            Amount = "00000000",
                                            Modifier = new[]
                                            {
                                                new Modifier
                                                {
                                                    code = "R"
                                                }
                                            }
                                        }
                                    },
                                    BusinessCategory = "ADVERTISNG",
                                    Comments = "",
                                    CurrentPercentage = "000",
                                    DateLastActivity = "00000000",
                                    DateReported = "20130200",
                                    DBT30 = "000",
                                    DBT60 = "000",
                                    DBT90 = "000",
                                    DBT90Plus = "000",
                                    NewlyReportedIndicator = new[]
                                    {
                                        new NewlyReportedIndicator
                                        {
                                            code = "E"
                                        }
                                    },
                                    RecentHighCredit = new[]
                                    {
                                        new RecentHighCredit
                                        {
                                            Amount = "00000200",
                                            Modifier = new[]
                                            {
                                                new Modifier
                                                {
                                                    code = "E"
                                                }
                                            }
                                        }
                                    },
                                    TradeLineFlag = new[]
                                    {
                                        new TradeLineFlag
                                        {
                                            code = "E"
                                        }
                                    }
                                }
                            },
                        BillingIndicator =
                            new[]
                            {
                                new NetConnectResponseProductsPremierProfileBillingIndicator
                                {
                                    code = "E",
                                    Value = "Indicator"
                                }
                            },
                        CollectionData =
                            new[]
                            {
                                new NetConnectResponseProductsPremierProfileCollectionData
                                {
                                    AccountStatus =
                                        new[]
                                        {
                                            new NetConnectResponseProductsPremierProfileCollectionDataAccountStatus
                                            {
                                                code = "00",
                                                Value = "Open Account"
                                            }
                                        },
                                    AmountPaid = "00000000",
                                    AmountPlacedForCollection = "00000320",
                                    CollectionAgencyInfo =
                                        new[]
                                        {
                                            new NetConnectResponseProductsPremierProfileCollectionDataCollectionAgencyInfo
                                            {
                                                AgencyName = "RECEIVABLE MANAGEMENT SERVICES",
                                                PhoneNumber = "4842424000"
                                            }
                                        },
                                    DateClosed = "00000000",
                                    DatePlacedForCollection = "20100300"
                                }
                            },
                        CommercialFraudShieldSummary =
                            new[]
                            {
                                new NetConnectResponseProductsPremierProfileCommercialFraudShieldSummary
                                {
                                    ActiveBusinessIndicator =
                                        new[]
                                        {
                                            new NetConnectResponseProductsPremierProfileCommercialFraudShieldSummaryActiveBusinessIndicator
                                            {
                                                code = "A",
                                                Value = "Active"
                                            }
                                        },
                                    BusinessBIN = "796744203",
                                    BusinessRiskTriggersIndicator =
                                        new[]
                                        {
                                            new NetConnectResponseProductsPremierProfileCommercialFraudShieldSummaryBusinessRiskTriggersIndicator
                                            {
                                                code = "N",
                                                Value = "Risk Trigger Not Present"
                                            }
                                        },
                                    BusinessVictimStatementIndicator =
                                        new[]
                                        {
                                            new NetConnectResponseProductsPremierProfileCommercialFraudShieldSummaryBusinessVictimStatementIndicator
                                            {
                                                code = "N",
                                                Value = "No statement Present"
                                            }
                                        },
                                    MatchingBusinessIndicator =
                                        new[]
                                        {
                                            new MatchingBusinessIndicator
                                            {
                                                code = "W",
                                                Value = "Indicator"
                                            }
                                        },
                                    NameAddressVerificationIndicator =
                                        new[]
                                        {
                                            new NetConnectResponseProductsPremierProfileCommercialFraudShieldSummaryNameAddressVerificationIndicator
                                            {
                                                code = "N",
                                                Value = "Verification was not performed"
                                            }
                                        },
                                    OFACMatchCode =
                                        new[]
                                        {
                                            new NetConnectResponseProductsPremierProfileCommercialFraudShieldSummaryOFACMatchCode
                                            {
                                                code = "W"
                                            }
                                        }
                                }
                            },
                        Competitors =
                            new[]
                            {
                                new NetConnectResponseProductsPremierProfileCompetitors
                                {
                                    CompetitorName = "Name"
                                }
                            },
                        CorporateLinkage =
                            new[]
                            {
                                new NetConnectResponseProductsPremierProfileCorporateLinkage
                                {
                                    MatchingBusinessIndicator =
                                        new[]
                                        {
                                            new MatchingBusinessIndicator
                                            {
                                                code = "N",
                                                Value = "N"
                                            }
                                        },
                                    LinkageCompanyAddress = "NEWENHAM HOUSE NORTHERN CROSS MALAHIDE ROAD",
                                    LinkageCompanyCity = "DUBLIN",
                                    LinkageCompanyName = "EXPERIAN PLC",
                                    LinkageCompanyState = "",
                                    LinkageCountryCode = "IRL",
                                    LinkageRecordBIN = "000028714",
                                    LinkageRecordType =
                                        new[]
                                        {
                                            new NetConnectResponseProductsPremierProfileCorporateLinkageLinkageRecordType
                                            {
                                                code = "1",
                                                Value = "Ultimate Parent"
                                            }
                                        },
                                    ReturnLimitExceeded =
                                        new[]
                                        {
                                            new NetConnectResponseProductsPremierProfileCorporateLinkageReturnLimitExceeded
                                            {
                                                code = "N"
                                            },
                                        },
                                }
                            },
                        DoingBusinessAs =
                            new[]
                            {
                                new NetConnectResponseProductsPremierProfileDoingBusinessAs
                                {
                                    DBAName = "EXPERIAN DATA"
                                }
                            },
                        ExecutiveSummary =
                            new[]
                            {
                                new NetConnectResponseProductsPremierProfileExecutiveSummary
                                {
                                    AllIndustryDBT = "005",
                                    BusinessDBT =
                                        new[]
                                        {
                                            new NetConnectResponseProductsPremierProfileExecutiveSummaryBusinessDBT
                                            {
                                                code = "006",
                                                Value = "Potential Risk. 11% of businesses fall into this range"
                                            }
                                        },
                                    CommonTerms = "CONTRCT",
                                    CommonTerms2 = "NET 30",
                                    CommonTerms3 = "REVOLVE",
                                    CurrentTotalAccountBalance =
                                        new[]
                                        {
                                            new NetConnectResponseProductsPremierProfileExecutiveSummaryCurrentTotalAccountBalance
                                            {
                                                Amount = "00000743400",
                                                Modifier = new[]
                                                {
                                                    new Modifier
                                                    {
                                                        code = "E"
                                                    }
                                                },
                                            }
                                        },
                                    HighCreditAmountExtended = "00000657200",
                                    HighestTotalAccountBalance =
                                        new[]
                                        {
                                            new NetConnectResponseProductsPremierProfileExecutiveSummaryHighestTotalAccountBalance
                                            {
                                                Amount = "00000775100",
                                                Modifier = new[]
                                                {
                                                    new Modifier
                                                    {
                                                        code = "E"
                                                    }
                                                },
                                            }
                                        },
                                    IndustryDBT = "006",
                                    IndustryDescription = "CREDIT RPT & COLLECT",
                                    IndustryPaymentComparison =
                                        new[]
                                        {
                                            new NetConnectResponseProductsPremierProfileExecutiveSummaryIndustryPaymentComparison
                                            {
                                                code = "S",
                                                Value = "Has paid the same as similar businesses",
                                            }
                                        },
                                    LowestTotalAccountBalance =
                                        new[]
                                        {
                                            new NetConnectResponseProductsPremierProfileExecutiveSummaryLowestTotalAccountBalance
                                            {
                                                Amount = "00000088500",
                                                Modifier = new[]
                                                {
                                                    new Modifier
                                                    {
                                                        code = "W"
                                                    }
                                                }
                                            }
                                        },
                                    MedianCreditAmountExtended = "00000013000",
                                    PaymentTrendIndicator =
                                        new[]
                                        {
                                            new NetConnectResponseProductsPremierProfileExecutiveSummaryPaymentTrendIndicator
                                            {
                                                code = "S",
                                                Value = "Stable",
                                            }
                                        },
                                    PredictedDBT = "006",
                                    PredictedDBTDate = "20150916"
                                }
                            },
                        ExpandedBusinessNameAndAddress =
                            new[]
                            {
                                new NetConnectResponseProductsPremierProfileExpandedBusinessNameAndAddress
                                {
                                    Zip = "92626",
                                    PhoneNumber = "714-830-7000",
                                    State = "CA",
                                    City = "COSTA MESA",
                                    BusinessName = "EXPERIAN",
                                    CorporateLink =
                                        new[]
                                        {
                                            new NetConnectResponseProductsPremierProfileExpandedBusinessNameAndAddressCorporateLink
                                            {
                                                CorporateLinkageIndicator = "Y"
                                            }
                                        },
                                    ExperianBIN = "796744203",
                                    MatchingBranchAddress =
                                        new[]
                                        {
                                            new NetConnectResponseProductsPremierProfileExpandedBusinessNameAndAddressMatchingBranchAddress
                                            {
                                                MatchingBranchBIN = "898481533",
                                                MatchingCity = "COSTA MESA",
                                                MatchingState = "CA",
                                                MatchingStreetAddress = "475 ANTON BLVD",
                                                MatchingZip = "92626",
                                                MatchingZipExtension = "7037"
                                            }
                                        },
                                    ProfileDate = "20150724",
                                    ProfileTime = "03:02:34",
                                    ProfileType =
                                        new[]
                                        {
                                            new NetConnectResponseProductsPremierProfileExpandedBusinessNameAndAddressProfileType
                                            {
                                                code = "BUSP"
                                            }
                                        },
                                    StreetAddress = "475 ANTON BLVD",
                                    TaxID = "131134319",
                                    TerminalNumber = "AMN",
                                    WebsiteURL = "experian.com",
                                    ZipExtension = "7037"
                                }
                            },
                        ExpandedCreditSummary =
                            new[]
                            {
                                new NetConnectResponseProductsPremierProfileExpandedCreditSummary
                                {
                                    ActiveTradelineCount = "0040",
                                    AllTradelineBalance = "00000819900",
                                    AllTradelineCount = "0019",
                                    AverageBalance5Quarters = "00000511440",
                                    BankruptcyFilingCount = "0000",
                                    CollectionBalance = "00000000320",
                                    CollectionCount = "0001",
                                    CollectionCountPast24Months = "0000",
                                    CurrentAccountBalance = "00000731200",
                                    CurrentDBT = "006",
                                    CurrentTradelineCount = "0020",
                                    HighBalance6Months = "007",
                                    HighestDBT5Quarters = "031",
                                    HighestDBT6Months = "234",
                                    JudgmentFilingCount = "0009",
                                    JudgmentFlag = "Y",
                                    LegalBalance = "00001067251",
                                    LowBalance6Months = "00000088500",
                                    MonthlyAverageDBT = "006",
                                    MostRecentBankruptcyDate = "",
                                    MostRecentCollectionDate = "20100300",
                                    MostRecentJudgmentDate = "20150408",
                                    MostRecentTaxLienDate = "20150205",
                                    MostRecentUCCFilingDate = "20150703",
                                   OFACMatch =
                                        new[]
                                        {
                                            new NetConnectResponseProductsPremierProfileExpandedCreditSummaryOFACMatch
                                            {
                                                code = "OFACMatch"
                                            }
                                        },
                                    OldestCollectionDate = "20100300",
                                    OldestJudgmentDate = "20090604",
                                    OldestTaxLienDate = "20100300",
                                    OldestUCCFilingDate = "20040108",
                                    OpenCollectionBalance = "00000000320",
                                    OpenCollectionCount = "0001",
                                    SingleHighCredit = "00000657200",
                                    TaxLienFilingCount = "0005",
                                    TaxLienFlag = "Y",
                                    TradeCollectionBalance = "00000820220",
                                    TradeCollectionCount = "0041",
                                    UCCDerogCount = "0002",
                                    UCCFilings = "0014",
                                    VictimStatement =
                                        new[]
                                        {
                                            new NetConnectResponseProductsPremierProfileExpandedCreditSummaryVictimStatement
                                            {
                                                code = "N",
                                                Value = "This business does not have a Victim statement"
                                            }
                                        }
                                }
                            },
                        UCCFilings =
                            new[]
                            {
                                new NetConnectResponseProductsPremierProfileUCCFilings
                                {
                                    DocumentNumber = "157473463080",
                                    LegalType = new[]
                                    {
                                        new LegalType
                                        {
                                            code = "08",
                                            Value = "UCC"
                                        }
                                    },
                                    DateFiled = "20150703",
                                    FilingLocation = "SEC OF STATE CA",
                                    LegalAction = new[]
                                    {
                                        new LegalAction
                                        {
                                            code = "01",
                                            Value = "Filed"
                                        }
                                    },
                                    CollateralCodes =
                                        new NetConnectResponseProductsPremierProfileUCCFilingsCollateralCodesCollateral
                                        {
                                            Collateral =
                                                new[]
                                                {
                                                    new NetConnectResponseProductsPremierProfileUCCFilingsCollateralCodesCollateralValues
                                                    {
                                                        code = "9 ",
                                                        Value = "AFTER ACQUIRED PROP"
                                                    }
                                                }
                                        },
                                    OriginalUCCFilingsInfo =
                                        new[]
                                        {
                                            new NetConnectResponseProductsPremierProfileUCCFilingsOriginalUCCFilingsInfo
                                            {
                                                LegalAction = new[]
                                                {
                                                    new LegalAction
                                                    {
                                                        code = "08",
                                                        Value = "TERMINATED"
                                                    }
                                                },
                                                DateFiled = "20140508",
                                                DocumentNumber = "019259366",
                                                FilingState = "IL"
                                            }
                                        },
                                    SecuredParty = "MILANZZ KILIMANJJARO CA FRESNO 93775 P.O.BOX 11777"
                                }
                            },
                        IndustryPaymentTrends =
                            new[]
                            {
                                new NetConnectResponseProductsPremierProfileIndustryPaymentTrends
                                {
                                    SIC = "",
                                    CurrentMonth = new[]
                                    {
                                        new CurrentMonth
                                        {
                                            Date = "20100300",
                                            DBT60 = "000",
                                            DBT30 = "000",
                                            CurrentPercentage = "20",
                                            DBT90 = "000",
                                            DBT = "000",
                                            DBT120 = "000",
                                            TotalAccountBalance = new[]
                                            {
                                                new TotalAccountBalance
                                                {
                                                    Amount = "2324",
                                                    Modifier = new[]
                                                    {
                                                        new Modifier
                                                        {
                                                            code = "R"
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    },
                                    PriorMonth = new[]
                                    {
                                        new PriorMonth
                                        {
                                            Date = "20100300",
                                            DBT60 = "000",
                                            DBT30 = "000",
                                            CurrentPercentage = "20",
                                            DBT90 = "000",
                                            TotalAccountBalance = new[]
                                            {
                                                new TotalAccountBalance
                                                {
                                                    Amount = "234",
                                                    Modifier = new[]
                                                    {
                                                        new Modifier
                                                        {
                                                            code = "W",
                                                        }
                                                    }
                                                }
                                            },
                                            DBT = "224",
                                            DBT120 = "333"
                                        }
                                    },
                                }
                            },
                        ScoreTrendsCreditLimit =
                            new[]
                            {
                                new NetConnectResponseProductsPremierProfileScoreTrendsCreditLimit
                                {
                                    CreditLimitAmount = "00000311800",
                                    MostRecentQuarter = new[]
                                    {
                                        new MostRecentQuarter
                                        {
                                            DBT = "123",
                                            CurrentPercentage = "23",
                                            DBT30 = "000",
                                            DBT60 = "000",
                                            DBT90 = "000",
                                            DBT120 = "000",
                                            YearOfQuarter = "2232",
                                            Quarter = "APR-JUN",
                                            Score = "017",
                                            TotalAccountBalance = new[]
                                            {
                                                new TotalAccountBalance
                                                {
                                                    Amount = "2123122",
                                                    Modifier = new[]
                                                    {
                                                        new Modifier
                                                        {
                                                            code = "W",
                                                        }
                                                    },
                                                }
                                            },
                                            QuarterWithinYear = new[]
                                            {
                                                new QuarterWithinYear
                                                {
                                                    code = "W",
                                                    Value = "WERR",
                                                }
                                            },
                                        }
                                    },
                                    PriorQuarter = new[]
                                    {
                                        new PriorQuarter
                                        {
                                            Date = "11123",
                                            DBT = "123",
                                            CurrentPercentage = "00",
                                            DBT30 = "000",
                                            DBT60 = "000",
                                            DBT90 = "000",
                                            YearOfQuarter = "89",
                                            Quarter = "AUG-OCT",
                                            Score = "042",
                                            TotalAccountBalance = new[]
                                            {
                                                new TotalAccountBalance
                                                {
                                                    Amount = "",
                                                    Modifier = new[]
                                                    {
                                                        new Modifier
                                                        {
                                                            code = "T",
                                                        }
                                                    },
                                                }
                                            },
                                            QuarterWithinYear = new[]
                                            {
                                                new QuarterWithinYear
                                                {
                                                    code = "T",
                                                    Value = "Year",
                                                }
                                            },
                                        }
                                    },
                                }
                            },
                        ScoreFactors =
                            new[]
                            {
                                new NetConnectResponseProductsPremierProfileScoreFactors
                                {
                                    ModelCode = "000223",
                                    ScoreFactor =
                                        new[]
                                        {
                                            new NetConnectResponseProductsPremierProfileScoreFactorsScoreFactor
                                            {
                                                code = "001",
                                                Value = "NUMBER OF COMMERCIAL COLLECTION ACCOUNTS",
                                            }
                                        },
                                }
                            },
                        IntelliscoreScoreInformation =
                            new[]
                            {
                                new NetConnectResponseProductsPremierProfileIntelliscoreScoreInformation
                                {
                                    Filler = "3",
                                    ProfileNumber = "I005542330",
                                    PercentileRanking = "012",
                                    Action = "MEDIUM TO HIGH RISK",
                                    RiskClass = "4",
                                    PubliclyHeldCompany =
                                        new[]
                                        {
                                            new NetConnectResponseProductsPremierProfileIntelliscoreScoreInformationPubliclyHeldCompany
                                            {
                                                code = "N",
                                            }
                                        },
                                    LimitedProfile =
                                        new[]
                                        {
                                            new NetConnectResponseProductsPremierProfileIntelliscoreScoreInformationLimitedProfile
                                            {
                                                code = "N",
                                            }
                                        },
                                    ScoreInfo =
                                        new[]
                                        {
                                            new NetConnectResponseProductsPremierProfileIntelliscoreScoreInformationScoreInfo
                                            {
                                                Score = "0000001800",
                                            }
                                        },
                                    ModelInformation =
                                        new[]
                                        {
                                            new NetConnectResponseProductsPremierProfileIntelliscoreScoreInformationModelInformation
                                            {
                                                ModelCode = "000223",
                                                ModelTitle = "FINANCIAL STABILITY RISK",
                                                CustomModelCode = "03",
                                            }
                                        },
                                }
                            },
                        NAICSCodes =
                            new[]
                            {
                                new Proxy.BusinessReportResponse.NetConnectResponseProductsPremierProfileNAICSCodesNAICS
                                {
                                    NAICS =
                                        new Proxy.BusinessReportResponse.
                                            NetConnectResponseProductsPremierProfileNAICSCodesNAICSValues
                                        {
                                            code = "",
                                            Value = "",
                                        },
                                }
                            },
                        SICCodes =
                            new[]
                            {
                                new Proxy.BusinessReportResponse.NetConnectResponseProductsPremierProfileSICCodesSIC
                                {
                                    SIC =
                                        new Proxy.BusinessReportResponse.
                                            NetConnectResponseProductsPremierProfileSICCodesSICValues
                                        {
                                            code = "7323",
                                            Value = "CREDIT REPORTING SERVICES",
                                        },
                                }
                            },
                        KeyPersonnelExecutiveInformation =
                            new[]
                            {
                                new NetConnectResponseProductsPremierProfileKeyPersonnelExecutiveInformation
                                {
                                    Name = "CRAIG BOUNDY",
                                    NameFlag =
                                        new[]
                                        {
                                            new NetConnectResponseProductsPremierProfileKeyPersonnelExecutiveInformationNameFlag
                                            {
                                                code = "0",
                                                Value = "Name Fielded into First, Middle and Last",
                                            }
                                        },
                                    Title =
                                        new[]
                                        {
                                            new NetConnectResponseProductsPremierProfileKeyPersonnelExecutiveInformationTitle
                                            {
                                                code = "CEO",
                                                Value = "CHIEF EXECUTIVE OFFICER",
                                            }
                                        },
                                }
                            },
                        UCCFilingsSummaryCounts =
                            new[]
                            {
                                new NetConnectResponseProductsPremierProfileUCCFilingsSummaryCounts
                                {
                                    UCCFilingsTotal = "00037",
                                    MostRecent6Months =
                                        new[]
                                        {
                                            new NetConnectResponseProductsPremierProfileUCCFilingsSummaryCountsMostRecent6Months
                                            {
                                                StartDate = "20150701",
                                                FilingsTotal = "001",
                                                FilingsWithDerogatoryCollateral = "000",
                                                ReleasesAndTerminationsTotal = "000",
                                                ContinuationsTotal = "000",
                                                AmendedAndAssignedTotal = "000",
                                            }
                                        },
                                    Previous6Months =
                                        new[]
                                        {
                                            new NetConnectResponseProductsPremierProfileUCCFilingsSummaryCountsPrevious6Months
                                            {
                                                StartDate = "20150101",
                                                FilingsTotal = "000",
                                                FilingsWithDerogatoryCollateral = "000",
                                                ReleasesAndTerminationsTotal = "000",
                                                ContinuationsTotal = "000",
                                                AmendedAndAssignedTotal = "000",
                                            }
                                        },
                                }
                            },
                        JudgmentOrAttachmentLien =
                            new[]
                            {
                                new NetConnectResponseProductsPremierProfileJudgmentOrAttachmentLien
                                {
                                    DateFiled = "20150408",
                                    DocumentNumber = "SCS131440",
                                    FilingLocation = "SOUTHERN BRANCH MUNCICPAL COURT",
                                    LiabilityAmount = "00000002500",
                                    PlaintiffName = "GEORGE MARDIKIAN",
                                    LegalType = new[]
                                    {
                                        new LegalType
                                        {
                                            code = "05",
                                            Value = "Judgment",
                                        }
                                    },
                                    LegalAction = new[]
                                    {
                                        new LegalAction
                                        {
                                            code = "01",
                                            Value = "Filed",
                                        }
                                    },
                                }
                            },
                        TaxLien = new[]
                        {
                            new NetConnectResponseProductsPremierProfileTaxLien
                            {
                                DateFiled = "20141205",
                                DocumentNumber = "10257128",
                                FilingLocation = "MARION CNTY CIRCUIT/SUPERIOR/MUNICIPAL C",
                                Owner = "STATE OF INDIANA",
                                LiabilityAmount = "00000000212",
                                LegalType = new[]
                                {
                                    new LegalType
                                    {
                                        code = "03",
                                        Value = "State Tax Lien",
                                    }
                                },
                                LegalAction = new[]
                                {
                                    new LegalAction
                                    {
                                        code = "07",
                                        Value = "Released",
                                    }
                                },
                            }
                        },
                        QuarterlyPaymentTrends =
                            new[]
                            {
                                new NetConnectResponseProductsPremierProfileQuarterlyPaymentTrends
                                {
                                    MostRecentQuarter = new[]
                                    {
                                        new MostRecentQuarter
                                        {
                                            DBT = "006",
                                            CurrentPercentage = "062",
                                            DBT30 = "000",
                                            DBT60 = "000",
                                            DBT90 = "000",
                                            DBT120 = "000",
                                            YearOfQuarter = "20140000",
                                            Quarter = "",
                                            Score = "",
                                            TotalAccountBalance = new[]
                                            {
                                                new TotalAccountBalance
                                                {
                                                    Amount = "00000749500",
                                                    Modifier = new[]
                                                    {
                                                        new Modifier
                                                        {
                                                            code = "W",
                                                        }
                                                    },
                                                }
                                            },
                                            QuarterWithinYear = new[]
                                            {
                                                new QuarterWithinYear
                                                {
                                                    code = "2",
                                                    Value = "Second quarter (Apr, May & Jun)",
                                                }
                                            },
                                        }
                                    },
                                    PriorQuarter = new[]
                                    {
                                        new PriorQuarter
                                        {
                                            Date = "20150700",
                                            DBT = "006",
                                            CurrentPercentage = "062",
                                            DBT30 = "000",
                                            DBT60 = "000",
                                            DBT90 = "000",
                                            YearOfQuarter = "20140000",
                                            Quarter = "1232",
                                            Score = "23",
                                            TotalAccountBalance = new[]
                                            {
                                                new TotalAccountBalance
                                                {
                                                    Amount = "24223",
                                                    Modifier = new[]
                                                    {
                                                        new Modifier
                                                        {
                                                            code = "W",
                                                        }
                                                    },
                                                }
                                            },
                                            QuarterWithinYear = new[]
                                            {
                                                new QuarterWithinYear
                                                {
                                                    code = "W",
                                                    Value = "Year",
                                                }
                                            },
                                        }
                                    },
                                }
                            },
                        PaymentTrends =
                            new[]
                            {
                                new NetConnectResponseProductsPremierProfilePaymentTrends
                                {
                                    CurrentMonth = new[]
                                    {
                                        new CurrentMonth
                                        {
                                            Date = "20150700",
                                            DBT = "006",
                                            CurrentPercentage = "062",
                                            DBT30 = "000",
                                            DBT60 = "000",
                                            DBT90 = "000",
                                            DBT120 = "000",
                                            TotalAccountBalance = new[]
                                            {
                                                new TotalAccountBalance
                                                {
                                                    Amount = "00000731200",
                                                    Modifier = new[]
                                                    {
                                                        new Modifier
                                                        {
                                                            code = "E",
                                                        }
                                                    },
                                                }
                                            },
                                        }
                                    },
                                    PriorMonth = new[]
                                    {
                                        new PriorMonth
                                        {
                                            Date = "20150700",
                                            DBT = "006",
                                            CurrentPercentage = "062",
                                            DBT30 = "062",
                                            DBT60 = "062",
                                            DBT90 = "062",
                                            DBT120 = "062",
                                            TotalAccountBalance = new[]
                                            {
                                                new TotalAccountBalance
                                                {
                                                    Amount = "20150700",
                                                    Modifier = new[]
                                                    {
                                                        new Modifier
                                                        {
                                                            code = "W",
                                                        }
                                                    },
                                                }
                                            },
                                        }
                                    },
                                }
                            },
                        PaymentTotals =
                            new[]
                            {
                                new NetConnectResponseProductsPremierProfilePaymentTotals
                                {
                                    NewlyReportedTradeLines =
                                        new[]
                                        {
                                            new NetConnectResponseProductsPremierProfilePaymentTotalsNewlyReportedTradeLines
                                            {
                                                NumberOfLines = "0001",
                                                DBT = "000",
                                                CurrentPercentage = "100",
                                                DBT30 = "000",
                                                DBT60 = "000",
                                                DBT90 = "000",
                                                DBT120 = "000",
                                                TotalHighCreditAmount =
                                                    new[]
                                                    {
                                                        new TotalHighCreditAmount
                                                        {
                                                            Amount = "00000013000",
                                                            Modifier = new[]
                                                            {
                                                                new Modifier
                                                                {
                                                                    code = "E",
                                                                }
                                                            },
                                                        }
                                                    },
                                                TotalAccountBalance =
                                                    new[]
                                                    {
                                                        new TotalAccountBalance
                                                        {
                                                            Amount = "00000013000",
                                                            Modifier = new[]
                                                            {
                                                                new Modifier
                                                                {
                                                                    code = "R",
                                                                }
                                                            },
                                                        }
                                                    },
                                            }
                                        },
                                    ContinouslyReportedTradeLines =
                                        new[]
                                        {
                                            new NetConnectResponseProductsPremierProfilePaymentTotalsContinouslyReportedTradeLines
                                            {
                                                NumberOfLines = "0019",
                                                DBT = "006",
                                                CurrentPercentage = "",
                                                DBT30 = "038",
                                                DBT60 = "000",
                                                DBT90 = "000",
                                                TotalHighCreditAmount =
                                                    new[]
                                                    {
                                                        new TotalHighCreditAmount
                                                        {
                                                            Amount = "00001239500",
                                                            Modifier = new[]
                                                            {
                                                                new Modifier
                                                                {
                                                                    code = "E",
                                                                }
                                                            },
                                                        }
                                                    },
                                                TotalAccountBalance =
                                                    new[]
                                                    {
                                                        new TotalAccountBalance
                                                        {
                                                            Amount = "00001239500",
                                                            Modifier = new[]
                                                            {
                                                                new Modifier
                                                                {
                                                                    code = "W",
                                                                }
                                                            },
                                                        }
                                                    },
                                            }
                                        },
                                    CombinedTradeLines =
                                        new[]
                                        {
                                            new NetConnectResponseProductsPremierProfilePaymentTotalsCombinedTradeLines
                                            {
                                                NumberOfLines = "0020",
                                                DBT = "006",
                                                CurrentPercentage = "062",
                                                DBT30 = "038",
                                                DBT60 = "000",
                                                DBT90 = "000",
                                                DBT120 = "000",
                                                TotalHighCreditAmount =
                                                    new[]
                                                    {
                                                        new TotalHighCreditAmount
                                                        {
                                                            Amount = "00001252500",
                                                            Modifier = new[]
                                                            {
                                                                new Modifier
                                                                {
                                                                    code = "W",
                                                                }
                                                            },
                                                        }
                                                    },
                                                TotalAccountBalance =
                                                    new[]
                                                    {
                                                        new TotalAccountBalance
                                                        {
                                                            Amount = "E",
                                                            Modifier = new[]
                                                            {
                                                                new Modifier
                                                                {
                                                                    code = "00000744200",
                                                                }
                                                            },
                                                        }
                                                    },
                                            }
                                        },
                                    AdditionalTradeLines =
                                        new[]
                                        {
                                            new NetConnectResponseProductsPremierProfilePaymentTotalsAdditionalTradeLines
                                            {
                                                NumberOfLines = "0020",
                                                CurrentPercentage = "013",
                                                DBT30 = "000",
                                                DBT60 = "087",
                                                DBT90 = "000",
                                                DBT120 = "000",
                                                TotalHighCreditAmount =
                                                    new[]
                                                    {
                                                        new TotalHighCreditAmount
                                                        {
                                                            Amount = "00000263300",
                                                            Modifier = new[]
                                                            {
                                                                new Modifier
                                                                {
                                                                    code = "R",
                                                                }
                                                            },
                                                        }
                                                    },
                                                TotalAccountBalance =
                                                    new[]
                                                    {
                                                        new TotalAccountBalance
                                                        {
                                                            Amount = "00000075700",
                                                            Modifier = new[]
                                                            {
                                                                new Modifier
                                                                {
                                                                    code = "E",
                                                                }
                                                            },
                                                        }
                                                    },
                                            }
                                        },
                                    TradeLines =
                                        new[]
                                        {
                                            new NetConnectResponseProductsPremierProfilePaymentTotalsTradeLines
                                            {
                                                NumberOfLines = "0040",
                                                CurrentPercentage = "058",
                                                DBT30 = "034",
                                                DBT60 = "008",
                                                DBT90 = "008",
                                                DBT120 = "008",
                                                TotalHighCreditAmount =
                                                    new[]
                                                    {
                                                        new TotalHighCreditAmount
                                                        {
                                                            Amount = "00001515800",
                                                            Modifier = new[]
                                                            {
                                                                new Modifier
                                                                {
                                                                    code = "W",
                                                                }
                                                            },
                                                        }
                                                    },
                                                TotalAccountBalance =
                                                    new[]
                                                    {
                                                        new TotalAccountBalance
                                                        {
                                                            Amount = "00000819900",
                                                            Modifier = new[]
                                                            {
                                                                new Modifier
                                                                {
                                                                    code = "W",
                                                                }
                                                            },
                                                        }
                                                    },
                                            }
                                        },
                                }
                            },
                        TradePaymentExperiences =
                            new[]
                            {
                                new NetConnectResponseProductsPremierProfileTradePaymentExperiences
                                {
                                    BusinessCategory = "ACCT SVCS",
                                    DateReported = "20150700",
                                    DateLastActivity = "00000000",
                                    Terms = "VARIED ",
                                    CurrentPercentage = "000",
                                    DBT30 = "000",
                                    DBT60 = "000",
                                    DBT90 = "000",
                                    DBT90Plus = "000",
                                    Comments = "",
                                    PaymentIndicator =
                                        new[]
                                        {
                                            new NetConnectResponseProductsPremierProfileTradePaymentExperiencesPaymentIndicator
                                            {
                                                code = "W",
                                            }
                                        },
                                    RecentHighCredit = new[]
                                    {
                                        new RecentHighCredit
                                        {
                                            Amount = "00000700",
                                            Modifier = new[]
                                            {
                                                new Modifier
                                                {
                                                    code = "E",
                                                }
                                            },
                                        }
                                    },
                                    AccountBalance = new[]
                                    {
                                        new AccountBalance
                                        {
                                            Amount = "00000000",
                                            Modifier = new[]
                                            {
                                                new Modifier
                                                {
                                                    code = "W",
                                                }
                                            },
                                        }
                                    },
                                    TradeLineFlag = new[]
                                    {
                                        new TradeLineFlag
                                        {
                                            code = "W",
                                        }
                                    },
                                    NewlyReportedIndicator = new[]
                                    {
                                        new NewlyReportedIndicator
                                        {
                                            code = "W",
                                        }
                                    },
                                }
                            },
                    }
                }
            };

            return response;
        }

        public static Proxy.BusinessReportResponse.NetConnectResponse GetFakeNullBusinessReportResponse()
        {
            var response = new Proxy.BusinessReportResponse.NetConnectResponse
            {
                CompletionCode = "0000",
                ErrorMessage = "",
                ErrorTag = "",
                ReferenceId = "Premier Profile List Example",
                TransactionId = "20566968",
                Products = new Proxy.BusinessReportResponse.Products
                {
                    BusinessProfile = new Proxy.BusinessReportResponse.NetConnectResponseProductsBusinessProfile
                    {
                        ProcessingMessage = null,
                        InputSummary = null,
                        Bankruptcy = null,
                        Inquiry = null,
                        BusinessFacts = null,
                        AdditionalPaymentExperiences = null,
                        BillingIndicator = null,
                        CollectionData = null,
                        CommercialFraudShieldSummary = null,
                        Competitors = null,
                        CorporateLinkage = null,
                        DoingBusinessAs = null,
                        ExecutiveSummary = null,
                        ExpandedBusinessNameAndAddress = null,
                        ExpandedCreditSummary = null,
                        UCCFilings = null,
                        IndustryPaymentTrends = null,
                        ScoreTrendsCreditLimit = null,
                        ScoreFactors = null,
                        IntelliscoreScoreInformation = null,
                        NAICSCodes = null,
                        SICCodes = null,
                        KeyPersonnelExecutiveInformation = null,
                        UCCFilingsSummaryCounts = null,
                        JudgmentOrAttachmentLien = null,
                        TaxLien = null,
                        QuarterlyPaymentTrends = null,
                        PaymentTrends = null,
                        PaymentTotals = null,
                        TradePaymentExperiences = null
                    }
                }
            };

            return response;
        }

        public static Proxy.SearchBusinessResponse.NetConnectResponse GetFakeSearchResponse()
        {
            var response = new Proxy.SearchBusinessResponse.NetConnectResponse
            {
                CompletionCode = "0000",
                ErrorMessage = "",
                ErrorTag = "",
                TransactionId = "20566818",
                IsSucceeded = true,
                Id = 0,
                ReferenceId = "Premier Profile List Example",
                Message = new List<string>()
                {
                    "Param1",
                    "Param2"
                },
                Products = new Proxy.SearchBusinessResponse.Products
                {
                    PremierProfile = new List<Proxy.SearchBusinessResponse.NetConnectResponseProductsPremierProfile>
                    {
                        new  Proxy.SearchBusinessResponse.NetConnectResponseProductsPremierProfile
                        {
                            BillingIndicator = new  BillingIndicator
                            {
                                Code = "A",
                                Text = "Access Charge"
                            },
                            BusinessNameAndAddress =
                                new[]
                                {
                                    new  NetConnectResponseProductsPremierProfileBusinessNameAndAddress
                                    {
                                        ProfileType = new  ProfileType
                                        {
                                            Code = "LIST",
                                            Text = "List of Similars"
                                        },
                                        TerminalNumber = "AMB",
                                        ProfileTime = "02:56:56",
                                        ProfileDate = "20150724",
                                        FileEstablishFlag = new  FileEstablishFlag
                                        {
                                            code = "E"
                                        }
                                    }
                                },
                            InputSummary = new[]
                            {
                                new  Proxy.SearchBusinessResponse.NetConnectResponseProductsPremierProfileInputSummary
                                {
                                    Inquiry =
                                        "NARQ/EXPERIAN INFORMATION SOLUTIONS;CA-475 ANTON BLVD/COSTA MESA CA 92626/LIST/BUSP/V-BIS110",
                                    CPUVersion = "CPU006",
                                    InquiryTransactionNumber = "C005716745",
                                    SubscriberNumber = "156960"
                                }
                            },
                            ListOfSimilars = new List<NetConnectResponseProductsPremierProfileListOfSimilars>
                            {
                                new  NetConnectResponseProductsPremierProfileListOfSimilars
                                {
                                    Zip = "926267037",
                                    State = "CA",
                                    City = "COSTA MESA",
                                    PhoneNumber = "7148307000",
                                    BusinessName = "EXPERIAN",
                                    StreetAddress = "475 ANTON BLVD",
                                    BusinessAssociationType = new  BusinessAssociationType
                                    {
                                        Code = "1",
                                        Text = "Primary Business"
                                    },
                                    DataContent =
                                        new NetConnectResponseProductsPremierProfileListOfSimilarsDataContent
                                        {
                                            FileEstablishFlag = new  FileEstablishFlag
                                            {
                                                code = "P",
                                                Text = "Business was added prior to Jan 1977"
                                            },
                                            BankDataIndicator = "N",
                                            CollectionIndicator = "Y",
                                            ExecutiveSummaryIndicator = "N",
                                            FileEstablishDate = "19770100",
                                            GovernmentDataIndicator = "N",
                                            IndustryPremierIndicator = "Y",
                                            InquiryIndicator = "N",
                                            KeyFactsIndicator = "Y",
                                            NumberOfTradeLines = "040",
                                            PublicRecordDataIndicator = "N",
                                            SAndPIndicator = "Y",
                                            UCCIndicator = "N"
                                        },
                                    ExperianFileNumber = "796744203",
                                    RecordSequenceNumber = "00"
                                }
                            }
                        }
                    }
                }
            };
            return response;
        }

        public static Proxy.SearchBusinessResponse.NetConnectResponse GetFakeNullSearchResponse()
        {
            var response = new Proxy.SearchBusinessResponse.NetConnectResponse
            {
                CompletionCode = "0000",
                ErrorMessage = "",
                ErrorTag = "",
                TransactionId = "20566818",
                IsSucceeded = true,
                Id = 0,
                ReferenceId = "Premier Profile List Example",
                Message = new List<string>()
                {
                    "Param1",
                    "Param2"
                },
                Products = new Proxy.SearchBusinessResponse.Products
                {
                    PremierProfile = new List<Proxy.SearchBusinessResponse.NetConnectResponseProductsPremierProfile>
                    {
                        new  Proxy.SearchBusinessResponse.NetConnectResponseProductsPremierProfile
                        {
                            BillingIndicator = null,
                            BusinessNameAndAddress =null,
                            InputSummary = null,
                            ListOfSimilars = null
                        }
                    }
                }
            };
            return response;
        }

        //    public static Proxy.DirectHitReportResponse.NetConnectResponse GetFakeDirectHitResponse()
        //    {
        //        var response = new Proxy.DirectHitReportResponse.NetConnectResponse
        //        {
        //            CompletionCode = "0000",
        //            ErrorMessage = "",
        //            ErrorTag = "",
        //            ReferenceId = "Premier Profile List Example",
        //            TransactionId = "20566968",
        //            Products = new Proxy.DirectHitReportResponse.Products
        //            {
        //                PremierProfile = new Proxy.DirectHitReportResponse.NetConnectResponseProductsBusinessProfile
        //                {
        //                    InputSummary =
        //                        new[]
        //                        {
        //                            new Proxy.DirectHitReportResponse.NetConnectResponseProductsPremierProfileInputSummary
        //                            {
        //                                SubscriberNumber = "156960",
        //                                InquiryTransactionNumber = "C005542330",
        //                                CPUVersion = "CPU006",
        //                                Inquiry = "LRPT/C005542330/79674420300/BUSP/V-BIS110",
        //                            }
        //                        },
        //                    ExpandedCreditSummary =
        //                        new[]
        //                        {
        //                            new NetConnectResponseProductsPremierProfileExpandedCreditSummary
        //                            {
        //                                    BankruptcyFilingCount= "0035",
        //                                    TaxLienFilingCount= "0001",
        //                                    OldestTaxLienDate= "20100306",
        //                                    MostRecentTaxLienDate= "20100306",
        //                                    JudgmentFilingCount= "00001",
        //                                    OldestJudgmentDate= "20100610",
        //                                    MostRecentJudgmentDate= "20100610",
        //                                    CollectionCount= "0002",
        //                                    CollectionBalance= "00000000190",
        //                                    CollectionCountPast24Months= "0000",
        //                                    LegalBalance= "00001810766",
        //                                    UCCFilings="0001",
        //                                    UCCDerogCount="0000",
        //                                    CurrentAccountBalance= "00000000300",
        //                                    CurrentTradelineCount= "0006",
        //                                    MonthlyAverageDBT="105",
        //                                    HighestDBT6Months= "105",
        //                                    HighestDBT5Quarters= "105",
        //                                    ActiveTradelineCount= "0006",
        //                                    AllTradelineBalance= "00000044000",
        //                                    AllTradelineCount= "0013",
        //                                    AverageBalance5Quarters="00000000500",
        //                                    SingleHighCredit="00000000400",
        //                                    LowBalance6Months="00000000000",
        //                                    HighBalance6Months="00000000000",
        //                                    OldestCollectionDate="20091100",
        //                                    MostRecentCollectionDate= "20111000",
        //                                    CurrentDBT= "102",
        //                                    OldestUCCFilingDate= "20060828",
        //                                    MostRecentUCCFilingDate= "20060828",
        //                                    JudgmentFlag= null,
        //                                    TaxLienFlag= "Y",
        //                                    TradeCollectionCount= "0015",
        //                                    TradeCollectionBalance= "00000044190",
        //                                    OpenCollectionCount= "0001",
        //                                    OpenCollectionBalance= "00000001980",
        //                                    MostRecentBankruptcyDate= "20101117",
        //                      OFACMatch = new[]
        //                                {
        //                                    new NetConnectResponseProductsPremierProfileExpandedCreditSummaryOFACMatch
        //                                    {
        //                                        code = "R",
        //                                    }
        //                                },
        //                      VictimStatement=new[]
        //                                {
        //                                      new NetConnectResponseProductsPremierProfileExpandedCreditSummaryVictimStatement
        //                                    {
        //                                        code = "T",
        //                                        Value = "Statement"
        //                                    }
        //                                }
        //                        },

        //                                VictimStatement = new[]
        //                                {
        //                                }
        //                            }
        //                },
        //                ExpandedBusinessNameAndAddress = new[]
        //                    {
        //                        new NetConnectResponseProductsPremierProfileExpandedBusinessNameAndAddress
        //                        {
        //    ExperianBIN= "700401236",
        //    ProfileDate= "20170308",
        //    ProfileTime= "23=03=19",
        //    TerminalNumber= "-AD2",
        //    BusinessName= "CHARLIE BROWNS OF TINTON FALLS INC",
        //    StreetAddress= "1450 US HIGHWAY 22 STE 4",
        //    city= MOUNTAINSIDE,
        //    state= NJ,
        //    zip= 07092,
        //    zipExtension= 2619,
        //    phoneNumber= 908-518-1520,
        //    taxId= null,
        //    websiteUrl= http=//www.charliebrowns.com,
        //    profileType= [
        //      {
        //        code= BUSP
        //      }
        //    ],
        //    matchingBranchAddress= null,
        //    corporateLink= [
        //      {
        //        corporateLinkageIndicator= Y
        //      }
        //    ]
        //                        }
        //                    },
        //                BusinessFacts =
        //                        new[]
        //                        {
        //                            new NetConnectResponseProductsPremierProfileBusinessFacts
        //                            {
        //                                BusinessTypeIndicator =
        //                                    new[]
        //                                    {
        //                                        new NetConnectResponseProductsPremierProfileBusinessFactsBusinessTypeIndicator
        //                                        {
        //                                            code = "C",
        //                                            Value = "Corporation"
        //                                        },
        //                                    },
        //                                DateOfIncorporation = "",
        //                                EmployeeSize = "000000000",
        //                                EmployeeSizeIndicator =
        //                                    new[]
        //                                    {
        //                                        new NetConnectResponseProductsPremierProfileBusinessFactsEmployeeSizeIndicator
        //                                        {
        //                                            code = "D"
        //                                        }
        //                                    },
        //                                FileEstablishedDate = "19770100",
        //                                FileEstablishedFlag =
        //                                    new[]
        //                                    {
        //                                        new NetConnectResponseProductsPremierProfileBusinessFactsFileEstablishedFlag
        //                                        {
        //                                            code = "R",
        //                                            Value = "Business was added prior to Jan 1977"
        //                                        }
        //                                    },
        //                                NonProfitIndicator =
        //                                    new[]
        //                                    {
        //                                        new NetConnectResponseProductsPremierProfileBusinessFactsNonProfitIndicator
        //                                        {
        //                                            code = "P",
        //                                            Value = "Profit"
        //                                        }
        //                                    },
        //                                PublicIndicator =
        //                                    new[]
        //                                    {
        //                                        new NetConnectResponseProductsPremierProfileBusinessFactsPublicIndicator
        //                                        {
        //                                            code = "S"
        //                                        }
        //                                    },
        //                                SalesIndicator =
        //                                    new[]
        //                                    {
        //                                        new NetConnectResponseProductsPremierProfileBusinessFactsSalesIndicator
        //                                        {
        //                                            code = "W"
        //                                        }
        //                                    },
        //                                SalesRevenue = "000000000000",
        //                                StateOfIncorporation = "W",
        //                                YearsInBusiness = "0000",
        //                                YearsInBusinessIndicator =
        //                                    new[]
        //                                    {
        //                                        new NetConnectResponseProductsPremierProfileBusinessFactsYearsInBusinessIndicator
        //                                        {
        //                                            code = "A",
        //                                            Value = "Under 1 Year"
        //                                        }
        //                                    }
        //                            }
        //                        },
        //                AdditionalPaymentExperiences =
        //                        new[]
        //                        {
        //                            new NetConnectResponseProductsPremierProfileAdditionalPaymentExperiences
        //                            {
        //                                Terms = "VARIED",
        //                                AccountBalance = new[]
        //                                {
        //                                    new AccountBalance
        //                                    {
        //                                        Amount = "00000000",
        //                                        Modifier = new[]
        //                                        {
        //                                            new Modifier
        //                                            {
        //                                                code = "R"
        //                                            }
        //                                        }
        //                                    }
        //                                },
        //                                BusinessCategory = "ADVERTISNG",
        //                                Comments = "",
        //                                CurrentPercentage = "000",
        //                                DateLastActivity = "00000000",
        //                                DateReported = "20130200",
        //                                DBT30 = "000",
        //                                DBT60 = "000",
        //                                DBT90 = "000",
        //                                DBT90Plus = "000",
        //                                NewlyReportedIndicator = new[]
        //                                {
        //                                    new NewlyReportedIndicator
        //                                    {
        //                                        code = "E"
        //                                    }
        //                                },
        //                                RecentHighCredit = new[]
        //                                {
        //                                    new RecentHighCredit
        //                                    {
        //                                        Amount = "00000200",
        //                                        Modifier = new[]
        //                                        {
        //                                            new Modifier
        //                                            {
        //                                                code = "E"
        //                                            }
        //                                        }
        //                                    }
        //                                },
        //                                TradeLineFlag = new[]
        //                                {
        //                                    new TradeLineFlag
        //                                    {
        //                                        code = "E"
        //                                    }
        //                                }
        //                            }
        //                        },
        //                BillingIndicator =
        //                        new[]
        //                        {
        //                            new NetConnectResponseProductsPremierProfileBillingIndicator
        //                            {
        //                                code = "E",
        //                                Value = "Indicator"
        //                            }
        //                        },
        //                CollectionData =
        //                        new[]
        //                        {
        //                            new NetConnectResponseProductsPremierProfileCollectionData
        //                            {
        //                                AccountStatus =
        //                                    new[]
        //                                    {
        //                                        new NetConnectResponseProductsPremierProfileCollectionDataAccountStatus
        //                                        {
        //                                            code = "00",
        //                                            Value = "Open Account"
        //                                        }
        //                                    },
        //                                AmountPaid = "00000000",
        //                                AmountPlacedForCollection = "00000320",
        //                                CollectionAgencyInfo =
        //                                    new[]
        //                                    {
        //                                        new NetConnectResponseProductsPremierProfileCollectionDataCollectionAgencyInfo
        //                                        {
        //                                            AgencyName = "RECEIVABLE MANAGEMENT SERVICES",
        //                                            PhoneNumber = "4842424000"
        //                                        }
        //                                    },
        //                                DateClosed = "00000000",
        //                                DatePlacedForCollection = "20100300"
        //                            }
        //                        },
        //                CommercialFraudShieldSummary =
        //                        new[]
        //                        {
        //                            new NetConnectResponseProductsPremierProfileCommercialFraudShieldSummary
        //                            {
        //                                ActiveBusinessIndicator =
        //                                    new[]
        //                                    {
        //                                        new NetConnectResponseProductsPremierProfileCommercialFraudShieldSummaryActiveBusinessIndicator
        //                                        {
        //                                            code = "A",
        //                                            Value = "Active"
        //                                        }
        //                                    },
        //                                BusinessBIN = "796744203",
        //                                BusinessRiskTriggersIndicator =
        //                                    new[]
        //                                    {
        //                                        new NetConnectResponseProductsPremierProfileCommercialFraudShieldSummaryBusinessRiskTriggersIndicator
        //                                        {
        //                                            code = "N",
        //                                            Value = "Risk Trigger Not Present"
        //                                        }
        //                                    },
        //                                BusinessVictimStatementIndicator =
        //                                    new[]
        //                                    {
        //                                        new NetConnectResponseProductsPremierProfileCommercialFraudShieldSummaryBusinessVictimStatementIndicator
        //                                        {
        //                                            code = "N",
        //                                            Value = "No statement Present"
        //                                        }
        //                                    },
        //                                MatchingBusinessIndicator =
        //                                    new[]
        //                                    {
        //                                        new MatchingBusinessIndicator
        //                                        {
        //                                            code = "W",
        //                                            Value = "Indicator"
        //                                        }
        //                                    },
        //                                NameAddressVerificationIndicator =
        //                                    new[]
        //                                    {
        //                                        new NetConnectResponseProductsPremierProfileCommercialFraudShieldSummaryNameAddressVerificationIndicator
        //                                        {
        //                                            code = "N",
        //                                            Value = "Verification was not performed"
        //                                        }
        //                                    },
        //                                OFACMatchCode =
        //                                    new[]
        //                                    {
        //                                        new NetConnectResponseProductsPremierProfileCommercialFraudShieldSummaryOFACMatchCode
        //                                        {
        //                                            code = "W"
        //                                        }
        //                                    }
        //                            }
        //                        },
        //                Competitors =
        //                        new[]
        //                        {
        //                            new NetConnectResponseProductsPremierProfileCompetitors
        //                            {
        //                                CompetitorName = "Name"
        //                            }
        //                        },
        //                CorporateLinkage =
        //                        new[]
        //                        {
        //                            new NetConnectResponseProductsPremierProfileCorporateLinkage
        //                            {
        //                                MatchingBusinessIndicator =
        //                                    new[]
        //                                    {
        //                                        new MatchingBusinessIndicator
        //                                        {
        //                                            code = "N",
        //                                            Value = "N"
        //                                        }
        //                                    },
        //                                LinkageCompanyAddress = "NEWENHAM HOUSE NORTHERN CROSS MALAHIDE ROAD",
        //                                LinkageCompanyCity = "DUBLIN",
        //                                LinkageCompanyName = "EXPERIAN PLC",
        //                                LinkageCompanyState = "",
        //                                LinkageCountryCode = "IRL",
        //                                LinkageRecordBIN = "000028714",
        //                                LinkageRecordType =
        //                                    new[]
        //                                    {
        //                                        new NetConnectResponseProductsPremierProfileCorporateLinkageLinkageRecordType
        //                                        {
        //                                            code = "1",
        //                                            Value = "Ultimate Parent"
        //                                        }
        //                                    },
        //                                ReturnLimitExceeded =
        //                                    new[]
        //                                    {
        //                                        new NetConnectResponseProductsPremierProfileCorporateLinkageReturnLimitExceeded
        //                                        {
        //                                            code = "N"
        //                                        },
        //                                    },
        //                            }
        //                        },
        //                DoingBusinessAs =
        //                        new[]
        //                        {
        //                            new NetConnectResponseProductsPremierProfileDoingBusinessAs
        //                            {
        //                                DBAName = "EXPERIAN DATA"
        //                            }
        //                        },
        //                ExecutiveSummary =
        //                        new[]
        //                        {
        //                            new NetConnectResponseProductsPremierProfileExecutiveSummary
        //                            {
        //                                AllIndustryDBT = "005",
        //                                BusinessDBT =
        //                                    new[]
        //                                    {
        //                                        new NetConnectResponseProductsPremierProfileExecutiveSummaryBusinessDBT
        //                                        {
        //                                            code = "006",
        //                                            Value = "Potential Risk. 11% of businesses fall into this range"
        //                                        }
        //                                    },
        //                                CommonTerms = "CONTRCT",
        //                                CommonTerms2 = "NET 30",
        //                                CommonTerms3 = "REVOLVE",
        //                                CurrentTotalAccountBalance =
        //                                    new[]
        //                                    {
        //                                        new NetConnectResponseProductsPremierProfileExecutiveSummaryCurrentTotalAccountBalance
        //                                        {
        //                                            Amount = "00000743400",
        //                                            Modifier = new[]
        //                                            {
        //                                                new Modifier
        //                                                {
        //                                                    code = "E"
        //                                                }
        //                                            },
        //                                        }
        //                                    },
        //                                HighCreditAmountExtended = "00000657200",
        //                                HighestTotalAccountBalance =
        //                                    new[]
        //                                    {
        //                                        new NetConnectResponseProductsPremierProfileExecutiveSummaryHighestTotalAccountBalance
        //                                        {
        //                                            Amount = "00000775100",
        //                                            Modifier = new[]
        //                                            {
        //                                                new Modifier
        //                                                {
        //                                                    code = "E"
        //                                                }
        //                                            },
        //                                        }
        //                                    },
        //                                IndustryDBT = "006",
        //                                IndustryDescription = "CREDIT RPT & COLLECT",
        //                                IndustryPaymentComparison =
        //                                    new[]
        //                                    {
        //                                        new NetConnectResponseProductsPremierProfileExecutiveSummaryIndustryPaymentComparison
        //                                        {
        //                                            code = "S",
        //                                            Value = "Has paid the same as similar businesses",
        //                                        }
        //                                    },
        //                                LowestTotalAccountBalance =
        //                                    new[]
        //                                    {
        //                                        new NetConnectResponseProductsPremierProfileExecutiveSummaryLowestTotalAccountBalance
        //                                        {
        //                                            Amount = "00000088500",
        //                                            Modifier = new[]
        //                                            {
        //                                                new Modifier
        //                                                {
        //                                                    code = "W"
        //                                                }
        //                                            }
        //                                        }
        //                                    },
        //                                MedianCreditAmountExtended = "00000013000",
        //                                PaymentTrendIndicator =
        //                                    new[]
        //                                    {
        //                                        new NetConnectResponseProductsPremierProfileExecutiveSummaryPaymentTrendIndicator
        //                                        {
        //                                            code = "S",
        //                                            Value = "Stable",
        //                                        }
        //                                    },
        //                                PredictedDBT = "006",
        //                                PredictedDBTDate = "20150916"
        //                            }
        //                        },
        //                ExpandedBusinessNameAndAddress =
        //                        new[]
        //                        {
        //                            new NetConnectResponseProductsPremierProfileExpandedBusinessNameAndAddress
        //                            {
        //                                Zip = "92626",
        //                                PhoneNumber = "714-830-7000",
        //                                State = "CA",
        //                                City = "COSTA MESA",
        //                                BusinessName = "EXPERIAN",
        //                                CorporateLink =
        //                                    new[]
        //                                    {
        //                                        new NetConnectResponseProductsPremierProfileExpandedBusinessNameAndAddressCorporateLink
        //                                        {
        //                                            CorporateLinkageIndicator = "Y"
        //                                        }
        //                                    },
        //                                ExperianBIN = "796744203",
        //                                MatchingBranchAddress =
        //                                    new[]
        //                                    {
        //                                        new NetConnectResponseProductsPremierProfileExpandedBusinessNameAndAddressMatchingBranchAddress
        //                                        {
        //                                            MatchingBranchBIN = "898481533",
        //                                            MatchingCity = "COSTA MESA",
        //                                            MatchingState = "CA",
        //                                            MatchingStreetAddress = "475 ANTON BLVD",
        //                                            MatchingZip = "92626",
        //                                            MatchingZipExtension = "7037"
        //                                        }
        //                                    },
        //                                ProfileDate = "20150724",
        //                                ProfileTime = "03:02:34",
        //                                ProfileType =
        //                                    new[]
        //                                    {
        //                                        new NetConnectResponseProductsPremierProfileExpandedBusinessNameAndAddressProfileType
        //                                        {
        //                                            code = "BUSP"
        //                                        }
        //                                    },
        //                                StreetAddress = "475 ANTON BLVD",
        //                                TaxID = "131134319",
        //                                TerminalNumber = "AMN",
        //                                WebsiteURL = "experian.com",
        //                                ZipExtension = "7037"
        //                            }
        //                        },
        //                ExpandedCreditSummary =
        //                        new[]
        //                        {
        //                            new NetConnectResponseProductsPremierProfileExpandedCreditSummary
        //                            {
        //                                ActiveTradelineCount = "0040",
        //                                AllTradelineBalance = "00000819900",
        //                                AllTradelineCount = "0019",
        //                                AverageBalance5Quarters = "00000511440",
        //                                BankruptcyFilingCount = "0000",
        //                                CollectionBalance = "00000000320",
        //                                CollectionCount = "0001",
        //                                CollectionCountPast24Months = "0000",
        //                                CurrentAccountBalance = "00000731200",
        //                                CurrentDBT = "006",
        //                                CurrentTradelineCount = "0020",
        //                                HighBalance6Months = "007",
        //                                HighestDBT5Quarters = "031",
        //                                HighestDBT6Months = "234",
        //                                JudgmentFilingCount = "0009",
        //                                JudgmentFlag = "Y",
        //                                LegalBalance = "00001067251",
        //                                LowBalance6Months = "00000088500",
        //                                MonthlyAverageDBT = "006",
        //                                MostRecentBankruptcyDate = "",
        //                                MostRecentCollectionDate = "20100300",
        //                                MostRecentJudgmentDate = "20150408",
        //                                MostRecentTaxLienDate = "20150205",
        //                                MostRecentUCCFilingDate = "20150703",
        //                               OFACMatch =
        //                                    new[]
        //                                    {
        //                                        new NetConnectResponseProductsPremierProfileExpandedCreditSummaryOFACMatch
        //                                        {
        //                                            code = "OFACMatch"
        //                                        }
        //                                    },
        //                                OldestCollectionDate = "20100300",
        //                                OldestJudgmentDate = "20090604",
        //                                OldestTaxLienDate = "20100300",
        //                                OldestUCCFilingDate = "20040108",
        //                                OpenCollectionBalance = "00000000320",
        //                                OpenCollectionCount = "0001",
        //                                SingleHighCredit = "00000657200",
        //                                TaxLienFilingCount = "0005",
        //                                TaxLienFlag = "Y",
        //                                TradeCollectionBalance = "00000820220",
        //                                TradeCollectionCount = "0041",
        //                                UCCDerogCount = "0002",
        //                                UCCFilings = "0014",
        //                                VictimStatement =
        //                                    new[]
        //                                    {
        //                                        new NetConnectResponseProductsPremierProfileExpandedCreditSummaryVictimStatement
        //                                        {
        //                                            code = "N",
        //                                            Value = "This business does not have a Victim statement"
        //                                        }
        //                                    }
        //                            }
        //                        },
        //                UCCFilings =
        //                        new[]
        //                        {
        //                            new NetConnectResponseProductsPremierProfileUCCFilings
        //                            {
        //                                DocumentNumber = "157473463080",
        //                                LegalType = new[]
        //                                {
        //                                    new LegalType
        //                                    {
        //                                        code = "08",
        //                                        Value = "UCC"
        //                                    }
        //                                },
        //                                DateFiled = "20150703",
        //                                FilingLocation = "SEC OF STATE CA",
        //                                LegalAction = new[]
        //                                {
        //                                    new LegalAction
        //                                    {
        //                                        code = "01",
        //                                        Value = "Filed"
        //                                    }
        //                                },
        //                                CollateralCodes =
        //                                    new NetConnectResponseProductsPremierProfileUCCFilingsCollateralCodesCollateral
        //                                    {
        //                                        Collateral =
        //                                            new[]
        //                                            {
        //                                                new NetConnectResponseProductsPremierProfileUCCFilingsCollateralCodesCollateralValues
        //                                                {
        //                                                    code = "9 ",
        //                                                    Value = "AFTER ACQUIRED PROP"
        //                                                }
        //                                            }
        //                                    },
        //                                OriginalUCCFilingsInfo =
        //                                    new[]
        //                                    {
        //                                        new NetConnectResponseProductsPremierProfileUCCFilingsOriginalUCCFilingsInfo
        //                                        {
        //                                            LegalAction = new[]
        //                                            {
        //                                                new LegalAction
        //                                                {
        //                                                    code = "08",
        //                                                    Value = "TERMINATED"
        //                                                }
        //                                            },
        //                                            DateFiled = "20140508",
        //                                            DocumentNumber = "019259366",
        //                                            FilingState = "IL"
        //                                        }
        //                                    },
        //                                SecuredParty = "MILANZZ KILIMANJJARO CA FRESNO 93775 P.O.BOX 11777"
        //                            }
        //                        },
        //                IndustryPaymentTrends =
        //                        new[]
        //                        {
        //                            new NetConnectResponseProductsPremierProfileIndustryPaymentTrends
        //                            {
        //                                SIC = "",
        //                                CurrentMonth = new[]
        //                                {
        //                                    new CurrentMonth
        //                                    {
        //                                        Date = "20100300",
        //                                        DBT60 = "000",
        //                                        DBT30 = "000",
        //                                        CurrentPercentage = "20",
        //                                        DBT90 = "000",
        //                                        DBT = "000",
        //                                        DBT120 = "000",
        //                                        TotalAccountBalance = new[]
        //                                        {
        //                                            new TotalAccountBalance
        //                                            {
        //                                                Amount = "2324",
        //                                                Modifier = new[]
        //                                                {
        //                                                    new Modifier
        //                                                    {
        //                                                        code = "R"
        //                                                    }
        //                                                }
        //                                            }
        //                                        }
        //                                    }
        //                                },
        //                                PriorMonth = new[]
        //                                {
        //                                    new PriorMonth
        //                                    {
        //                                        Date = "20100300",
        //                                        DBT60 = "000",
        //                                        DBT30 = "000",
        //                                        CurrentPercentage = "20",
        //                                        DBT90 = "000",
        //                                        TotalAccountBalance = new[]
        //                                        {
        //                                            new TotalAccountBalance
        //                                            {
        //                                                Amount = "234",
        //                                                Modifier = new[]
        //                                                {
        //                                                    new Modifier
        //                                                    {
        //                                                        code = "W",
        //                                                    }
        //                                                }
        //                                            }
        //                                        },
        //                                        DBT = "224",
        //                                        DBT120 = "333"
        //                                    }
        //                                },
        //                            }
        //                        },
        //                ScoreTrendsCreditLimit =
        //                        new[]
        //                        {
        //                            new NetConnectResponseProductsPremierProfileScoreTrendsCreditLimit
        //                            {
        //                                CreditLimitAmount = "00000311800",
        //                                MostRecentQuarter = new[]
        //                                {
        //                                    new MostRecentQuarter
        //                                    {
        //                                        DBT = "123",
        //                                        CurrentPercentage = "23",
        //                                        DBT30 = "000",
        //                                        DBT60 = "000",
        //                                        DBT90 = "000",
        //                                        DBT120 = "000",
        //                                        YearOfQuarter = "2232",
        //                                        Quarter = "APR-JUN",
        //                                        Score = "017",
        //                                        TotalAccountBalance = new[]
        //                                        {
        //                                            new TotalAccountBalance
        //                                            {
        //                                                Amount = "2123122",
        //                                                Modifier = new[]
        //                                                {
        //                                                    new Modifier
        //                                                    {
        //                                                        code = "W",
        //                                                    }
        //                                                },
        //                                            }
        //                                        },
        //                                        QuarterWithinYear = new[]
        //                                        {
        //                                            new QuarterWithinYear
        //                                            {
        //                                                code = "W",
        //                                                Value = "WERR",
        //                                            }
        //                                        },
        //                                    }
        //                                },
        //                                PriorQuarter = new[]
        //                                {
        //                                    new PriorQuarter
        //                                    {
        //                                        Date = "11123",
        //                                        DBT = "123",
        //                                        CurrentPercentage = "00",
        //                                        DBT30 = "000",
        //                                        DBT60 = "000",
        //                                        DBT90 = "000",
        //                                        YearOfQuarter = "89",
        //                                        Quarter = "AUG-OCT",
        //                                        Score = "042",
        //                                        TotalAccountBalance = new[]
        //                                        {
        //                                            new TotalAccountBalance
        //                                            {
        //                                                Amount = "",
        //                                                Modifier = new[]
        //                                                {
        //                                                    new Modifier
        //                                                    {
        //                                                        code = "T",
        //                                                    }
        //                                                },
        //                                            }
        //                                        },
        //                                        QuarterWithinYear = new[]
        //                                        {
        //                                            new QuarterWithinYear
        //                                            {
        //                                                code = "T",
        //                                                Value = "Year",
        //                                            }
        //                                        },
        //                                    }
        //                                },
        //                            }
        //                        },
        //                ScoreFactors =
        //                        new[]
        //                        {
        //                            new NetConnectResponseProductsPremierProfileScoreFactors
        //                            {
        //                                ModelCode = "000223",
        //                                ScoreFactor =
        //                                    new[]
        //                                    {
        //                                        new NetConnectResponseProductsPremierProfileScoreFactorsScoreFactor
        //                                        {
        //                                            code = "001",
        //                                            Value = "NUMBER OF COMMERCIAL COLLECTION ACCOUNTS",
        //                                        }
        //                                    },
        //                            }
        //                        },
        //                IntelliscoreScoreInformation =
        //                        new[]
        //                        {
        //                            new NetConnectResponseProductsPremierProfileIntelliscoreScoreInformation
        //                            {
        //                                Filler = "3",
        //                                ProfileNumber = "I005542330",
        //                                PercentileRanking = "012",
        //                                Action = "MEDIUM TO HIGH RISK",
        //                                RiskClass = "4",
        //                                PubliclyHeldCompany =
        //                                    new[]
        //                                    {
        //                                        new NetConnectResponseProductsPremierProfileIntelliscoreScoreInformationPubliclyHeldCompany
        //                                        {
        //                                            code = "N",
        //                                        }
        //                                    },
        //                                LimitedProfile =
        //                                    new[]
        //                                    {
        //                                        new NetConnectResponseProductsPremierProfileIntelliscoreScoreInformationLimitedProfile
        //                                        {
        //                                            code = "N",
        //                                        }
        //                                    },
        //                                ScoreInfo =
        //                                    new[]
        //                                    {
        //                                        new NetConnectResponseProductsPremierProfileIntelliscoreScoreInformationScoreInfo
        //                                        {
        //                                            Score = "0000001800",
        //                                        }
        //                                    },
        //                                ModelInformation =
        //                                    new[]
        //                                    {
        //                                        new NetConnectResponseProductsPremierProfileIntelliscoreScoreInformationModelInformation
        //                                        {
        //                                            ModelCode = "000223",
        //                                            ModelTitle = "FINANCIAL STABILITY RISK",
        //                                            CustomModelCode = "03",
        //                                        }
        //                                    },
        //                            }
        //                        },
        //                NAICSCodes =
        //                        new[]
        //                        {
        //                            new Proxy.BusinessReportResponse.NetConnectResponseProductsPremierProfileNAICSCodesNAICS
        //                            {
        //                                NAICS =
        //                                    new Proxy.BusinessReportResponse.
        //                                        NetConnectResponseProductsPremierProfileNAICSCodesNAICSValues
        //                                    {
        //                                        code = "",
        //                                        Value = "",
        //                                    },
        //                            }
        //                        },
        //                SICCodes =
        //                        new[]
        //                        {
        //                            new Proxy.BusinessReportResponse.NetConnectResponseProductsPremierProfileSICCodesSIC
        //                            {
        //                                SIC =
        //                                    new Proxy.BusinessReportResponse.
        //                                        NetConnectResponseProductsPremierProfileSICCodesSICValues
        //                                    {
        //                                        code = "7323",
        //                                        Value = "CREDIT REPORTING SERVICES",
        //                                    },
        //                            }
        //                        },
        //                KeyPersonnelExecutiveInformation =
        //                        new[]
        //                        {
        //                            new NetConnectResponseProductsPremierProfileKeyPersonnelExecutiveInformation
        //                            {
        //                                Name = "CRAIG BOUNDY",
        //                                NameFlag =
        //                                    new[]
        //                                    {
        //                                        new NetConnectResponseProductsPremierProfileKeyPersonnelExecutiveInformationNameFlag
        //                                        {
        //                                            code = "0",
        //                                            Value = "Name Fielded into First, Middle and Last",
        //                                        }
        //                                    },
        //                                Title =
        //                                    new[]
        //                                    {
        //                                        new NetConnectResponseProductsPremierProfileKeyPersonnelExecutiveInformationTitle
        //                                        {
        //                                            code = "CEO",
        //                                            Value = "CHIEF EXECUTIVE OFFICER",
        //                                        }
        //                                    },
        //                            }
        //                        },
        //                UCCFilingsSummaryCounts =
        //                        new[]
        //                        {
        //                            new NetConnectResponseProductsPremierProfileUCCFilingsSummaryCounts
        //                            {
        //                                UCCFilingsTotal = "00037",
        //                                MostRecent6Months =
        //                                    new[]
        //                                    {
        //                                        new NetConnectResponseProductsPremierProfileUCCFilingsSummaryCountsMostRecent6Months
        //                                        {
        //                                            StartDate = "20150701",
        //                                            FilingsTotal = "001",
        //                                            FilingsWithDerogatoryCollateral = "000",
        //                                            ReleasesAndTerminationsTotal = "000",
        //                                            ContinuationsTotal = "000",
        //                                            AmendedAndAssignedTotal = "000",
        //                                        }
        //                                    },
        //                                Previous6Months =
        //                                    new[]
        //                                    {
        //                                        new NetConnectResponseProductsPremierProfileUCCFilingsSummaryCountsPrevious6Months
        //                                        {
        //                                            StartDate = "20150101",
        //                                            FilingsTotal = "000",
        //                                            FilingsWithDerogatoryCollateral = "000",
        //                                            ReleasesAndTerminationsTotal = "000",
        //                                            ContinuationsTotal = "000",
        //                                            AmendedAndAssignedTotal = "000",
        //                                        }
        //                                    },
        //                            }
        //                        },
        //                JudgmentOrAttachmentLien =
        //                        new[]
        //                        {
        //                            new NetConnectResponseProductsPremierProfileJudgmentOrAttachmentLien
        //                            {
        //                                DateFiled = "20150408",
        //                                DocumentNumber = "SCS131440",
        //                                FilingLocation = "SOUTHERN BRANCH MUNCICPAL COURT",
        //                                LiabilityAmount = "00000002500",
        //                                PlaintiffName = "GEORGE MARDIKIAN",
        //                                LegalType = new[]
        //                                {
        //                                    new LegalType
        //                                    {
        //                                        code = "05",
        //                                        Value = "Judgment",
        //                                    }
        //                                },
        //                                LegalAction = new[]
        //                                {
        //                                    new LegalAction
        //                                    {
        //                                        code = "01",
        //                                        Value = "Filed",
        //                                    }
        //                                },
        //                            }
        //                        },
        //                TaxLien = new[]
        //                    {
        //                        new NetConnectResponseProductsPremierProfileTaxLien
        //                        {
        //                            DateFiled = "20141205",
        //                            DocumentNumber = "10257128",
        //                            FilingLocation = "MARION CNTY CIRCUIT/SUPERIOR/MUNICIPAL C",
        //                            Owner = "STATE OF INDIANA",
        //                            LiabilityAmount = "00000000212",
        //                            LegalType = new[]
        //                            {
        //                                new LegalType
        //                                {
        //                                    code = "03",
        //                                    Value = "State Tax Lien",
        //                                }
        //                            },
        //                            LegalAction = new[]
        //                            {
        //                                new LegalAction
        //                                {
        //                                    code = "07",
        //                                    Value = "Released",
        //                                }
        //                            },
        //                        }
        //                    },
        //                QuarterlyPaymentTrends =
        //                        new[]
        //                        {
        //                            new NetConnectResponseProductsPremierProfileQuarterlyPaymentTrends
        //                            {
        //                                MostRecentQuarter = new[]
        //                                {
        //                                    new MostRecentQuarter
        //                                    {
        //                                        DBT = "006",
        //                                        CurrentPercentage = "062",
        //                                        DBT30 = "000",
        //                                        DBT60 = "000",
        //                                        DBT90 = "000",
        //                                        DBT120 = "000",
        //                                        YearOfQuarter = "20140000",
        //                                        Quarter = "",
        //                                        Score = "",
        //                                        TotalAccountBalance = new[]
        //                                        {
        //                                            new TotalAccountBalance
        //                                            {
        //                                                Amount = "00000749500",
        //                                                Modifier = new[]
        //                                                {
        //                                                    new Modifier
        //                                                    {
        //                                                        code = "W",
        //                                                    }
        //                                                },
        //                                            }
        //                                        },
        //                                        QuarterWithinYear = new[]
        //                                        {
        //                                            new QuarterWithinYear
        //                                            {
        //                                                code = "2",
        //                                                Value = "Second quarter (Apr, May & Jun)",
        //                                            }
        //                                        },
        //                                    }
        //                                },
        //                                PriorQuarter = new[]
        //                                {
        //                                    new PriorQuarter
        //                                    {
        //                                        Date = "20150700",
        //                                        DBT = "006",
        //                                        CurrentPercentage = "062",
        //                                        DBT30 = "000",
        //                                        DBT60 = "000",
        //                                        DBT90 = "000",
        //                                        YearOfQuarter = "20140000",
        //                                        Quarter = "1232",
        //                                        Score = "23",
        //                                        TotalAccountBalance = new[]
        //                                        {
        //                                            new TotalAccountBalance
        //                                            {
        //                                                Amount = "24223",
        //                                                Modifier = new[]
        //                                                {
        //                                                    new Modifier
        //                                                    {
        //                                                        code = "W",
        //                                                    }
        //                                                },
        //                                            }
        //                                        },
        //                                        QuarterWithinYear = new[]
        //                                        {
        //                                            new QuarterWithinYear
        //                                            {
        //                                                code = "W",
        //                                                Value = "Year",
        //                                            }
        //                                        },
        //                                    }
        //                                },
        //                            }
        //                        },
        //                PaymentTrends =
        //                        new[]
        //                        {
        //                            new NetConnectResponseProductsPremierProfilePaymentTrends
        //                            {
        //                                CurrentMonth = new[]
        //                                {
        //                                    new CurrentMonth
        //                                    {
        //                                        Date = "20150700",
        //                                        DBT = "006",
        //                                        CurrentPercentage = "062",
        //                                        DBT30 = "000",
        //                                        DBT60 = "000",
        //                                        DBT90 = "000",
        //                                        DBT120 = "000",
        //                                        TotalAccountBalance = new[]
        //                                        {
        //                                            new TotalAccountBalance
        //                                            {
        //                                                Amount = "00000731200",
        //                                                Modifier = new[]
        //                                                {
        //                                                    new Modifier
        //                                                    {
        //                                                        code = "E",
        //                                                    }
        //                                                },
        //                                            }
        //                                        },
        //                                    }
        //                                },
        //                                PriorMonth = new[]
        //                                {
        //                                    new PriorMonth
        //                                    {
        //                                        Date = "20150700",
        //                                        DBT = "006",
        //                                        CurrentPercentage = "062",
        //                                        DBT30 = "062",
        //                                        DBT60 = "062",
        //                                        DBT90 = "062",
        //                                        DBT120 = "062",
        //                                        TotalAccountBalance = new[]
        //                                        {
        //                                            new TotalAccountBalance
        //                                            {
        //                                                Amount = "20150700",
        //                                                Modifier = new[]
        //                                                {
        //                                                    new Modifier
        //                                                    {
        //                                                        code = "W",
        //                                                    }
        //                                                },
        //                                            }
        //                                        },
        //                                    }
        //                                },
        //                            }
        //                        },
        //                PaymentTotals =
        //                        new[]
        //                        {
        //                            new NetConnectResponseProductsPremierProfilePaymentTotals
        //                            {
        //                                NewlyReportedTradeLines =
        //                                    new[]
        //                                    {
        //                                        new NetConnectResponseProductsPremierProfilePaymentTotalsNewlyReportedTradeLines
        //                                        {
        //                                            NumberOfLines = "0001",
        //                                            DBT = "000",
        //                                            CurrentPercentage = "100",
        //                                            DBT30 = "000",
        //                                            DBT60 = "000",
        //                                            DBT90 = "000",
        //                                            DBT120 = "000",
        //                                            TotalHighCreditAmount =
        //                                                new[]
        //                                                {
        //                                                    new TotalHighCreditAmount
        //                                                    {
        //                                                        Amount = "00000013000",
        //                                                        Modifier = new[]
        //                                                        {
        //                                                            new Modifier
        //                                                            {
        //                                                                code = "E",
        //                                                            }
        //                                                        },
        //                                                    }
        //                                                },
        //                                            TotalAccountBalance =
        //                                                new[]
        //                                                {
        //                                                    new TotalAccountBalance
        //                                                    {
        //                                                        Amount = "00000013000",
        //                                                        Modifier = new[]
        //                                                        {
        //                                                            new Modifier
        //                                                            {
        //                                                                code = "R",
        //                                                            }
        //                                                        },
        //                                                    }
        //                                                },
        //                                        }
        //                                    },
        //                                ContinouslyReportedTradeLines =
        //                                    new[]
        //                                    {
        //                                        new NetConnectResponseProductsPremierProfilePaymentTotalsContinouslyReportedTradeLines
        //                                        {
        //                                            NumberOfLines = "0019",
        //                                            DBT = "006",
        //                                            CurrentPercentage = "",
        //                                            DBT30 = "038",
        //                                            DBT60 = "000",
        //                                            DBT90 = "000",
        //                                            TotalHighCreditAmount =
        //                                                new[]
        //                                                {
        //                                                    new TotalHighCreditAmount
        //                                                    {
        //                                                        Amount = "00001239500",
        //                                                        Modifier = new[]
        //                                                        {
        //                                                            new Modifier
        //                                                            {
        //                                                                code = "E",
        //                                                            }
        //                                                        },
        //                                                    }
        //                                                },
        //                                            TotalAccountBalance =
        //                                                new[]
        //                                                {
        //                                                    new TotalAccountBalance
        //                                                    {
        //                                                        Amount = "00001239500",
        //                                                        Modifier = new[]
        //                                                        {
        //                                                            new Modifier
        //                                                            {
        //                                                                code = "W",
        //                                                            }
        //                                                        },
        //                                                    }
        //                                                },
        //                                        }
        //                                    },
        //                                CombinedTradeLines =
        //                                    new[]
        //                                    {
        //                                        new NetConnectResponseProductsPremierProfilePaymentTotalsCombinedTradeLines
        //                                        {
        //                                            NumberOfLines = "0020",
        //                                            DBT = "006",
        //                                            CurrentPercentage = "062",
        //                                            DBT30 = "038",
        //                                            DBT60 = "000",
        //                                            DBT90 = "000",
        //                                            DBT120 = "000",
        //                                            TotalHighCreditAmount =
        //                                                new[]
        //                                                {
        //                                                    new TotalHighCreditAmount
        //                                                    {
        //                                                        Amount = "00001252500",
        //                                                        Modifier = new[]
        //                                                        {
        //                                                            new Modifier
        //                                                            {
        //                                                                code = "W",
        //                                                            }
        //                                                        },
        //                                                    }
        //                                                },
        //                                            TotalAccountBalance =
        //                                                new[]
        //                                                {
        //                                                    new TotalAccountBalance
        //                                                    {
        //                                                        Amount = "E",
        //                                                        Modifier = new[]
        //                                                        {
        //                                                            new Modifier
        //                                                            {
        //                                                                code = "00000744200",
        //                                                            }
        //                                                        },
        //                                                    }
        //                                                },
        //                                        }
        //                                    },
        //                                AdditionalTradeLines =
        //                                    new[]
        //                                    {
        //                                        new NetConnectResponseProductsPremierProfilePaymentTotalsAdditionalTradeLines
        //                                        {
        //                                            NumberOfLines = "0020",
        //                                            CurrentPercentage = "013",
        //                                            DBT30 = "000",
        //                                            DBT60 = "087",
        //                                            DBT90 = "000",
        //                                            DBT120 = "000",
        //                                            TotalHighCreditAmount =
        //                                                new[]
        //                                                {
        //                                                    new TotalHighCreditAmount
        //                                                    {
        //                                                        Amount = "00000263300",
        //                                                        Modifier = new[]
        //                                                        {
        //                                                            new Modifier
        //                                                            {
        //                                                                code = "R",
        //                                                            }
        //                                                        },
        //                                                    }
        //                                                },
        //                                            TotalAccountBalance =
        //                                                new[]
        //                                                {
        //                                                    new TotalAccountBalance
        //                                                    {
        //                                                        Amount = "00000075700",
        //                                                        Modifier = new[]
        //                                                        {
        //                                                            new Modifier
        //                                                            {
        //                                                                code = "E",
        //                                                            }
        //                                                        },
        //                                                    }
        //                                                },
        //                                        }
        //                                    },
        //                                TradeLines =
        //                                    new[]
        //                                    {
        //                                        new NetConnectResponseProductsPremierProfilePaymentTotalsTradeLines
        //                                        {
        //                                            NumberOfLines = "0040",
        //                                            CurrentPercentage = "058",
        //                                            DBT30 = "034",
        //                                            DBT60 = "008",
        //                                            DBT90 = "008",
        //                                            DBT120 = "008",
        //                                            TotalHighCreditAmount =
        //                                                new[]
        //                                                {
        //                                                    new TotalHighCreditAmount
        //                                                    {
        //                                                        Amount = "00001515800",
        //                                                        Modifier = new[]
        //                                                        {
        //                                                            new Modifier
        //                                                            {
        //                                                                code = "W",
        //                                                            }
        //                                                        },
        //                                                    }
        //                                                },
        //                                            TotalAccountBalance =
        //                                                new[]
        //                                                {
        //                                                    new TotalAccountBalance
        //                                                    {
        //                                                        Amount = "00000819900",
        //                                                        Modifier = new[]
        //                                                        {
        //                                                            new Modifier
        //                                                            {
        //                                                                code = "W",
        //                                                            }
        //                                                        },
        //                                                    }
        //                                                },
        //                                        }
        //                                    },
        //                            }
        //                        },
        //                TradePaymentExperiences =
        //                        new[]
        //                        {
        //                            new NetConnectResponseProductsPremierProfileTradePaymentExperiences
        //                            {
        //                                BusinessCategory = "ACCT SVCS",
        //                                DateReported = "20150700",
        //                                DateLastActivity = "00000000",
        //                                Terms = "VARIED ",
        //                                CurrentPercentage = "000",
        //                                DBT30 = "000",
        //                                DBT60 = "000",
        //                                DBT90 = "000",
        //                                DBT90Plus = "000",
        //                                Comments = "",
        //                                PaymentIndicator =
        //                                    new[]
        //                                    {
        //                                        new NetConnectResponseProductsPremierProfileTradePaymentExperiencesPaymentIndicator
        //                                        {
        //                                            code = "W",
        //                                        }
        //                                    },
        //                                RecentHighCredit = new[]
        //                                {
        //                                    new RecentHighCredit
        //                                    {
        //                                        Amount = "00000700",
        //                                        Modifier = new[]
        //                                        {
        //                                            new Modifier
        //                                            {
        //                                                code = "E",
        //                                            }
        //                                        },
        //                                    }
        //                                },
        //                                AccountBalance = new[]
        //                                {
        //                                    new AccountBalance
        //                                    {
        //                                        Amount = "00000000",
        //                                        Modifier = new[]
        //                                        {
        //                                            new Modifier
        //                                            {
        //                                                code = "W",
        //                                            }
        //                                        },
        //                                    }
        //                                },
        //                                TradeLineFlag = new[]
        //                                {
        //                                    new TradeLineFlag
        //                                    {
        //                                        code = "W",
        //                                    }
        //                                },
        //                                NewlyReportedIndicator = new[]
        //                                {
        //                                    new NewlyReportedIndicator
        //                                    {
        //                                        code = "W",
        //                                    }
        //                                },
        //                            }
        //                        },
        //            }
        //        }
        //        };

        //public static Proxy.DirectHitReportResponse.NetConnectResponse GetFakeNullDirectHitResponse()
        //{
        //    var response = new Proxy.BusinessReportResponse.NetConnectResponse
        //    {
        //        CompletionCode = "0000",
        //        ErrorMessage = "",
        //        ErrorTag = "",
        //        ReferenceId = "Premier Profile List Example",
        //        TransactionId = "20566968",
        //        Products = new Proxy.BusinessReportResponse.Products
        //        {
        //            BusinessProfile = new Proxy.BusinessReportResponse.NetConnectResponseProductsBusinessProfile
        //            {
        //                ProcessingMessage = null,
        //                InputSummary = null,
        //                Bankruptcy = null,
        //                Inquiry = null,
        //                BusinessFacts = null,
        //                AdditionalPaymentExperiences = null,
        //                BillingIndicator = null,
        //                CollectionData = null,
        //                CommercialFraudShieldSummary = null,
        //                Competitors = null,
        //                CorporateLinkage = null,
        //                DoingBusinessAs = null,
        //                ExecutiveSummary = null,
        //                ExpandedBusinessNameAndAddress = null,
        //                ExpandedCreditSummary = null,
        //                UCCFilings = null,
        //                IndustryPaymentTrends = null,
        //                ScoreTrendsCreditLimit = null,
        //                ScoreFactors = null,
        //                IntelliscoreScoreInformation = null,
        //                NAICSCodes = null,
        //                SICCodes = null,
        //                KeyPersonnelExecutiveInformation = null,
        //                UCCFilingsSummaryCounts = null,
        //                JudgmentOrAttachmentLien = null,
        //                TaxLien = null,
        //                QuarterlyPaymentTrends = null,
        //                PaymentTrends = null,
        //                PaymentTotals = null,
        //                TradePaymentExperiences = null
        //            }
        //        }
        //    };

        //    return response;
        //}
    }
}