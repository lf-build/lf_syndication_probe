﻿using LendFoundry.Syndication.LexisNexis.Bankruptcy;
using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy;
using LendFoundry.Syndication.LexisNexis.CriminalRecord;
using LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy;
using Moq;
using System.Xml;
using Xunit;

namespace LendFoundry.Syndication.LexisNexis.Test.Live
{
    public class LexisNexisServiceLiveTest
    {
        //private LexisNexisConfiguration lexisNexisConfiguration { get; }
        //private BankruptcyConfiguration bankruptcyConfiguration { get; }

        //private CriminalRecordConfiguration criminalRecordConfiguration { get; }
        //private LexisNexisService lexisnexisService { get; }

        //public LexisNexisServiceLiveTest()
        //{
           

        //    bankruptcyConfiguration = new BankruptcyConfiguration
        //    {
        //        BankruptcyUrl = "http://192.168.1.72:7022/bankruptcy",
        //        UserName = "CAPLLCXML",
        //        Password = "Sigma@123",
        //        EndUser = null,
        //        Options = null
        //        //EndUser = new LendFoundry.Syndication.LexisNexis.Bankruptcy.User
        //        //{
        //        //     ReferenceCode="",
        //        //     BillingCode="",
        //        //     QueryId="",
        //        //     GLBPurpose="3",
        //        //     DLPurpose="1",
        //        //     MaxWaitSeconds= 10000,
        //        //     AccountNumber="AccountNumber",
        //        //     EndUser= {
        //        //       City="",
        //        //       CompanyName="",
        //        //       State="",
        //        //       StreetAddress1="",
        //        //       Zip5=""
        //        //      }
        //        // },
        //        //Options = new SearchOption
        //        //{
        //        //     FCRAPurpose=1,
        //        //     ReturnCount=10,
        //        //     StartingRecord=1
        //        // }
        //    };

        //    criminalRecordConfiguration = new CriminalRecordConfiguration()
        //    {
        //        CriminalRecordUrl = "http =//192.168.1.72=7022/criminalrecord",
        //        UserName = "CAPLLCXML",
        //        Password = "Sigma@123",
        //        EndUser = null,
        //        Options = null
        //        //EndUser = new LendFoundry.Syndication.LexisNexis.CriminalRecord.User
        //        //{
        //        //                  ReferenceCode= "",
        //        //                  BillingCode="",
        //        //                  QueryId="",
        //        //                  GLBPurpose="3",
        //        //                  DLPurpose= "1",
        //        //                  MaxWaitSeconds= 10000,
        //        //                  AccountNumber= "AccountNumber",
        //        //                  EndUser= {
        //        //                    City="" ,
        //        //                    CompanyName="" ,
        //        //                    State= "",
        //        //                    StreetAddress1="" ,
        //        //                    Zip5=""
        //        //                    }
        //        //            },
        //        //Options = new SearchOption
        //        //{
        //        //          FCRAPurpose= 1,
        //        //          ReturnCount= 10,
        //        //          StartingRecord= 1
        //        //        }
        //    };

        //    lexisNexisConfiguration = new LexisNexisConfiguration();
        //    lexisNexisConfiguration.Bankruptcy = bankruptcyConfiguration;
        //    lexisNexisConfiguration.CriminalRecord = criminalRecordConfiguration;
        //    lexisnexisService = new LexisNexisService(null,null,null,null,null,null);
        //}

        //[Fact]
        //public void BankruptcySearch_Response()
        //{
        //    var response = lexisnexisService.BankruptcySearch("application", "123456", new SearchBankruptcyRequest
        //    {
        //         Ssn="773273174",
        //         FirstName="David",
        //          LastName="H"
        //    });
        //    Assert.NotNull(response);
        //}

        //[Fact]
        //public void BankruptcyReport_Response()
        //{
        //    var response = lexisnexisService.BankruptcyReport("application","123456",new BankruptcyReportRequest
        //    {
        //        QueryId = "BKMI0039831080",
        //        UniqueId = "999931269275",
        //    });
        //    Assert.NotNull(response);
        //}
        //[Fact]
        //public void CriminalRecordReport_Response()
        //{
        //    var response = lexisnexisService.CriminalRecordReport("application", "123456", new CriminalRecordReportRequest
        //    {
        //            QueryId="158786763R5539",
        //            UniqueId="999915523688"
        //    });
        //    Assert.NotNull(response);
        //}
        //[Fact]
        //public void CriminalSearch_Response()
        //{
        //    var response = lexisnexisService.CriminalSearch("application", "123456", new CriminalRecordSearchRequest
        //    {
        //          Ssn="773027164",
        //          FirstName="LARRY",
        //          LastName="TEMURALI"
        //    });
        //    Assert.NotNull(response);
        //}

    }
}