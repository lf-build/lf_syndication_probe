﻿
using LendFoundry.Syndication.LexisNexis;
using LendFoundry.Syndication.LexisNexis.Bankruptcy;
using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy;
using LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy;
using Moq;
using System;
using Xunit;

namespace CreditExchange.Syndication.LexisNexis.Test
{
    public class LexisNexisConfigurationTest
    {
        public void ThrowArgumentExceptionWithNullConfiguration()
        {
            Assert.Throws<ArgumentNullException>(() => new LexisNexisConfiguration());
        }

        [Fact]
        public void ThrowArgumentExceptionWithMissingConfigurationBankruptcy()
        {
            var bankruptcyConfiguration = new BankruptcyConfiguration
            {
                BankruptcyUrl = null,
                UserName = "CAPLLCXML",
                Password = "Sigma@123",
                EndUser = null,
                Options = new SearchOption
                {
                    FCRAPurpose = 1,
                    ReturnCount = 10,
                    StartingRecord = 1
                }
            };
            LexisNexisConfiguration BankruptcyUrlNull = new LexisNexisConfiguration
            {
                Bankruptcy = bankruptcyConfiguration,
                CriminalRecord = null
            };
            Assert.Throws<ArgumentNullException>(() => new BankruptcyProxy(BankruptcyUrlNull));

            var bankruptcyConfiguration1 = new BankruptcyConfiguration
            {
                BankruptcyUrl = "http://192.168.1.72:7022/bankruptcy",
                UserName = null,
                Password = "Sigma@123",
                EndUser = null,
                Options =null
            };
            LexisNexisConfiguration UserNameNull = new LexisNexisConfiguration
            {
                Bankruptcy = bankruptcyConfiguration1,
                CriminalRecord = null
            };
            Assert.Throws<ArgumentNullException>(() => new BankruptcyProxy(UserNameNull));

            var bankruptcyConfiguration2 = new BankruptcyConfiguration
            {
                BankruptcyUrl = "http://192.168.1.72:7022/bankruptcy",
                UserName = "CAPLLCXML",
                Password = null,
                EndUser = null,
                Options = new SearchOption
                {
                    FCRAPurpose = 1,
                    ReturnCount = 10,
                    StartingRecord = 1
                }
            };
            LexisNexisConfiguration PasswordNull = new LexisNexisConfiguration
            {
                Bankruptcy = bankruptcyConfiguration2,
                CriminalRecord = null
            };
            Assert.Throws<ArgumentNullException>(() => new BankruptcyProxy(PasswordNull));

            var  bankruptcyConfiguration3 = new BankruptcyConfiguration
            {
                BankruptcyUrl = "http://192.168.1.72:7022/bankruptcy",
                UserName = "CAPLLCXML",
                Password = "Sigma@123",
                EndUser = null,
                Options = new SearchOption
               {
                  FCRAPurpose = 1,
                  ReturnCount = 10,
                  StartingRecord = 1
               }
          };
            LexisNexisConfiguration EndUserNull = new LexisNexisConfiguration
            {
                Bankruptcy = bankruptcyConfiguration3,
                CriminalRecord = null
            };
            Assert.Throws<ArgumentNullException>(() => new BankruptcyProxy(EndUserNull));
           
        }



        [Fact]
        public void ThrowArgumentExceptionWithMissingConfigurationCriminalRecord()
        {

            var criminalRecordConfiguration = new CriminalRecordConfiguration()
            {
                CriminalRecordUrl = null,
                UserName = "CAPLLCXML",
                Password = "Sigma@123",
                EndUser = null,
                Options = null

            };
            LexisNexisConfiguration CriminalRecordUrlNull = new LexisNexisConfiguration
            {
                Bankruptcy = null,
                CriminalRecord = criminalRecordConfiguration
            };
            Assert.Throws<ArgumentNullException>(() => new CriminalRecordProxy(CriminalRecordUrlNull));


            var criminalRecordConfiguration1 = new CriminalRecordConfiguration()
            {
                CriminalRecordUrl = "http =//192.168.1.72=7022/criminalrecord",
                UserName = null,
                Password = "Sigma@123",
                EndUser = null,
                Options = null

            };
            LexisNexisConfiguration UserNameNull = new LexisNexisConfiguration
            {
                Bankruptcy = null,
                CriminalRecord = criminalRecordConfiguration
            };
            Assert.Throws<ArgumentNullException>(() => new CriminalRecordProxy(UserNameNull));

            var criminalRecordConfiguration2 = new CriminalRecordConfiguration()
            {
                CriminalRecordUrl = "http =//192.168.1.72=7022/criminalrecord",
                UserName = "CAPLLCXML",
                Password = null,
                EndUser = null,
                Options = null

            };
            LexisNexisConfiguration PasswordNull = new LexisNexisConfiguration
            {
                Bankruptcy = null,
                CriminalRecord = criminalRecordConfiguration2
            };
            Assert.Throws<ArgumentNullException>(() => new CriminalRecordProxy(PasswordNull));

            var criminalRecordConfiguration3 = new CriminalRecordConfiguration()
            {
                CriminalRecordUrl = "http =//192.168.1.72=7022/criminalrecord",
                UserName = "CAPLLCXML",
                Password = "Sigma@123",
                EndUser = null,
                Options = null

            };
            LexisNexisConfiguration EndUserNull = new LexisNexisConfiguration
            {
                Bankruptcy = null,
                CriminalRecord = criminalRecordConfiguration3
            };
            Assert.Throws<ArgumentNullException>(() => new CriminalRecordProxy(EndUserNull));


        }
    }
}