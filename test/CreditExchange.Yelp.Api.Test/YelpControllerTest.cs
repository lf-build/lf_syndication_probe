﻿using CreditExchange.Syndication.Yelp;
using CreditExchange.Syndication.Yelp.Response;
using CreditExchange.Yelp.Api.Controller;
using LendFoundry.Foundation.Services;
using Microsoft.AspNet.Mvc;
using Moq;
using Xunit;

namespace CreditExchange.Yelp.Api.Test
{
    public class YelpControllerTest
    {
        private Mock<IYelpService> yelpService { get; }

        public ApiController GetController(IYelpService yelpService)
        {
            return new ApiController(yelpService);
        }

        private Mock<IYelpService> GetYelpService()
        {
            return new Mock<IYelpService>();
        }

        [Fact]
        public async void GetBusinessDetailsReturnOnSuccess()
        {
            var yelpService = GetYelpService();
            yelpService.Setup(x => x.GetBusinessDetails("", "", "", "", "", true, true)).ReturnsAsync(It.IsAny<IBusinessDetailsResponse>());
            var response = (HttpOkObjectResult)await GetController(yelpService.Object).GetBusinessDetails("", "", "", "", "", true, true);
            Assert.NotNull(response);
            Assert.Equal(response.StatusCode, 200);
        }

        [Fact]
        public async void GetPhoneDetailsReturnOnSuccess()
        {
            var yelpService = GetYelpService();
            yelpService.Setup(x => x.GetPhoneDetails("", "", "")).ReturnsAsync(new SearchResponse());
            var response = (HttpOkObjectResult)await GetController(yelpService.Object).GetPhoneDetails("", "", "");
            Assert.NotNull(response);
            Assert.Equal(response.StatusCode, 200);
        }

        [Fact]
        public async void SerchDetailsReturnOnSuccess()
        {
            var yelpService = GetYelpService();
            yelpService.Setup(x => x.SearchDetails(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),
                It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).ReturnsAsync(new SearchResponse());
            var response = (HttpOkObjectResult)await GetController(yelpService.Object).SearchDetails("test", "test", "test", "test", 1, 1, 1, "", 1, true);
            Assert.NotNull(response);
            Assert.Equal(response.StatusCode, 200);
        }

        [Fact]
        public async void GetBusinessDetailsReturnError()
        {
            var yelpService = GetYelpService();
            yelpService.Setup(x => x.GetBusinessDetails("", "", "", "", "", true, true)).Throws(new System.ArgumentNullException());
            var response = (ErrorResult)await GetController(yelpService.Object).GetBusinessDetails("", "", "", "", "", true, true);
            Assert.NotNull(response);
            Assert.Equal(response.StatusCode, 400);
        }

        [Fact]
        public async void GetPhoneDetailsReturnError()
        {
            var yelpService = GetYelpService();
            yelpService.Setup(x => x.GetPhoneDetails("", "", "")).Throws(new System.ArgumentNullException());
            var response = (ErrorResult)await GetController(yelpService.Object).GetPhoneDetails("", "", "");
            Assert.NotNull(response);
            Assert.Equal(response.StatusCode, 400);
        }

        [Fact]
        public async void SerchDetailsReturnError()
        {
            var yelpService = GetYelpService();
            yelpService.Setup(x => x.SearchDetails(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<int>(),
                It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Throws(new System.ArgumentNullException());
            var response = (ErrorResult)await GetController(yelpService.Object).SearchDetails("test", "test", "test", "test", 1, 1, 1, "", 1, true);
            Assert.NotNull(response);
            Assert.Equal(response.StatusCode, 400);
        }
    }
}