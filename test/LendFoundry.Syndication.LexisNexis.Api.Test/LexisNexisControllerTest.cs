﻿using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Foundation.Services;
using LendFoundry.Syndication.LexisNexis;
using LendFoundry.Syndication.LexisNexis.Api.Controllers;
using LendFoundry.Syndication.LexisNexis.Bankruptcy;
using LendFoundry.Syndication.LexisNexis.CriminalRecord;
using LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport;
using Microsoft.AspNet.Mvc;
using Moq;
using Xunit;

namespace LendFoundry.Paynet.Api.Test
{
    public class LexisNexisControllerTest
    {
        private Mock<ILexisNexisService> LexisNexisService { get; }
        private Mock<IEventHubClient> EventHub { get; }
        private Mock<ILookupService> Lookup { get; }
        private Mock<ILogger> Logger { get; }

        public LexisNexisControllerTest()
        {
            EventHub = new Mock<IEventHubClient>();
            Lookup = new Mock<ILookupService>();
            Logger = new Mock<ILogger>();

        }
        private BankruptcyController GetBankruptcyController(ILexisNexisService LexisNexisService)
        {
           
            return new BankruptcyController(LexisNexisService,Logger.Object,EventHub.Object,Lookup.Object);
        }

        private CriminalRecordController GetCriminalRecordController(ILexisNexisService LexisNexisService)
        {

            return new CriminalRecordController(LexisNexisService, Logger.Object, EventHub.Object, Lookup.Object);
        }
        private Mock<ILexisNexisService> GetlexisNexisService()
        {
            return new Mock<ILexisNexisService>();
        }

        [Fact]
        public async void BankruptcySearchReturnOnSucess()
        {
            var lexisNexisService = GetlexisNexisService();
            lexisNexisService.Setup(x => x.BankruptcySearch("", "", It.IsAny<ISearchBankruptcyRequest>())).ReturnsAsync(null);
            var response = (HttpOkObjectResult)await GetBankruptcyController(lexisNexisService.Object).BankruptcySearch("application", "123", It.IsAny<SearchBankruptcyRequest>());
            Assert.NotNull(response);
            Assert.Equal(response.StatusCode, 200);
        }

        [Fact]
        public async void BankruptcyReportReturnOnSucess()
        {
            var lexisNexisService = GetlexisNexisService();
            lexisNexisService.Setup(x => x.BankruptcyReport("", "", It.IsAny<IBankruptcyReportRequest>())).ReturnsAsync(new IBankruptcyReportResponse());
            var response = (HttpOkObjectResult)await GetBankruptcyController(lexisNexisService.Object).BankruptcySearch("application", "123", It.IsAny<SearchBankruptcyRequest>());
            Assert.NotNull(response);
            Assert.Equal(response.StatusCode, 200);
        }


        [Fact]
        public async void CriminalSearchReturnOnSucess()
        {
            var lexisNexisService = GetlexisNexisService();
            lexisNexisService.Setup(x => x.CriminalSearch("", "", It.IsAny<ICriminalRecordSearchRequest>())).ReturnsAsync(null);
            var response = (HttpOkObjectResult)await GetCriminalRecordController(lexisNexisService.Object).CriminalSearch("application", "123", It.IsAny<CriminalRecordSearchRequest>());
            Assert.NotNull(response);
            Assert.Equal(response.StatusCode, 200);
        }

        [Fact]
        public async void CriminalRecordReportReturnOnSucess()
        {
            var lexisNexisService = GetlexisNexisService();
            lexisNexisService.Setup(x => x.CriminalRecordReport("", "", It.IsAny<ICriminalRecordReportRequest>())).ReturnsAsync(new CriminalReportResponse());
            var response = (HttpOkObjectResult)await GetCriminalRecordController(lexisNexisService.Object).CriminalReport("application", "123", It.IsAny<CriminalRecordReportRequest>());
            Assert.NotNull(response);
            Assert.Equal(response.StatusCode, 200);
        }
    }
}