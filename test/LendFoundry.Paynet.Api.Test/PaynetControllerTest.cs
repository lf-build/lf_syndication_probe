﻿using LendFoundry.Foundation.Services;
using LendFoundry.Paynet.Api.Controller;
using LendFoundry.Syndication.Paynet;
using LendFoundry.Syndication.Paynet.Request;
using LendFoundry.Syndication.Paynet.Response;
using Microsoft.AspNet.Mvc;
using Moq;
using Xunit;

namespace LendFoundry.Paynet.Api.Test
{
    public class PaynetControllerTest
    {
        private Mock<IPaynetService> paynetService { get; }

        private ApiController GetController(IPaynetService paynetService)
        {
            return new ApiController(paynetService);
        }

        private Mock<IPaynetService> GetpaynetService()
        {
            return new Mock<IPaynetService>();
        }

        [Fact]
        public async void SearchCompanyReturnOnSucess()
        {
            var paynetService = GetpaynetService();
            paynetService.Setup(x => x.SearchCompany("", "", It.IsAny<ISearchCompanyRequest>())).ReturnsAsync(new SearchCompanyResponse());
            var response = (HttpOkObjectResult)await GetController(paynetService.Object).SearchCompany("application", "123", It.IsAny<SearchCompanyRequest>());
            Assert.NotNull(response);
            Assert.Equal(response.StatusCode, 200);
        }

        [Fact]
        public async void SearchCompanyReturnOnError()
        {
            var paynetService = GetpaynetService();
            paynetService.Setup(x => x.SearchCompany(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<ISearchCompanyRequest>())).Throws(new PaynetException());
            var response = (ErrorResult)await GetController(paynetService.Object).SearchCompany("application", "123", It.IsAny<SearchCompanyRequest>());
            Assert.NotNull(response);
            Assert.Equal(response.StatusCode, 400);
        }

        [Fact]
        public async void GetCompanyReportReturnOnSucess()
        {
            var paynetService = GetpaynetService();
            paynetService.Setup(x => x.GetCompanyReport("", "", It.IsAny<IGetReportRequest>())).ReturnsAsync(new GetReportResponse());
            var response = (HttpOkObjectResult)await GetController(paynetService.Object).GetCompanyReport("application", "123", It.IsAny<GetReportRequest>());
            Assert.NotNull(response);
            Assert.Equal(response.StatusCode, 200);
        }
    }
}