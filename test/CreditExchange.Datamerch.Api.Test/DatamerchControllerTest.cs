﻿using CreditExchange.Datamerch.Api.Controller;
using CreditExchange.Syndication.Datamerch;
using CreditExchange.Syndication.Datamerch.Response;
using LendFoundry.Foundation.Services;
using Microsoft.AspNet.Mvc;
using Moq;
using Xunit;

namespace CreditExchange.Datamerch.Api.Test
{
    public class DatamerchControllerTest
    {
        private Mock<IDataMerchService> dataMerchService { get; }

        private ApiController GetController(IDataMerchService dataMerchService)
        {
            return new ApiController(dataMerchService);
        }

        private Mock<IDataMerchService> GetDataMerchService()
        {
            return new Mock<IDataMerchService>();
        }

        [Fact]
        public async void SearchMerchantReturnOnSucess()
        {
            var dataMerchService = GetDataMerchService();
            dataMerchService.Setup(x => x.SearchMerchant("", "", "")).ReturnsAsync(new SearchMerchantResponse());
            var response = (HttpOkObjectResult)await GetController(dataMerchService.Object).SearchMerchant("application", "123", "1234567");
            Assert.NotNull(response);
            Assert.Equal(response.StatusCode, 200);
        }

        [Fact]
        public async void SearchMerchantReturnOnError()
        {
            var dataMerchService = GetDataMerchService();
            dataMerchService.Setup(x => x.SearchMerchant(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Throws(new DatamerchException());
            var response = (ErrorResult)await GetController(dataMerchService.Object).SearchMerchant("application", "123", "1234567");
            Assert.NotNull(response);
            Assert.Equal(response.StatusCode, 400);
        }
    }
}