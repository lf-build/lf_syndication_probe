﻿using System;
using LendFoundry.Security.Tokens;
using LendFoundry.Syndication.Probe;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Client;
using Microsoft.Extensions.DependencyInjection;

namespace LendFoundry.Syndication.Probe.Client
{
    public class ProbeServiceClientFactory :IProbeServiceClientFactory
    {
        [Obsolete("Need to use the overloaded with Uri")]
        public ProbeServiceClientFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Endpoint = endpoint;
            Port = port;
            Uri = new UriBuilder("http", endpoint, port).Uri;
        }
        public ProbeServiceClientFactory(IServiceProvider provider, Uri uri = null)
        {
            Provider = provider;
            Uri = uri;
        }
        private IServiceProvider Provider { get; }

        private string Endpoint { get; }

        private int Port { get; }
        private Uri Uri { get; }


        public IProbeService Create(ITokenReader reader)
        {
            var uri = Uri;
            if (uri == null)
            {
                var logger = Provider.GetService<ILoggerFactory>().Create(NullLogContext.Instance);
                uri = Provider.GetRequiredService<IDependencyServiceUriResolverFactory>().Create(reader, logger).Get("probe");
            }
            var client = Provider.GetServiceClient(reader, uri);
            return new ProbeService(client);
        }
    }
}
