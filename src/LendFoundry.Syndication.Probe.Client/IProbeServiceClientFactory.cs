﻿using LendFoundry.Security.Tokens;
using LendFoundry.Syndication.Probe;

namespace LendFoundry.Syndication.Probe.Client
{
   public interface IProbeServiceClientFactory
    {
        IProbeService Create(ITokenReader reader);
    }
}
