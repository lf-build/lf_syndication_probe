﻿using System;
using System.Threading.Tasks;
using LendFoundry.Foundation.Client;
using LendFoundry.Syndication.Probe;
using LendFoundry.Syndication.Probe.Request;
using LendFoundry.Syndication.Probe.Response;
using RestSharp;

namespace LendFoundry.Syndication.Probe.Client
{
    public class ProbeService :IProbeService
    {
        public ProbeService(IServiceClient client)
        {
            Client = client;
        }
        private IServiceClient Client { get; }

        public async Task<IGetSearchCompanyResponse> SearchCompany(string entityType, string entityId, ISearchRequest searchrequest)
        {
            var request = new RestRequest("/{entitytype}/{entityid}/companies", Method.POST);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityid", entityId);
            request.AddJsonBody(request);
            return await Client.ExecuteAsync<GetSearchCompanyResponse>(request);
        }

        public async Task<IGetFinancialDetailResponse> GetFinancialDetail(string entityType,string entityId, string cin)
        {
            var request = new RestRequest("/{entitytype}/{entityid}/finance/{cin}", Method.GET);
            request.AddUrlSegment("cin", cin);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityid", entityId);
            return await Client.ExecuteAsync<FinancialDetailResponse>(request);
        }
        public async Task<IGetCompanyDetailResponse> GetCompanyDetails(string entityType, string entityId, string cin)
        {
            var request = new RestRequest("/{entitytype}/{entityid}/companies/{cin}", Method.GET);
            request.AddUrlSegment("cin", cin);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityid", entityId);
            return await Client.ExecuteAsync<GetCompanyDetailResponse>(request);
        }
        public async Task<IGetAuthorisedSignatoryDetail> GetAuthorizedSignatoryDetail(string entityType, string entityId, string cin)
        {
            var request = new RestRequest("/{entitytype}/{entityid}/authorizedsignatory/detail/{cin}", Method.GET);
            request.AddUrlSegment("cin", cin);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityid", entityId);
            return await Client.ExecuteAsync<GetAuthorisedSignatoryDetail>(request);
        }
        public async Task<IFinancialParameterResponse> GetFinancialParameters(string entityType, string entityId, string cin)
        {
            var request = new RestRequest("/{entitytype}/{entityid}/financial/parameters/{cin}", Method.GET);
            request.AddUrlSegment("cin", cin);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityid", entityId);
            return await Client.ExecuteAsync<FinancialParameterResponse>(request);
        }
    }
}
