﻿using LendFoundry.Foundation.Client;
using LendFoundry.Security.Tokens;
using System;

using Microsoft.Extensions.DependencyInjection;

namespace LendFoundry.Syndication.Probe.Client
{
    public static class ProbeServiceClientExtensions
    {
        [Obsolete("Need to use the overloaded with Uri")]
        public static IServiceCollection AddProbeService(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<IProbeServiceClientFactory>(p => new ProbeServiceClientFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<IProbeServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
        public static IServiceCollection AddProbeService(this IServiceCollection services, Uri uri)
        {
            services.AddSingleton<IProbeServiceClientFactory>(p => new ProbeServiceClientFactory(p, uri));
            services.AddSingleton(p => p.GetService<IProbeServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddProbeService(this IServiceCollection services)
        {
            services.AddSingleton<IProbeServiceClientFactory>(p => new ProbeServiceClientFactory(p));
            services.AddSingleton(p => p.GetService<IProbeServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}
