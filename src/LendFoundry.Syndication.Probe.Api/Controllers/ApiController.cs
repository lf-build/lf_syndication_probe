﻿using System;
#if DOTNET2
using Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Mvc;
#endif
using System.Threading.Tasks;
using LendFoundry.Foundation.Services;
using LendFoundry.Syndication.Probe.Request;
using LendFoundry.Foundation.Logging;
using LendFoundry.Syndication.Probe.Response;

namespace LendFoundry.Syndication.Probe.Api.Controllers
{
    /// <summary>
    /// ApiController class
    /// </summary>
    [Route("/")]
    public class ApiController : ExtendedController
    {
        /// <summary>
        /// ApiController
        /// </summary>
        /// <param name="probeService"></param>
        /// <param name="logger"></param>
        /// <returns></returns>
        public ApiController(IProbeService probeService,ILogger logger):base(logger)
        {
            if (probeService == null)
                throw new ArgumentNullException(nameof(probeService));
            ProbeService = probeService;
        }

        private IProbeService ProbeService { get; }

        /// <summary>
        /// GetFinancialDetail
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="cin"></param>
        /// <returns></returns>
        [HttpGet("{entitytype}/{entityid}/finance/{cin}")]
#if DOTNET2
        [ProducesResponseType(typeof(IGetFinancialDetailResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> GetFinancialDetail(string entityType, string entityId,string cin)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await ProbeService.GetFinancialDetail(entityType, entityId, cin));
                }
                catch (ProbeException exception)
                {
                    return ErrorResult.BadRequest(exception.Message);
                }
            });
        }

        /// <summary>
        /// GetFinancialParameters
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="cin"></param>
        /// <returns></returns>
        [HttpGet("{entitytype}/{entityid}/financial/parameters/{cin}")]
        #if DOTNET2
        [ProducesResponseType(typeof(IFinancialParameterResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> GetFinancialParameters(string entityType, string entityId, string cin)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await ProbeService.GetFinancialParameters(entityType, entityId, cin));
                }
                catch (ProbeException exception)
                {
                    return ErrorResult.BadRequest(exception.Message);
                }
            });
        }

        /// <summary>
        ///  SearchCompany
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("{entitytype}/{entityid}/companies")]
       #if DOTNET2
        [ProducesResponseType(typeof(IGetSearchCompanyResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> SearchCompany(string entityType, string entityId, [FromBody]SearchRequest request)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await ProbeService.SearchCompany(entityType, entityId, request));
                }
                catch (ProbeException exception)
                {
                    return ErrorResult.BadRequest(exception.Message);
                }
            });
        }

        /// <summary>
        /// GetCompanyDetails
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="cin"></param>
        /// <returns></returns>
        [HttpGet("{entitytype}/{entityid}/companies/{cin}")]
        #if DOTNET2
        [ProducesResponseType(typeof(IGetCompanyDetailResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> GetCompanyDetails(string entityType, string entityId,string cin)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await ProbeService.GetCompanyDetails(entityType, entityId,cin));
                }
                catch (ProbeException exception)
                {
                    return ErrorResult.BadRequest(exception.Message);
                }
            });
        }

        /// <summary>
        /// GetAuthorizedSignatoryDetail
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="cin"></param>
        /// <returns></returns>
        [HttpGet("{entitytype}/{entityid}/authorizedsignatory/detail/{cin}")]
        #if DOTNET2
        [ProducesResponseType(typeof(IGetAuthorisedSignatoryDetail), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> GetAuthorizedSignatoryDetail(string entityType, string entityId,string cin)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await ProbeService.GetAuthorizedSignatoryDetail(entityType, entityId, cin));
                }
                catch (ProbeException exception)
                {
                    return ErrorResult.BadRequest(exception.Message);
                }
            });
        }
    }
}
