﻿using LendFoundry.Syndication.Probe.CompanyResponse;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Probe.Response
{
    public interface IGetFinancialDetailResponse
    {
        List<IFinancialDetail> FinancialDetails { get; set; }
        string ReferenceNumber { get; set; }
    }
}