﻿using LendFoundry.Foundation.Client;
using LendFoundry.Syndication.Probe.CompanyResponse;
using LendFoundry.Syndication.Probe.Proxy;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Probe.Response
{
    public class GetCompanyDetailResponse : IGetCompanyDetailResponse
    {
        public GetCompanyDetailResponse(CompanyDetailResponse companyDetailResponse)
        {
            if (companyDetailResponse?.data?.company != null)
                Company = new CompanyDetail(companyDetailResponse.data.company);
        }

        [JsonConverter(typeof(InterfaceConverter<ICompanyDetail, CompanyDetail>))]
        public ICompanyDetail Company { get; set; }

        public string ReferenceNumber { get; set; }
    }
}