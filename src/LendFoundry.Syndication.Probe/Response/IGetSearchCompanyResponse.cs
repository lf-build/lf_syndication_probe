﻿using LendFoundry.Syndication.Probe.SearchResponse;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Probe.Response
{
    public interface IGetSearchCompanyResponse
    {
        List<ISearchCompanyResponse> Companies { get; set; }
        string ReferenceNumber { get; set; }
    }
}