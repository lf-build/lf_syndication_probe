﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Probe.Response
{
    public class FinancialParameterResponse: IFinancialParameterResponse
    {
        public FinancialParameterResponse()
        {
        }

        public FinancialParameterResponse(Proxy.FinancialParameterResponse financialParameterResponse)
        {
            if (financialParameterResponse?.data?.financial_parameters != null)
                FinancialParameter = financialParameterResponse.data.financial_parameters.Select(p => new FinancialParameter(p)).ToList<IFinancialParameter>();
        }

        [JsonConverter(typeof(InterfaceListConverter<IFinancialParameter, FinancialParameter>))]
        public List<IFinancialParameter> FinancialParameter { get; set; }
        public string ReferenceNumber { get; set; }
    }
}
    
  

   
