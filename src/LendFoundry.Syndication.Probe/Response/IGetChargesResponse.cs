﻿using LendFoundry.Syndication.Probe.CompanyResponse;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Probe.Response
{
    public interface IGetChargesResponse
    {
        List<IChargesDetail> Charges { get; set; }

        string ReferenceNumber { get; set; }
    }
}