﻿namespace LendFoundry.Syndication.Probe.Response
{
    public class FinancialParameter : IFinancialParameter
    {
        public FinancialParameter()
        {

        }
        public FinancialParameter(Proxy.FinancialParameter financialParameter)
        {
            if (financialParameter!=null)
            {
                year = financialParameter.year;
                nature = financialParameter.nature;
                earning_fc = financialParameter.earning_fc;
                expenditure_fc = financialParameter.expenditure_fc;
                trade_receivable_exceeding_six_months = financialParameter.trade_receivable_exceeding_six_months;
                proposed_dividend = financialParameter.proposed_dividend;
            }
        }
        public string year { get; set; }
        public string nature { get; set; }
        public long earning_fc { get; set; }
        public long expenditure_fc { get; set; }
        public long transaction_related_parties_as_18 { get; set; }
        public long gross_fixed_assets { get; set; }
        public long trade_receivable_exceeding_six_months { get; set; }
        public string proposed_dividend { get; set; }
    }
}
