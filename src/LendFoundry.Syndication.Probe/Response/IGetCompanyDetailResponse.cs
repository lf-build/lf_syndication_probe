﻿using LendFoundry.Syndication.Probe.CompanyResponse;

namespace LendFoundry.Syndication.Probe.Response
{
    public interface IGetCompanyDetailResponse
    {
        ICompanyDetail Company { get; set; }

        string ReferenceNumber { get; set; }

    }
}