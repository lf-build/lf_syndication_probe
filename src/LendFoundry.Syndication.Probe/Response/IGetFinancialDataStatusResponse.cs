﻿using LendFoundry.Syndication.Probe.CompanyResponse;

namespace LendFoundry.Syndication.Probe.Response
{
    public interface IGetFinancialDataStatusResponse
    {
        IFinancialDataStatus Status { get; set; }

        string ReferenceNumber { get; set; }
        
    }
}