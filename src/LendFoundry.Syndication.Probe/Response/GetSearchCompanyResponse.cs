﻿using LendFoundry.Foundation.Client;
using LendFoundry.Syndication.Probe.Proxy;
using LendFoundry.Syndication.Probe.SearchResponse;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Probe.Response
{
    public class GetSearchCompanyResponse : IGetSearchCompanyResponse
    {
        public GetSearchCompanyResponse(CompanySearchResponse searchResponse)
        {
            if (searchResponse?.data?.companies != null)
                Companies = searchResponse.data.companies.Select(p => new SearchCompanyResponse(p)).ToList<ISearchCompanyResponse>();
        }

        [JsonConverter(typeof(InterfaceListConverter<ISearchCompanyResponse, SearchCompanyResponse>))]
        public List<ISearchCompanyResponse> Companies { get; set; }

        public string ReferenceNumber { get; set; }
    }
}