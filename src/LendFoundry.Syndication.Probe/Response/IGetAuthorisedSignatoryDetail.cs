﻿using LendFoundry.Syndication.Probe.CompanyResponse;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Probe.Response
{
    public interface IGetAuthorisedSignatoryDetail
    {
        List<IAuthorizedSignatoryDetail> AthorisedSignatories { get; set; }

        string ReferenceNumber { get; set; }
    }
}