﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Probe.Response
{
    public interface IFinancialParameterResponse
    {
         List<IFinancialParameter> FinancialParameter { get; set; }
         string ReferenceNumber { get; set; }
    }
}
