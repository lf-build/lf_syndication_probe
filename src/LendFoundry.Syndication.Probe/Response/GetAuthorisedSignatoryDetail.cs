﻿using LendFoundry.Foundation.Client;
using LendFoundry.Syndication.Probe.CompanyResponse;
using LendFoundry.Syndication.Probe.Proxy;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Probe.Response
{
    public class GetAuthorisedSignatoryDetail : IGetAuthorisedSignatoryDetail
    {
        public GetAuthorisedSignatoryDetail(AuthorizedSignatoryResponse authorizedSignatoryResponse)
        {
            if (authorizedSignatoryResponse?.data?.authorizedsignatories != null)
                AthorisedSignatories = authorizedSignatoryResponse.data.authorizedsignatories.Select(p => new AuthorizedSignatoryDetail(p)).ToList<IAuthorizedSignatoryDetail>();
        }

        [JsonConverter(typeof(InterfaceListConverter<IAuthorizedSignatoryDetail, AuthorizedSignatoryDetail>))]
        public List<IAuthorizedSignatoryDetail> AthorisedSignatories { get; set; }
        public string ReferenceNumber { get; set; }
    }
}