﻿namespace LendFoundry.Syndication.Probe.Request
{
    public interface ISearchRequest
    {
         string Filter { get; set; }

         string Value { get; set; }
    }
}
