﻿namespace LendFoundry.Syndication.Probe.Request
{
    public partial class SearchRequest: ISearchRequest
    {
        public string Filter { get; set; }

        public string Value { get; set; }

    }
}
