﻿using System.Threading.Tasks;
using LendFoundry.Syndication.Probe.Response;
using LendFoundry.Syndication.Probe.Request;

namespace LendFoundry.Syndication.Probe
{
    public interface IProbeService
    {
        Task<IGetAuthorisedSignatoryDetail> GetAuthorizedSignatoryDetail(string entityType, string entityId, string cin);
        //Task<IGetChargesResponse>  GetChargesDetail(IGetCompanyDetailRequest companyDetailRequest);
        Task<IGetCompanyDetailResponse> GetCompanyDetails(string entityType, string entityId, string cin);
        //Task<IGetFinancialDataStatusResponse> GetFinancialDataStatus(IGetCompanyDetailRequest companyDetailRequest);
        Task<IGetFinancialDetailResponse> GetFinancialDetail(string entityType, string entityId, string cin);
     //   Task<IGetSearchAuthorizedSignatoryResponse> SearchAuthorizedSignatory(string entityType,string entityId,string cin);
        Task<IGetSearchCompanyResponse> SearchCompany(string entityType, string entityId, ISearchRequest request);
        Task<IFinancialParameterResponse> GetFinancialParameters(string entityType, string entityId, string cin);
    }
}