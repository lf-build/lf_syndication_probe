﻿using LendFoundry.Syndication.Probe.Proxy;
using LendFoundry.Syndication.Probe.Response;
using System;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using LendFoundry.EventHub;
using LendFoundry.Syndication.Probe.Events;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Foundation.Services;
using LendFoundry.Foundation.Client;
using System.Linq;
using LendFoundry.Syndication.Probe.Request;

namespace LendFoundry.Syndication.Probe
{
  
    public class ProbeService :IProbeService
    {
        public ProbeService(IProbeServiceConfiguration configurationservice,IProbeProxy probeproxy, IEventHubClient eventHub, ILookupService lookup)
        {
            if (configurationservice == null)
                throw new ArgumentNullException(nameof(configurationservice));
            if (probeproxy == null)
                throw new ArgumentNullException(nameof(probeproxy));
            if (string.IsNullOrWhiteSpace(configurationservice.ServiceUrls))
                throw new ArgumentException("Url is required", nameof(configurationservice.ServiceUrls));
            if (string.IsNullOrWhiteSpace(configurationservice.ApiKey))
                throw new ArgumentException("ApiKey is required", nameof(configurationservice.ApiKey));
            if (string.IsNullOrWhiteSpace(configurationservice.ApiVersion))
                throw new ArgumentException("ApiVersion is required", nameof(configurationservice.ApiVersion));
            ProbeServiceConfiguration = configurationservice;
            ProbeProxy = probeproxy;
            EventHub = eventHub;
            Lookup = lookup;
        }
        public static string ServiceName { get; } = "probe";

        private IProbeServiceConfiguration ProbeServiceConfiguration { get; }
        private IProbeProxy ProbeProxy { get; }
        private IEventHubClient EventHub { get; }
        private ILookupService Lookup { get; }
        

        public async Task<IGetFinancialDetailResponse> GetFinancialDetail(string entityType,string entityId,string cin)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException("EntityType is require", nameof(entityType));
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException("EntityId is require", nameof(entityId));
            if (string.IsNullOrWhiteSpace(cin))
                throw new ArgumentException("Cin is require", nameof(cin));
            if (!(new Regex(@"[A-Z]{1}[0-9]{5}[A-Z]{2}[0-9]{4}[A-Z]{3}[0-9]{6}").IsMatch(cin)))
                throw new ArgumentException("Invalid Cin format", nameof(cin));
            try
            {
                entityType = EnsureEntityType(entityType);
               
                var financialDetailrequestproxy = new DetailRequest(ProbeServiceConfiguration, cin);
                var financialDetailproxyResponse = await ProbeProxy.GetFinancialDetail(financialDetailrequestproxy);
                var response = new Response.FinancialDetailResponse(financialDetailproxyResponse);
                var referencenumber = Guid.NewGuid().ToString("N");
                await EventHub.Publish(new FinancialDetailRequested
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    ReferenceNumber = referencenumber,
                    Response = response,
                    Request = new { cin },
                    Name = ServiceName
                });
                response.ReferenceNumber = referencenumber;
                return response;
            }
            catch(ProbeException exception)
            {
                await EventHub.Publish(new FinancialDetailRequestedFail
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    ReferenceNumber = null,
                    Response = exception.Message,
                    Request = new { cin },
                    Name = ServiceName
                });
                throw new ProbeException(exception.Message);
            }

        }

        public async Task<IGetSearchCompanyResponse> SearchCompany(string entityType, string entityId, ISearchRequest request)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException("EntityType is require", nameof(entityType));
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException("EntityId is require", nameof(entityId));
            if (request==null)
                throw new ArgumentException(nameof(request));
            try
            {
                entityType = EnsureEntityType(entityType);
                var proxyRequest = new Proxy.SearchRequest(request.Filter,request.Value);
                var companyDetailproxyResponse = await ProbeProxy.SearchCompany(proxyRequest);
                var response = new Response.GetSearchCompanyResponse(companyDetailproxyResponse);
                var referencenumber = Guid.NewGuid().ToString("N");
                await EventHub.Publish(new CompanySearchRequested
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    ReferenceNumber = referencenumber,
                    Response = response,
                    Request = request,
                    Name = ServiceName
                });
                response.ReferenceNumber = referencenumber;
                return response;
            }
            catch (ProbeException exception)
            {
                await EventHub.Publish(new CompanySearchFail
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    ReferenceNumber = null,
                    Response = exception.Message,
                    Request = request,
                    Name = ServiceName
                });
                throw new ProbeException(exception.Message);
            }

        }

        public async Task<IGetCompanyDetailResponse> GetCompanyDetails(string entityType, string entityId, string cin)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException("EntityType is require", nameof(entityType));
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException("EntityId is require", nameof(entityId));
            if (string.IsNullOrWhiteSpace(cin))
                throw new ArgumentException("Cin is require", nameof(cin));
            if (!(new Regex(@"[A-Z]{1}[0-9]{5}[A-Z]{2}[0-9]{4}[A-Z]{3}[0-9]{6}").IsMatch(cin)))
                throw new ArgumentException("Invalid Cin format", nameof(cin));
            try
            {
                entityType = EnsureEntityType(entityType);

                var fcompanyDetailproxyRequest = new DetailRequest(ProbeServiceConfiguration, cin);
                var companyDetailproxyResponse = await ProbeProxy.GetCompanyDetail(fcompanyDetailproxyRequest);
                var response = new Response.GetCompanyDetailResponse(companyDetailproxyResponse);
                var referencenumber = Guid.NewGuid().ToString("N");
                await EventHub.Publish(new CompanyDetailRequested
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    ReferenceNumber = referencenumber,
                    Response = response,
                    Request = new { cin },
                    Name = ServiceName
                });
                response.ReferenceNumber = referencenumber;
                return response;
            }
            catch (ProbeException exception)
            {
                await EventHub.Publish(new CompanyDetailRequestedFail
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    ReferenceNumber = null,
                    Response = exception.Message,
                    Request = new { cin },
                    Name = ServiceName
                });
                throw new ProbeException(exception.Message);
            }

        }

        public async Task<IGetAuthorisedSignatoryDetail> GetAuthorizedSignatoryDetail(string entityType, string entityId,string cin)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException("EntityType is require", nameof(entityType));
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException("EntityId is require", nameof(entityId));
            if (string.IsNullOrWhiteSpace(cin))
                throw new ArgumentException("Cin is require", nameof(cin));
            if (!(new Regex(@"[A-Z]{1}[0-9]{5}[A-Z]{2}[0-9]{4}[A-Z]{3}[0-9]{6}").IsMatch(cin)))
                throw new ArgumentException("Invalid Cin format", nameof(cin));
            try
            {
                entityType = EnsureEntityType(entityType);

                var proxyRequest = new DetailRequest(ProbeServiceConfiguration, cin);
                var proxyResponse = await ProbeProxy.GetAuthorizedSignatoryDetail(proxyRequest);
                var response = new Response.GetAuthorisedSignatoryDetail(proxyResponse);
                var referencenumber = Guid.NewGuid().ToString("N");
                await EventHub.Publish(new AuthorizedSignatoryDetailRequested
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    ReferenceNumber = referencenumber,
                    Response = response,
                    Request = new { cin },
                    Name = ServiceName
                });
                response.ReferenceNumber = referencenumber;
                return response;
            }
            catch (ProbeException exception)
            {
                await EventHub.Publish(new AuthorizedSignatoryDetailFail
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    ReferenceNumber = null,
                    Response = exception.Message,
                    Request = new { cin },
                    Name = ServiceName
                });
                throw new ProbeException(exception.Message);
            }

        }

        public async Task<IFinancialParameterResponse> GetFinancialParameters(string entityType, string entityId, string cin)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException("EntityType is require", nameof(entityType));
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException("EntityId is require", nameof(entityId));
            if (string.IsNullOrWhiteSpace(cin))
                throw new ArgumentException("Cin is require", nameof(cin));
            if (!(new Regex(@"[A-Z]{1}[0-9]{5}[A-Z]{2}[0-9]{4}[A-Z]{3}[0-9]{6}").IsMatch(cin)))
                throw new ArgumentException("Invalid Cin format", nameof(cin));
            try
            {
                entityType = EnsureEntityType(entityType);

                var financialDetailrequestproxy = new DetailRequest(ProbeServiceConfiguration, cin);
                var financialDetailproxyResponse = await ProbeProxy.GetFinancialParameters(financialDetailrequestproxy);
                var response = new Response.FinancialParameterResponse(financialDetailproxyResponse);
                var referencenumber = Guid.NewGuid().ToString("N");
                await EventHub.Publish(new FinancialParameterRequested
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    ReferenceNumber = referencenumber,
                    Response = response,
                    Request = new { cin },
                    Name = ServiceName
                });
                response.ReferenceNumber = referencenumber;
                return response;
            }
            catch (ProbeException exception)
            {
                await EventHub.Publish(new FinancialParameterRequestedFail
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    ReferenceNumber = null,
                    Response = exception.Message,
                    Request = new { cin },
                    Name = ServiceName
                });
                throw new ProbeException(exception.Message);
            }

        }
        public string EnsureEntityType(string entityType)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new InvalidArgumentException("Invalid Entity Type");

            entityType = entityType.ToLower();
            var validEntityTypes = Lookup.GetLookupEntry("entityTypes", entityType);

            if (validEntityTypes == null || !validEntityTypes.Any())
                throw new InvalidArgumentException("Invalid Entity Type");

            return entityType;
        }
    }
}
