﻿using LendFoundry.Syndication.Probe.Proxy;

namespace LendFoundry.Syndication.Probe.CompanyResponse
{
    public class FinancialDataStatus : IFinancialDataStatus
    {
        public FinancialDataStatus(FinancialDataDataStatus financialdatastatus)
        {
            if (financialdatastatus != null)
            {
                LastFinYearEnd = financialdatastatus.last_fin_year_end;
                LastUpdated = financialdatastatus.last_updated;
            }
        }

        public string LastFinYearEnd { get; set; }
        public string LastUpdated { get; set; }
    }
}