﻿using LendFoundry.Foundation.Client;
using LendFoundry.Syndication.Probe.Proxy;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Probe.CompanyResponse
{
    public class CompanyDetail : ICompanyDetail
    {
        public CompanyDetail()
        {
                
        }
        public CompanyDetail(Company company)
        {
            if (company != null)
            {
                AuthorizedCapital = company.authorized_capital;
                Cin = company.cin;
                EfilingStatus = company.efiling_status;
                IncorporationDate = company.incorporation_date;
                LegalName = company.legal_name;
                PaidUpCapital = company.paid_up_capital;
                SumOfCharges = company.sum_of_charges;
                if (company.registered_address != null)
                    RegisteredAddress = new Address(company.registered_address);
                Classification = company.classification;
                Status = company.status;
                NextCin = company.next_cin;
                LastAgmDate = company.last_agm_date;
                LastFilingDate = company.last_filing_date;
            }
        }

        public int AuthorizedCapital { get; set; }
        public string Cin { get; set; }
        public string EfilingStatus { get; set; }
        public string IncorporationDate { get; set; }
        public string LegalName { get; set; }
        public string PaidUpCapital { get; set; }
        public string SumOfCharges { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IAddress, Address>))]
        public IAddress RegisteredAddress { get; set; }
        public string Classification { get; set; }
        public string Status { get; set; }
        public string NextCin { get; set; }
        public string LastAgmDate { get; set; }
        public string LastFilingDate { get; set; }
    }
}