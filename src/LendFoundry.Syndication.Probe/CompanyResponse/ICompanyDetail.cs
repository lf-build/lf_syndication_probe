﻿namespace LendFoundry.Syndication.Probe.CompanyResponse
{
    public interface ICompanyDetail
    {
        int AuthorizedCapital { get; set; }
        string Cin { get; set; }
        string Classification { get; set; }
        string EfilingStatus { get; set; }
        string IncorporationDate { get; set; }
        string LastAgmDate { get; set; }
        string LastFilingDate { get; set; }
        string LegalName { get; set; }
        string NextCin { get; set; }
        string PaidUpCapital { get; set; }
        IAddress RegisteredAddress { get; set; }
        string Status { get; set; }
        string SumOfCharges { get; set; }
    }
}