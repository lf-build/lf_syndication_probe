﻿using LendFoundry.Foundation.Client;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Probe
{
    public interface IProbeServiceConfiguration : IDependencyConfiguration
    {
        string ServiceUrls { get; set; }
        string ApiKey { get; set; }
        string ApiVersion { get; set; }

        int Limit { get; set; }
        int Offset { get; set; }
        string SearchFilterValue { get; set; }
        string ConnectionString { get; set; }

    }
}