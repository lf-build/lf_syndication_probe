﻿using LendFoundry.SyndicationStore.Events;

namespace LendFoundry.Syndication.Probe.Events
{
    public class FinancialDetailRequestedFail : SyndicationCalledEvent
    {
    }
}
