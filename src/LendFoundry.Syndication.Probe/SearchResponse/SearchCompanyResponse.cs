﻿using LendFoundry.Syndication.Probe.Proxy;

namespace LendFoundry.Syndication.Probe.SearchResponse
{
    public class SearchCompanyResponse : ISearchCompanyResponse
    {
        public SearchCompanyResponse(SearchCompany searchcompany)
        {
            if (searchcompany != null)
            {
                Cin = searchcompany.cin;
                LegalName = searchcompany.legal_name;
            }
        }

        public string Cin { get; set; }

        public string LegalName { get; set; }
    }
}