﻿using LendFoundry.Foundation.Client;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Probe
{
    public class ProbeServiceConfiguration : IProbeServiceConfiguration, IDependencyConfiguration
    {
        public string ApiVersion { get; set; } 

        public string ApiKey { get; set; } 

        public string ServiceUrls { get; set; }

        public int Limit { get; set; }
        public int Offset { get; set; } 
        public string SearchFilterValue { get; set; }   
        public Dictionary<string, string> Dependencies { get; set; }
        public string Database { get; set; }
        public string ConnectionString { get; set; }

    }
}