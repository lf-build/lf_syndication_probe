﻿namespace LendFoundry.Syndication.Probe.Proxy
{
    public class SearchCompany
    {
        public string legal_name { get; set; }
        public string cin { get; set; }
    }
}