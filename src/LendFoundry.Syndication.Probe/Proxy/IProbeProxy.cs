﻿using System.Threading.Tasks;

namespace LendFoundry.Syndication.Probe.Proxy
{
    public interface IProbeProxy
    {
       Task<AuthorizedSignatoryResponse> GetAuthorizedSignatoryDetail(DetailRequest companydetailrequest);

      Task<ChargesResponse> GetChargesDetail(DetailRequest companydetailrequest);

       Task<CompanyDetailResponse> GetCompanyDetail(DetailRequest companydetailrequest);

       Task<FinancialDataStatusResponse> GetFinancialDataStatus(DetailRequest companydetailrequest);

        Task<FinancialDetailResponse> GetFinancialDetail(DetailRequest companydetailrequest);

        Task<AuthorizedSignatorySearchResponse> SearchAuthorizedSignatory(SearchRequest searchrequest);

        Task<CompanySearchResponse> SearchCompany(SearchRequest searchrequest);
        Task<FinancialParameterResponse> GetFinancialParameters(DetailRequest companydetailrequest);
    }
}