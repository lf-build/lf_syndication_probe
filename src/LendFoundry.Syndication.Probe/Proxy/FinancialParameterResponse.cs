﻿
using System.Collections.Generic;

namespace LendFoundry.Syndication.Probe.Proxy
{
    public class FinancialParameterResponse
    {
        public Metadata metadata { get; set; }
        public Data data { get; set; }
    }
    

    public class FinancialParameter
    {
        public string year { get; set; }
        public string nature { get; set; }
        public long earning_fc { get; set; }
        public long expenditure_fc { get; set; }
        public long transaction_related_parties_as_18 { get; set; }
        public long gross_fixed_assets { get; set; }
        public long trade_receivable_exceeding_six_months { get; set; }
        public string proposed_dividend { get; set; }
    }

    public class Data
    {
        public List<FinancialParameter> financial_parameters { get; set; }
    }

}
