﻿using Newtonsoft.Json;

namespace LendFoundry.Syndication.Probe.Proxy
{
    public class ChargesResponse
    {
        [JsonProperty("meta")]
        public Metadata metadata { get; set; }
        [JsonProperty("data")]
        public ChargeData data { get; set; }
    }

    public class ChargeData
    {
        [JsonProperty("charges")]
        public Charge[] charges { get; set; }
    }

    public class Charge
    {
        public double amount { get; set; }
        public string date { get; set; }
        public string holder_name { get; set; }
        public string type { get; set; }
    }
}