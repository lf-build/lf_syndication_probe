﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Net;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Probe.Proxy
{
    public class ProbeProxy : IProbeProxy
    {
        private IProbeServiceConfiguration ProbeServiceConfiguration { get; }

        public ProbeProxy(IProbeServiceConfiguration probeserviceconfiguration)
        {
            if (probeserviceconfiguration == null)
                throw new ArgumentNullException(nameof(probeserviceconfiguration));
            if (string.IsNullOrWhiteSpace(probeserviceconfiguration.ServiceUrls))
                throw new ArgumentException("Url is required", nameof(probeserviceconfiguration.ServiceUrls));
            if (string.IsNullOrWhiteSpace(probeserviceconfiguration.ApiKey))
                throw new ArgumentException("ApiKey is required", nameof(probeserviceconfiguration.ApiKey));
            if (string.IsNullOrWhiteSpace(probeserviceconfiguration.ApiVersion))
                throw new ArgumentException("ApiVersion is required", nameof(probeserviceconfiguration.ApiVersion));
            ProbeServiceConfiguration = probeserviceconfiguration;
        }

        public async Task<CompanySearchResponse> SearchCompany(SearchRequest searchrequest)
        {
            if (searchrequest == null)
                throw new ArgumentNullException(nameof(searchrequest));
            var client = new RestClient(ProbeServiceConfiguration.ServiceUrls + "companies");
            var request = new RestRequest(Method.GET);
            string filter = WebUtility.UrlEncode("{\"nameStartsWith\":\"Probe\"}");
            request.AddHeader("Accept", "application/json");
            request.AddHeader("x-api-key", ProbeServiceConfiguration.ApiKey);
            request.AddHeader("x-api-version", ProbeServiceConfiguration.ApiVersion);
            request.AddParameter("limit", ProbeServiceConfiguration.Limit);
            request.AddParameter("filters", "{\""+ProbeServiceConfiguration.SearchFilterValue+"\":\""+searchrequest.Filter+"\"}");
            var objectResponse =await ExecuteRequestAsync<CompanySearchResponse>(client, request);
            return objectResponse;
        }

        public async Task<AuthorizedSignatorySearchResponse> SearchAuthorizedSignatory(SearchRequest searchrequest)
        {
            if (searchrequest == null)
                throw new ArgumentNullException(nameof(searchrequest));
            var client = new RestClient(ProbeServiceConfiguration.ServiceUrls + "authorized-signatories/");
            var request = new RestRequest(Method.GET);
            request.AddHeader("Accept", "application/json");
            request.AddHeader("x-api-key", ProbeServiceConfiguration.ApiKey);
            request.AddHeader("x-api-version", ProbeServiceConfiguration.ApiVersion);
            request.AddParameter("limit", ProbeServiceConfiguration.Limit);
            request.AddParameter("filters",searchrequest.Filter);
            request.AddParameter("offset", ProbeServiceConfiguration.Offset);
            var objectResponse = await ExecuteRequestAsync<AuthorizedSignatorySearchResponse>(client, request);
            return objectResponse;
        }


        public async Task<CompanyDetailResponse> GetCompanyDetail(DetailRequest companydetailrequest)
        {
            if (companydetailrequest == null)
                throw new ArgumentNullException(nameof(companydetailrequest));
            var client = new RestClient(ProbeServiceConfiguration.ServiceUrls + "companies/"+ companydetailrequest.Cin);
            var request = new RestRequest(Method.GET);
            request.AddHeader("Accept", "application/json");
            request.AddHeader("x-api-key", ProbeServiceConfiguration.ApiKey);
            request.AddHeader("x-api-version", ProbeServiceConfiguration.ApiVersion);
            var objectResponse = await ExecuteRequestAsync<CompanyDetailResponse>(client, request);
            return objectResponse;
        }

        public async Task<AuthorizedSignatoryResponse> GetAuthorizedSignatoryDetail(DetailRequest companydetailrequest)
        {
            if (companydetailrequest == null)
                throw new ArgumentNullException(nameof(companydetailrequest));
            var client = new RestClient(ProbeServiceConfiguration.ServiceUrls + "companies/"+companydetailrequest.Cin+ "/authorized-signatories");
            var request = new RestRequest(Method.GET);
            request.AddHeader("Accept", "application/json");
            request.AddHeader("x-api-key", ProbeServiceConfiguration.ApiKey);
            request.AddHeader("x-api-version", ProbeServiceConfiguration.ApiVersion);
            var objectResponse = await ExecuteRequestAsync<AuthorizedSignatoryResponse>(client, request);
            return objectResponse;
        }

        public async Task<ChargesResponse> GetChargesDetail(DetailRequest companydetailrequest)
        {
            if (companydetailrequest == null)
                throw new ArgumentNullException(nameof(companydetailrequest));
            var client = new RestClient(ProbeServiceConfiguration.ServiceUrls + "companies/" + companydetailrequest.Cin + "/charges");
            var request = new RestRequest(Method.GET);
            request.AddHeader("Accept", "application/json");
            request.AddHeader("x-api-key", ProbeServiceConfiguration.ApiKey);
            request.AddHeader("x-api-version", ProbeServiceConfiguration.ApiVersion);
            var objectResponse = await ExecuteRequestAsync<ChargesResponse>(client, request);
            return objectResponse;
        }

        public async Task<FinancialDataStatusResponse> GetFinancialDataStatus(DetailRequest companydetailrequest)
        {
            if (companydetailrequest == null)
                throw new ArgumentNullException(nameof(companydetailrequest));
            var client = new RestClient(ProbeServiceConfiguration.ServiceUrls + "companies/" + companydetailrequest.Cin + "/financial-datastatus");
            var request = new RestRequest(Method.GET);
            request.AddHeader("Accept", "application/json");
            request.AddHeader("x-api-key", ProbeServiceConfiguration.ApiKey);
            request.AddHeader("x-api-version", ProbeServiceConfiguration.ApiVersion);
            var objectResponse = await ExecuteRequestAsync<FinancialDataStatusResponse>(client, request);
            return objectResponse;
        }
        public async Task<FinancialDetailResponse> GetFinancialDetail(DetailRequest companydetailrequest)
        {
            if (companydetailrequest == null)
                throw new ArgumentNullException(nameof(companydetailrequest));
            var client = new RestClient(ProbeServiceConfiguration.ServiceUrls + "companies/" + companydetailrequest.Cin + "/financials");
            var request = new RestRequest(Method.GET);
            request.AddHeader("Accept", "application/json");
            request.AddHeader("x-api-key", ProbeServiceConfiguration.ApiKey);
            request.AddHeader("x-api-version", ProbeServiceConfiguration.ApiVersion);
            var objectResponse = await ExecuteRequestAsync<FinancialDetailResponse>(client, request);
            return objectResponse;
        }
        public async Task<FinancialParameterResponse> GetFinancialParameters(DetailRequest companydetailrequest)
        {
            if (companydetailrequest == null)
                throw new ArgumentNullException(nameof(companydetailrequest));
            var client = new RestClient(ProbeServiceConfiguration.ServiceUrls + "companies/" + companydetailrequest.Cin + "/financial-parameters");
            var request = new RestRequest(Method.GET);
            request.AddHeader("Accept", "application/json");
            request.AddHeader("x-api-key", ProbeServiceConfiguration.ApiKey);
            request.AddHeader("x-api-version", ProbeServiceConfiguration.ApiVersion);
            var objectResponse = await ExecuteRequestAsync<FinancialParameterResponse>(client, request);
            return objectResponse;
        }
        private async Task<T> ExecuteRequestAsync<T>(IRestClient client, IRestRequest request) where T : class
        {
            var response =await client.ExecuteTaskAsync(request);
            if (response == null)
                throw new ArgumentNullException(nameof(response));

            if (response.ErrorException != null)
                throw new ProbeException("Service call failed", response.ErrorException);

            if (response.ResponseStatus != ResponseStatus.Completed)
                throw new ProbeException(
                    $"Service call failed. Status {response.ResponseStatus}. Response: {response.Content ?? ""}");

            if (response.ResponseStatus == ResponseStatus.Completed)
            {
                if (response.StatusCode.ToString().ToLower() == "methodnotallowed")
                    throw new ProbeException(
                        $"Service call failed. Status {response.ResponseStatus}. Response: {response.StatusDescription ?? ""}");
            }

            return JsonConvert.DeserializeObject<T>(response.Content);
        }
    }
}
