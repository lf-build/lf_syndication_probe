﻿public interface IFinancialParameter
{
    long earning_fc { get; set; }
    long expenditure_fc { get; set; }
    long gross_fixed_assets { get; set; }
    string nature { get; set; }
    string proposed_dividend { get; set; }
    long trade_receivable_exceeding_six_months { get; set; }
    long transaction_related_parties_as_18 { get; set; }
    string year { get; set; }
}